<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "entrenadores".
 *
 * @property string $dni
 * @property string|null $nombre
 * @property string|null $posicion
 * @property int|null $sueldomensual
 * @property int|null $años_entrenando
 * @property string|null $es_ataque_o_es_defensa
 * @property int|null $es_principal
 * @property string|null $nombre_equipo
 *
 * @property Equipos $nombreEquipo
 */
class Entrenadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'entrenadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni'], 'required'],
            [['sueldomensual', 'años_entrenando', 'es_principal'], 'integer'],
            [['dni', 'nombre', 'posicion', 'nombre_equipo'], 'string', 'max' => 20],
            [['es_ataque_o_es_defensa'], 'string', 'max' => 15],
            [['dni'], 'unique'],
            [['nombre_equipo'], 'exist', 'skipOnError' => true, 'targetClass' => Equipos::className(), 'targetAttribute' => ['nombre_equipo' => 'nombre']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni' => 'Dni',
            'nombre' => 'Nombre',
            'posicion' => 'Posicion',
            'sueldomensual' => 'Sueldo mensual',
            //'años_entrenando' => 'Años Entrenando',
            //'es_ataque_o_es_defensa' => 'Posicion',
            'es_principal' => 'Principal',
            'nombre_equipo' => 'Equipo',
        ];
    }

    /**
     * Gets query for [[NombreEquipo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombreEquipo()
    {
        return $this->hasOne(Equipos::className(), ['nombre' => 'nombre_equipo']);
    }
}
