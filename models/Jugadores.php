<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jugadores".
 *
 * @property string $dni
 * @property string|null $nombre
 * @property int|null $dorsal
 * @property string|null $centro
 * @property int|null $sueldo_anual
 * @property int|null $edad
 * @property string|null $posicion
 * @property string|null $nombre_equipo
 *
 * @property Equipos $nombreEquipo
 */
class Jugadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jugadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni'], 'required'],
            [['dorsal', 'sueldo_anual', 'edad'], 'integer'],
            [['dni', 'nombre', 'centro', 'posicion', 'nombre_equipo'], 'string', 'max' => 20],
            [['dni'], 'unique'],
            [['nombre_equipo'], 'exist', 'skipOnError' => true, 'targetClass' => Equipos::className(), 'targetAttribute' => ['nombre_equipo' => 'nombre']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni' => 'Dni',
            'nombre' => 'Nombre',
            'dorsal' => 'Dorsal',
            'centro' => 'Centro',
            'sueldo_anual' => 'Sueldo Anual',
            'edad' => 'Edad',
            'posicion' => 'Posicion',
            'nombre_equipo' => 'Equipo',
        ];
    }

    /**
     * Gets query for [[NombreEquipo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombreEquipo()
    {
        return $this->hasOne(Equipos::className(), ['nombre' => 'nombre_equipo']);
    }
}
