<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "personal".
 *
 * @property string $dni
 * @property string|null $nombre
 * @property string|null $especializacion
 * @property int|null $sueldomensual
 * @property string|null $nombre_equipo
 *
 * @property Equipos $nombreEquipo
 */
class Personal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'personal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni'], 'required'],
            [['sueldomensual'], 'integer'],
            [['dni', 'nombre', 'especializacion', 'nombre_equipo'], 'string', 'max' => 20],
            [['dni'], 'unique'],
            [['nombre_equipo'], 'exist', 'skipOnError' => true, 'targetClass' => Equipos::className(), 'targetAttribute' => ['nombre_equipo' => 'nombre']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni' => 'Dni',
            'nombre' => 'Nombre',
            'especializacion' => 'Especializacion',
            'sueldomensual' => 'Sueldo mensual',
            'nombre_equipo' => 'Equipo',
        ];
    }

    /**
     * Gets query for [[NombreEquipo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombreEquipo()
    {
        return $this->hasOne(Equipos::className(), ['nombre' => 'nombre_equipo']);
    }
}
