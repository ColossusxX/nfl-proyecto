<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "equipos".
 *
 * @property string $nombre
 * @property string|null $estado
 * @property int|null $numjugadores
 * @property string|null $entrenadorprin
 *
 * @property Entrenadores[] $entrenadores
 * @property Jugadores[] $jugadores
 * @property Partidos[] $partidos
 * @property Partidos[] $partidos0
 * @property Personal[] $personals
 */
class Equipos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'equipos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['numjugadores'], 'integer'],
            [['nombre', 'estado', 'entrenadorprin'], 'string', 'max' => 20],
            [['nombre'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nombre' => 'Nombre',
            'estado' => 'Estado',
            'numjugadores' => 'Jugadores',
            'entrenadorprin' => 'Entrenador Principal',
        ];
    }

    /**
     * Gets query for [[Entrenadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntrenadores()
    {
        return $this->hasMany(Entrenadores::className(), ['nombre_equipo' => 'nombre']);
    }

    /**
     * Gets query for [[Jugadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJugadores()
    {
        return $this->hasMany(Jugadores::className(), ['nombre_equipo' => 'nombre']);
    }

    /**
     * Gets query for [[Partidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPartidos()
    {
        return $this->hasMany(Partidos::className(), ['nombre_equipo_local' => 'nombre']);
    }

    /**
     * Gets query for [[Partidos0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPartidos0()
    {
        return $this->hasMany(Partidos::className(), ['nombre_equipo_visitante' => 'nombre']);
    }

    /**
     * Gets query for [[Personals]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonals()
    {
        return $this->hasMany(Personal::className(), ['nombre_equipo' => 'nombre']);
    }
}
