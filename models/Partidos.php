<?php

namespace app\models;
use Yii;
/**
 * This is the model class for table "partidos".
 *
 * @property int $id
 * @property string|null $ganador
 * @property string|null $MVP
 * @property int|null $TD_totales
 * @property string|null $nombre_equipo_local
 * @property string|null $nombre_equipo_visitante
 * @property int|null $TD_locales
 * @property int|null $TD_Visitantes
 * @property int|null $ExtrasLocales
 * @property int|null $ExtrasVisitantes
 *
 * @property EntrenadoresPartidos[] $entrenadoresPartidos
 * @property Equipos $nombreEquipoLocal
 * @property Equipos $nombreEquipoVisitante
 */
class Partidos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $media;
    public static function tableName()
    {
        return 'partidos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['TD_totales', 'TD_locales', 'TD_Visitantes', 'ExtrasLocales', 'ExtrasVisitantes'], 'integer'],
            [['ganador', 'MVP', 'nombre_equipo_local', 'nombre_equipo_visitante'], 'string', 'max' => 20],
            [['nombre_equipo_local'], 'exist', 'skipOnError' => true, 'targetClass' => Equipos::className(), 'targetAttribute' => ['nombre_equipo_local' => 'nombre']],
            [['nombre_equipo_visitante'], 'exist', 'skipOnError' => true, 'targetClass' => Equipos::className(), 'targetAttribute' => ['nombre_equipo_visitante' => 'nombre']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ganador' => 'Ganador',
            'MVP' => 'Mvp',
            //'TD_totales' => 'Touchdowns Totales',
            'nombre_equipo_local' => 'Equipo Local',
            'nombre_equipo_visitante' => 'Equipo Visitante',
            'TD_locales' => 'TD Locales',
            'TD_Visitantes' => 'TD Visitantes',
            //'ExtrasLocales' => 'Extras Locales',
            //'ExtrasVisitantes' => 'Extras Visitantes',
        ];
    }

    /**
     * Gets query for [[EntrenadoresPartidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntrenadoresPartidos()
    {
        return $this->hasMany(EntrenadoresPartidos::className(), ['id_partidos' => 'id']);
    }

    /**
     * Gets query for [[NombreEquipoLocal]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombreEquipoLocal()
    {
        return $this->hasOne(Equipos::className(), ['nombre' => 'nombre_equipo_local']);
    }

    /**
     * Gets query for [[NombreEquipoVisitante]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombreEquipoVisitante()
    {
        return $this->hasOne(Equipos::className(), ['nombre' => 'nombre_equipo_visitante']);
    }
}
