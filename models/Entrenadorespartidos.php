<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "entrenadores_partidos".
 *
 * @property string|null $nombre_entrenadores
 * @property int|null $id_partidos
 * @property int $id
 *
 * @property Partidos $partidos
 */
class Entrenadorespartidos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'entrenadores_partidos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_partidos'], 'integer'],
            [['nombre_entrenadores'], 'string', 'max' => 20],
            [['nombre_entrenadores', 'id_partidos'], 'unique', 'targetAttribute' => ['nombre_entrenadores', 'id_partidos']],
            [['id_partidos'], 'exist', 'skipOnError' => true, 'targetClass' => Partidos::className(), 'targetAttribute' => ['id_partidos' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nombre_entrenadores' => 'Nombre Entrenador',
            'id_partidos' => 'Id Partidos',
            'id' => 'ID',
        ];
    }

    /**
     * Gets query for [[Partidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPartidos()
    {
        return $this->hasOne(Partidos::className(), ['id' => 'id_partidos']);
    }
}
