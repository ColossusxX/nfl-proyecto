﻿DROP DATABASE IF EXISTS NFL;
CREATE DATABASE NFL;

USE NFL;
/*crear las tablas */

CREATE OR REPLACE TABLE Equipos(
  nombre varchar(20),
  estado varchar(20),
  numjugadores int(3),
  entrenadorprin varchar(20),
  PRIMARY KEY (nombre)
  );
CREATE OR REPLACE TABLE Jugadores(
  dni varchar(20),
  nombre varchar(20),
  dorsal int(3),
  centro varchar(20),
  sueldo_anual int,
  edad int,
  posicion varchar(20),
  nombre_equipo varchar(20),
  PRIMARY KEY (dni)
  );
CREATE OR REPLACE TABLE Personal(
  dni varchar(20),
  nombre varchar(20),
  especializacion varchar(20),
  sueldomensual int,
  nombre_equipo varchar(20),
  PRIMARY KEY (dni)
  );
CREATE OR REPLACE TABLE Entrenadores(
  dni varchar(20),
  nombre varchar(20),
  posicion varchar(20),
  sueldomensual int,
  años_entrenando int(3),
  es_ataque_o_es_defensa varchar(15),
  es_principal boolean,
  nombre_equipo varchar(20),
  PRIMARY KEY (dni)
  );
CREATE OR REPLACE TABLE Partidos(
  id int AUTO_INCREMENT,
  ganador varchar(20),
  MVP varchar(20),
  TD_totales int,
  nombre_equipo_local varchar(20),
  nombre_equipo_visitante varchar(20),
  TD_locales int(3),
  TD_Visitantes int(3),
  ExtrasLocales int(2),
  ExtrasVisitantes int(2),
  PRIMARY KEY (id)
  );
CREATE OR REPLACE TABLE Entrenadores_Partidos(
  nombre_entrenadores varchar(20),
  id_partidos int,
  id int AUTO_INCREMENT,
  PRIMARY KEY (id)
  );


 ALTER TABLE Jugadores
  ADD CONSTRAINT fk_Equipos_Jugadores
  FOREIGN KEY (nombre_equipo)
  REFERENCES Equipos(nombre);

ALTER TABLE Personal
  ADD CONSTRAINT fk_Equipos_Personal
  FOREIGN KEY (nombre_equipo)
  REFERENCES Equipos(nombre);

ALTER TABLE Entrenadores
  ADD CONSTRAINT fk_Equipos_Entrenadores
  FOREIGN KEY (nombre_equipo)
  REFERENCES Equipos(nombre);

ALTER TABLE Partidos
  ADD CONSTRAINT fk_Partidos_EquipoLocal
  FOREIGN KEY (nombre_equipo_local)
  REFERENCES Equipos(nombre),

 ADD CONSTRAINT fk_Partidos_EquipoVisitante
  FOREIGN KEY (nombre_equipo_visitante)
  REFERENCES Equipos(nombre);

ALTER TABLE Entrenadores_Partidos
  ADD CONSTRAINT fk_Entrenadores_Partidos
  FOREIGN KEY (id_partidos)
  REFERENCES Partidos(id),

  ADD CONSTRAINT fk_EntrenadoresPartidos
  UNIQUE KEY(nombre_entrenadores,id_partidos);
