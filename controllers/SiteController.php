<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use app\models\Partidos;
use app\models\Jugadores;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionCalendario()
    {
        return $this->render('calendario');
    }
    public function actionMvp(){
        
        
        $dataProvider = new ActiveDataProvider([
            'query' => Jugadores::find() 
                ->where("nombre in (SELECT MVP FROM partidos)")
    
            ]);
        
        return $this->render("mvp",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad','nombre_equipo'],
            "titulo"=>"MVPs",
            "enunciado"=>"Mvp de la temporada 2019/2020",
            "sql"=>"", 
      
            
        ]);
    }
    public function actionConsulta2(){
        
        
        $dataProvider = new ActiveDataProvider([
            'query' => Jugadores::find()
                ->where("centro = 'Yale'")
            ]);
        
        return $this->render("consulta2",[
            "resultados"=>$dataProvider,    
            "campos"=>['nombre','edad','dorsal','posicion','nombre_equipo'],
            "titulo"=>"Centro Yale",
            "enunciado"=>"Jugadores pertenecientes al centro de yale",
            "sql"=>"", 
      
            
        ]);
    }
    public function actionConsulta3(){
        
        
        $dataProvider = new ActiveDataProvider([
            'query' => Jugadores::find()
                ->select ("nombre,edad,nombre_equipo,dorsal")->where("edad >18 AND edad <25")->orderBy("edad ASC")
            ]);
        
        return $this->render("consulta3",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad','nombre_equipo','dorsal'],
            "titulo"=>"Media de touchdowns",
            "enunciado"=>"Media de touchdowns",
            "sql"=>"", 
      
            
        ]);
    }
    public function actionConsulta4(){
        
        
        $dataProvider = new ActiveDataProvider([
            'query' => Jugadores::find()
                ->select ("nombre,edad,nombre_equipo,dorsal")->where("edad >18 AND edad <25")->orderBy("edad ASC")
            ]);
        
        return $this->render("consulta4",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad','nombre_equipo','dorsal'],
            "titulo"=>"Jugadores mas jovenes",
            "enunciado"=>"",
            "sql"=>"", 
      
            
        ]);
    }
    public function actionConsulta5(){
        
        
        $dataProvider = new ActiveDataProvider([
            'query' => Jugadores::find()
                ->select ("nombre,edad,dorsal,centro")->where("nombre_equipo= 'Jaguar'")->orderBy("edad ASC")
            ]);
        
        return $this->render("consulta5",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad','centro','dorsal'],
            "titulo"=>"Quarterbacks",
            "enunciado"=>"",
            "sql"=>"", 
      
            
        ]);
    }
    public function actionConsulta6(){
        
        
        $dataProvider = new ActiveDataProvider([
            'query' => Jugadores::find()
                ->where("centro = 'Michigan'")
            ]);
        
        return $this->render("consulta6",[
            "resultados"=>$dataProvider,    
            "campos"=>['nombre','edad','dorsal','posicion','nombre_equipo'],
            "titulo"=>"Centro Michigan",
            "enunciado"=>"Jugadores pertenecientes al centro de Michigan",
            "sql"=>"", 
      
            
        ]);
    }
    public function actionConsulta7(){
        
        
        $dataProvider = new ActiveDataProvider([
            'query' => Jugadores::find()
                ->where("centro = 'Penn State'")
            ]);
        
        return $this->render("consulta7",[
            "resultados"=>$dataProvider,    
            "campos"=>['nombre','edad','dorsal','posicion','nombre_equipo'],
            "titulo"=>"Centro Michigan",
            "enunciado"=>"Jugadores pertenecientes al centro de Penn State",
            "sql"=>"", 
      
            
        ]);
    }
    public function actionConsulta8(){
        
        
        $dataProvider = new ActiveDataProvider([
            'query' => Jugadores::find()
                ->where("centro = 'Standford'")
            ]);
        
        return $this->render("consulta8",[
            "resultados"=>$dataProvider,    
            "campos"=>['nombre','edad','dorsal','posicion','nombre_equipo'],
            "titulo"=>"Centro Standford",
            "enunciado"=>"Jugadores pertenecientes al centro de Standford",
            "sql"=>"", 
      
            
        ]);
    }
    public function actionConsulta9(){
        
        
        $dataProvider = new ActiveDataProvider([
            'query' => Jugadores::find()
                ->where("centro = 'Columbia'")
            ]);
        
        return $this->render("consulta9",[
            "resultados"=>$dataProvider,    
            "campos"=>['nombre','edad','dorsal','posicion','nombre_equipo'],
            "titulo"=>"Centro Columbia",
            "enunciado"=>"Jugadores pertenecientes al centro de Columbia",
            "sql"=>"", 
      
            
        ]);
    }
    public function actionConsulta10(){
        
        
        $dataProvider = new ActiveDataProvider([
            'query' => Jugadores::find()
                ->where("centro = 'Clemson'")
            ]);
        
        return $this->render("consulta10",[
            "resultados"=>$dataProvider,    
            "campos"=>['nombre','edad','dorsal','posicion','nombre_equipo'],
            "titulo"=>"Centro Clemson",
            "enunciado"=>"Jugadores pertenecientes al centro de Clemson",
            "sql"=>"", 
      
            
        ]);
    }
}
