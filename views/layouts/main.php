<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap" >
    <?php
    NavBar::begin([
        'brandLabel' => Html ::img('@web/nfl.png', ['alt'=>Yii::$app->name,'class'=>'logo']),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
   
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        
        'items' => [
            ['label' => 'Equipos', 'url' => ['/equipos/index']],
            ['label' => 'Jugadores', 'url' => ['/jugadores/index']],
            ['label' => 'Calendario', 'url' => ['/site/calendario']],
            ['label' => 'MVP', 'url' => ['/site/mvp']],
            ['label' => 'Centros', ['class' =>'glyphicon glyphicon-search'],
                'items' => [
                    ['label' => 'Yale', 'url' => ['/site/consulta2']],
                    ['label' => 'Michigan', 'url' => ['/site/consulta6']],
                    ['label' => 'Penn State', 'url' => ['/site/consulta7']],
                    ['label' => 'Standford', 'url' => ['/site/consulta8']],
                    ['label' => 'Columbia', 'url' => ['/site/consulta9']],
                    ['label' => 'Clemson', 'url' => ['/site/consulta10']],
                ],
            ],
            ['label' => 'Más', ['class' =>'glyphicon glyphicon-search'],
                'items' => [
                    ['label' => 'Entrenadores', 'url' => ['/entrenadores/index']],
                    ['label' => 'Personal', 'url' => ['/personal/index']],
                    ['label' => 'Partidos', 'url' => ['/partidos/index']],
                    ['label' => 'Entrenadores titulares', 'url' => ['/entrenadorespartidos/index']],
                ],
            ],
           
            //['label' => 'Nosotros', 'url' => ['/site/about']],
            //['label' => 'Contacta', 'url' => ['/site/contact']],
            
            
            /*Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )*/
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p>© <?= date('Y') ?> NFL Enterprises LLC. NFL y el diseño del escudo de la NFL son marcas registradas de la National Football League. Los nombres de los equipos, logotipos y diseños de uniformes son marcas registradas de los equipos indicados. Todas las demás marcas comerciales relacionadas con la NFL son marcas comerciales de la National Football League. Imágenes de la NFL © NFL Productions LLC.
            Términos de servicio</p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
