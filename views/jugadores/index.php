<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Jugadores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jugadores-index">

    <h1><?= Html::encode($this->title) ?></h1>
     <br>
    <br>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout'=>"{items}\n{pager}",
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            'dni',
            'nombre',
            'dorsal',
            'centro',
            'sueldo_anual',
            'edad',
            //'posicion',
            'nombre_equipo',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
  <p style="text-align: right">
        <?= Html::a('Crear jugador', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


</div>
