<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Partidos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="partidos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ganador')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MVP')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'TD_totales')->textInput() ?>

    <?= $form->field($model, 'nombre_equipo_local')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre_equipo_visitante')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'TD_locales')->textInput() ?>

    <?= $form->field($model, 'TD_Visitantes')->textInput() ?>

    <?= $form->field($model, 'ExtrasLocales')->textInput() ?>

    <?= $form->field($model, 'ExtrasVisitantes')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
