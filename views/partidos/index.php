<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Partidos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partidos-index">

    <h1><?= Html::encode($this->title) ?></h1>

   <br>
    <br> 

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout'=>"{items}\n{pager}",
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'ganador',
            'MVP',
            //'TD_totales',
            'nombre_equipo_local',
            'nombre_equipo_visitante',
            'TD_locales',
            'TD_Visitantes',
            //'ExtrasLocales',
            //'ExtrasVisitantes',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

<p style="text-align: right">
        <?= Html::a('Crear partido', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

</div>
