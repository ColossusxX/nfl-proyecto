<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Personal';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personal-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <br>
    <br>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout'=>"{items}\n{pager}",
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            'dni',
            'nombre',
            'especializacion',
            'sueldomensual',
            'nombre_equipo',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<p style="text-align: right">
        <?= Html::a('Crear personal', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


</div>
