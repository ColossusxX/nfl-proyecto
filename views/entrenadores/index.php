<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Entrenadores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entrenadores-index">

    <h1><?= Html::encode($this->title) ?></h1>

     <br>
    <br>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,    
        'layout'=>"{items}\n{pager}",
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            'dni',
            'nombre',
            'posicion',
            'sueldomensual',
            'años_entrenando',
            'es_ataque_o_es_defensa',
            //'es_principal',
            //'nombre_equipo',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <p style="text-align: right">
        <?= Html::a('Crear entrenadores', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
</div>
