<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Entrenadorespartidos */

$this->title = 'Crear entrenador titular';
$this->params['breadcrumbs'][] = ['label' => 'Entrenadorespartidos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entrenadorespartidos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
