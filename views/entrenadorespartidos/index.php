<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Entrenadores titulares';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entrenadorespartidos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <br>
    <br>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout'=>"{items}\n{pager}",
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            'nombre_entrenadores',
            'id_partidos',
            'id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    
    <p style="text-align: right">
        <?= Html::a('Crear entrenador titular', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


</div>
