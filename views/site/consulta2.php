<?php use yii\grid\GridView;

?>
<div class="jumbotron">
        <h1>Centro de Yale</h1>

    </div>
<div class="row">
        <div class="col-lg-12">
                <div class="thumbnail">
                    <div class="caption">
                        <h2 style="text-align: center">Jugadores del centro de Yale</h2>
                        <p style="text-align: center">
                            <?=$enunciado?>
                        </p>
                        <p style="text-align: center">
                            <?=$sql?> 
                        </p>
                    </div>
                </div>
</div>
</div>
<?= GridView::widget([
    'dataProvider'=> $resultados,
    'layout'=>"{items}\n{pager}",
    'columns'=>$campos,
]); 
?> 