<?php

/* @var $this yii\web\View */

$this->title = 'National Football League Management';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Calendario de partidos</h1>
    </div>
    <div>
        <html>
               <head><script src="https://www.googletagservices.com/activeview/js/current/osd.js?cb=%2Fr20100101"></script><script type="text/javascript" async="async" src="https://smetrics.nfl.com/b/ss/cbsnfl/10/JS-2.20.0-LATI/s77332487150841?AQB=1&amp;ndh=1&amp;pf=1&amp;callback=s_c_il[1].doPostbacks&amp;et=1&amp;t=27%2F4%2F2020%2017%3A35%3A9%203%20-120&amp;d.&amp;nsid=0&amp;jsonv=1&amp;.d&amp;sdid=54441B2F3EE43F67-541A47FBE2A3B5B8&amp;mid=56140775752980722403340586790118740879&amp;aamlh=6&amp;ce=UTF-8&amp;pageName=nfl.com%3Aschedules%3A2020%3Areg1&amp;g=https%3A%2F%2Fwww.nfl.com%2Fschedules%2F2020%2FREG1%2F&amp;r=https%3A%2F%2Fwww.nfl.com%2Fplayers%2F&amp;c.&amp;visitorAPI=VisitorAPI%20Present&amp;pt.&amp;rdr=0.00&amp;apc=0.00&amp;dns=0.00&amp;tcp=0.00&amp;req=1.08&amp;rsp=0.05&amp;prc=0.71&amp;onl=0.00&amp;tot=1.81&amp;pfi=1&amp;.pt&amp;.c&amp;cc=USD&amp;ch=schedules&amp;events=event1&amp;aamb=RKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y&amp;c2=2020&amp;v2=nfl.com%3Aschedules%3A2020%3Areg1&amp;v3=schedules&amp;v4=false&amp;c8=schedules&amp;v9=nfl.com%3Aschedules%3A2020%3Apre0&amp;c12=desktop&amp;v14=2020&amp;v20=2.0.0.0&amp;v24=desktop&amp;c33=nfl.com&amp;v33=nfl.com&amp;c48=pv&amp;v48=english&amp;c49=NFL.com%20-%20Launch%7Cv20200225%7C2.20.0%7C4.5.2%7C2020-05-26T22%3A51%3A08Z%7CExisting%20ID%7C2.3.0&amp;c50=D%3Dmid&amp;v50=D%3Dmid&amp;c51=D%3Dv49&amp;c52=D%3Dv9&amp;v57=https%3A%2F%2Fwww.nfl.com%2Fschedules%2F2020%2FREG1%2F&amp;c67=reg1&amp;v67=desktop&amp;c71=Launch&amp;c72=D%3Dv57&amp;c73=D%3Dv73&amp;v73=www.nfl.com&amp;v83=reg1&amp;v88=deltatre&amp;v94=no%20cp&amp;s=1536x864&amp;c=24&amp;j=1.6&amp;v=N&amp;k=Y&amp;bw=1536&amp;bh=760&amp;mcorgid=F75C3025512D2C1D0A490D44%40AdobeOrg&amp;AQE=1"></script>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="canonical" href="https://www.nfl.com/schedules/2020/reg1/">
  


<title>NFL 2020 WEEK 1 Schedule | NFL.com</title>
<meta name="description" content="NFL Schedule, Schedule History, Schedule Release, Tickets to NFL Games">

<meta property="og:title" content="">
<meta property="og:description" content="">
<meta property="og:type">
<meta property="og:url" content="https://www.nfl.com/schedules/2020/REG1/?campaign=sf:fanshare:facebook">
<meta property="og:site_name">
<meta property="og:locale" content="en-US">
<meta property="og:image" content="">
<meta property="og:image:type" content="image/jpeg">

<meta name="twitter:card">
<meta name="twitter:title" content="">
<meta name="twitter:description" content="">
<meta name="twitter:image:src" content="">
<meta name="twitter:site" content="@">  
  
  <link href="/manifest.json" rel="manifest">
  


<link rel="icon" href="https://static.www.nfl.com/league/run3dfyjniqxah4ehxfu">
<link href="https://www.nfl.com/compiledassets/css/base.css?_t=b1c216f5a6162b5a219977356a2ea2a6" rel="stylesheet" type="text/css">
  <link href="https://www.nfl.com/compiledassets/theming/6158f5b4cb53e416700c9f5995e408da" rel="stylesheet" type="text/css">
<script>
  // Picture element HTML5 shiv
  document.createElement("picture");
  window.baseUrl = "https://www.nfl.com";
  window.fbappid = "";
  window.marketoHostName = "";
  window.gigyaUrl = 'https://cdns.us1.gigya.com/js/gigya.js?apikey=3_Qa8TkWpIB8ESCBT8tY2TukbVKgO5F6BJVc7N1oComdwFzI7H2L9NOWdm11i_BY9f';
  window.gigyaApiKey = '3_Qa8TkWpIB8ESCBT8tY2TukbVKgO5F6BJVc7N1oComdwFzI7H2L9NOWdm11i_BY9f';
</script>
  
  
<script src="//apv-launcher.minute.ly/api/launcher/MIN-70000.js" async=""></script>
  <script src="//cdn.adsafeprotected.com/iasPET.1.js"></script>
  <script async="" defer="" src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
  <script type="text/javascript" src="https://imasdk.googleapis.com/js/sdkloader/gpt_proxy.js"></script>
  


  <script type="text/javascript">
  window.gptconfig = {};
  var googletag = window.googletag || {};
  googletag.cmd = window.googletag.cmd || [];
  window.gptSlots = [];
  var advPageInfo = {"Sections":["schedules","2020","reg1","landing"],"AdUnitName":"schedules/2020/reg1/landing","IsCustom":false};
  var adUnitName = advPageInfo.AdUnitName;
  var clubAdvId = '/4595/nfl.';
  var leagueAdvId = '/4595/nfl.';
  var testParameter = '';
window.adUnit = leagueAdvId + adUnitName;
  googletag.cmd.push(function () {
    gptconfig.responsiveMappings = {};
    gptconfig.responsiveMappings['leaderboard'] = googletag.sizeMapping().
    addSize([1024, 1], [
    [728, 90],
    [970, 90]
    ]).
    addSize([760, 1], [
    [728, 90]
    ]).
    addSize([100, 1], [
    [320, 50]
    ]).
    addSize([0, 0], []).
    build();

    gptconfig.responsiveMappings['inpage_1'] = googletag.sizeMapping().
    addSize([1024, 1], [
      [728, 90],
      [970, 90]
    ]).
    addSize([760, 1], [
      [728, 90]
    ]).
    addSize([100, 1], [
      [320, 50]
    ]).
    addSize([0, 0], []).
    build();

    gptconfig.responsiveMappings['inpage_2'] = googletag.sizeMapping().
    addSize([1024, 1], [
      [728, 90],
      [970, 90]
    ]).
    addSize([760, 1], [
      [728, 90]
    ]).
    addSize([100, 1], [
      [320, 50]
    ]).
    addSize([0, 0], []).
    build();

    gptconfig.responsiveMappings['mrec'] = googletag.sizeMapping().
    addSize([100,1],[
    [300, 250]
    ]).
    addSize([0, 0], []).
    build();

    gptconfig.responsiveMappings['mrec-photogallery'] = googletag.sizeMapping().
    addSize([1024,1],[
    [300, 600],
    [300, 250]
    ]).
    addSize([100,1],[
    [300, 250]
    ]).
    addSize([0, 0], []).
    build();

    gptconfig.responsiveMappings['mrec-photogallery-list'] = googletag.sizeMapping().
    addSize([1024, 1], [
    [728, 90],
    [970, 250],
    [970, 90]
    ]).
    addSize([100, 1], [
    [300, 250],
    [320, 50]
    ]).
    addSize([0, 0], []).
    build();

    gptconfig.responsiveMappings['package-c'] = googletag.sizeMapping().
    addSize([1024,1],[
    [300, 600]
    ]).
    addSize([100,1],[
    [300, 250]
    ]).
    addSize([0, 0], []).
    build();

    gptconfig.responsiveMappings['package-b'] = googletag.sizeMapping().
    addSize([1024,1],[
    [160, 600]
    ]).
    addSize([100,1],[
    [300, 250]
    ]).
    addSize([0, 0], []).
    build();

    gptconfig.responsiveMappings['presented-by'] = googletag.sizeMapping().
    addSize([1024,1],[
    [100, 30]
    ]).
    addSize([100,1],[
    [100, 30],
    [30, 30]
    ]).
    addSize([0, 0], []).
    build();

    gptconfig.responsiveMappings['companion'] = googletag.sizeMapping().
    addSize([1024,1],[
    [300, 60]
    ]).
    addSize([100,1],[
    [300, 60]
    ]).
    addSize([0, 0], []).
    build();

    gptconfig.responsiveMappings['interstitials'] = googletag.sizeMapping().
    addSize([1024,1],[
    [1, 4]
    ]).
    addSize([760,1],[
    [3, 4]
    ]).
    addSize([100,1],[
    [3, 4]
    ]).
    addSize([0, 0], []).
    build();

    gptconfig.responsiveMappings['game_sched_inline'] = googletag.sizeMapping().
    addSize([1024, 1], [
    [728, 90],
    [970, 90]
    ]).
    addSize([760, 1], [
    [728, 90]
    ]).
    addSize([100, 1], [
    [320, 50]
    ]).
    addSize([0, 0], []).
    build();

    gptconfig.responsiveMappings['teaminfo'] = googletag.sizeMapping().
    addSize([1024, 1], [
      [300, 250]
    ]).
    addSize([760, 1], [
      [300, 250]
    ]).
    addSize([100, 1], [
      [300, 250]
    ]).
    addSize([0, 0], []).
      build();


    gptconfig.responsiveMappings["rightrail"] = googletag.sizeMapping()
      .addSize([992, 0], [
          [300, 250],
      ])
      .addSize(
        [768, 0], [
          [300, 250],
      ])
      .build();

    gptconfig.responsiveMappings["picks-rightrail"] = googletag.sizeMapping()
      .addSize([0, 0], [
        [300, 250],
    ]).build();

    gptconfig.responsiveMappings["prospects-rightrail"] = googletag.sizeMapping()
      .addSize([992, 0], [
          [300, 600]
      ])
      .addSize([768, 0], [
          [728, 90]
      ])
      .addSize([0, 0], [
          [320, 50]
      ])
      .build();

    gptconfig.responsiveMappings["teams-rightrail"] = googletag.sizeMapping()
      .addSize([992, 0], [
          [300, 250]
      ])
      .addSize([768, 0], [
          [728, 90]
      ])
      .addSize([0, 0], [
          [320, 50]
      ])
      .build();

    gptconfig.responsiveMappings['bottom'] = googletag.sizeMapping()
      .addSize([0, 0], [
          [300, 250]
      ])
      .addSize([768, 0], [
          [728, 90]
      ])
      .addSize([1024, 0], [
          [300, 250]
      ])
      .addSize([1200, 0], [
          [728, 90]
      ])
      .build();

    // bigPlay, "drivechartBlings / spon2"
    gptconfig.responsiveMappings['spon2'] = googletag.sizeMapping()
      .addSize([0, 0], [
          [110, 16]
      ])
      .addSize([768, 0], [
          [220, 32]
      ])
      .build();

    // pressByBlingWatchPlay
    gptconfig.responsiveMappings['spon9'] = googletag.sizeMapping()
      .addSize([0, 0], [
          [220, 32]
      ])
      .build();

    gptconfig.responsiveMappings['spon1'] = googletag.sizeMapping()
      .addSize([0, 0], [
          [100, 45]
      ])
      .build();

    gptconfig.responsiveMappings['spon3'] = googletag.sizeMapping()
      .addSize([0, 0], [
          [220, 32]
      ])
      .build();

    gptconfig.responsiveMappings['spon4'] = googletag.sizeMapping()
      .addSize([0, 0], [
          [220, 32]
      ])
      .build();

    gptconfig.responsiveMappings['spon5'] = googletag.sizeMapping()
      .addSize([0, 0], [
          [220, 32]
      ])
      .build();

    gptconfig.responsiveMappings['spon6'] = googletag.sizeMapping()
      .addSize([0, 0], [
          [220, 32]
      ])
      .build();

    gptconfig.responsiveMappings['spon7'] = googletag.sizeMapping()
      .addSize([0, 0], [
          [220, 32]
      ])
      .build();

    gptconfig.responsiveMappings['spon8'] = googletag.sizeMapping()
      .addSize([0, 0], [
          [220, 32]
      ])
      .build();

    gptconfig.responsiveMappings["draft-strip"] = googletag.sizeMapping()
      .addSize([0, 0], [
        [220, 32]
      ]).build();

    gptconfig.responsiveMappings['backgroundSkin'] = googletag.sizeMapping()
      .addSize([1500,1],[
        [1, 1]
      ])
      .addSize([0, 0], [])
      .build();

    gptconfig.responsiveMappings['videoSkin'] = googletag.sizeMapping()
      .addSize([1024, 1], [
        [1, 1]
      ])
      .addSize([0, 0], [])
      .build();

    gptconfig.adSizes = [
    [728, 90],
    [970, 250],
    [970, 90],
    [300, 250],
    [320, 50],
    [300, 600],
    [160, 600],
    [300, 60],
    [300, 100],
    [1, 4],
    [3, 4]
    ];
  });
  </script>
  




<script type="text/javascript">
  var googletag = window.googletag || {};
  googletag.cmd = window.googletag.cmd || [];

  googletag.cmd.push(function() {
    if (document.getElementById('adv_league')) {
      gptSlots.push(
        googletag
            .defineSlot(adUnit, gptconfig.adSizes, 'adv_league')
            .setTargeting('slot', 'top')
            .setTargeting('s1', adUnitName === 'home' ? 'home' : undefined)
            .setTargeting('s2', adUnitName === 'home' ? 'landing' : undefined)
            .defineSizeMapping(gptconfig.responsiveMappings['leaderboard'])
            .addService(googletag.pubads())
        );
    }

    if (document.getElementById('adv_inpage_1')) {
      gptSlots.push(
        googletag
          .defineSlot(adUnit, gptconfig.adSizes, 'adv_inpage_1')
          .setTargeting('slot', 'inpage_1')
          .defineSizeMapping(gptconfig.responsiveMappings['inpage_1'])
          .addService(googletag.pubads())
      );
    }

    if (document.getElementById('adv_inpage_2')) {
      gptSlots.push(
        googletag
          .defineSlot(adUnit, gptconfig.adSizes, 'adv_inpage_2')
          .setTargeting('slot', 'inpage_2')
          .defineSizeMapping(gptconfig.responsiveMappings['inpage_2'])
          .addService(googletag.pubads())
      );
    }

    var sectionsLength;
    if (adUnitName === 'home') {
      googletag.pubads().setTargeting('s1', 'home');
    } else if (sectionsLength = advPageInfo.Sections.length) {
      for (var i = 0; i < sectionsLength; i++) {
        googletag.pubads().setTargeting('s' + (i + 1), advPageInfo.Sections[i]);
      }
    }

    if (testParameter) {
      googletag.pubads().setTargeting('test', testParameter);
    }

    var disableOOP = false;
    if (!disableOOP) {
      gptSlots.push(
        googletag
          .defineSlot(adUnit, gptconfig.adSizes, 'background_skin')
          .setTargeting('slot', 'background')
          .defineSizeMapping(gptconfig.responsiveMappings['backgroundSkin'])
          .addService(googletag.pubads())
      );
    }

    var modulePartnerCampaignID = '';
    if (modulePartnerCampaignID) {
      googletag.pubads().setTargeting('partner', modulePartnerCampaignID);
    }

    var pagePartnerCampaignID = window.partnerCampaignID;
    if (pagePartnerCampaignID && pagePartnerCampaignID.length) {
      googletag.pubads().setTargeting('partner', pagePartnerCampaignID);
    }

    var seriesName = '';
    if (seriesName && advPageInfo.Sections.length) {
      googletag.pubads().setTargeting('s' + (advPageInfo.Sections.length + 1), seriesName);
    }

  });
</script>

<script type="text/javascript">
  var googletag = window.googletag || {};
  googletag.cmd = window.googletag.cmd || [];

  googletag.cmd.push(function () {
    googletag.pubads().enableLazyLoad({
      fetchMarginPercent: 100,
      renderMarginPercent: 25,
      mobileScaling: 2.0
    });
    googletag.pubads().enableAsyncRendering();
    googletag.pubads().collapseEmptyDivs();
    googletag.pubads().disableInitialLoad();
    googletag.enableServices();
  });

  if (googletag && googletag.pubadsReady) {
    googletag.pubads().refresh();
  }
  else {
    window.requestAnimationFrame(awaitPub);
  };

  function awaitPub() {
    var googletagFooter = window.googletag || {};
    googletagFooter.cmd = window.googletag.cmd || [];
    if (googletagFooter && googletagFooter.pubadsReady) {
      googletagFooter.pubads().refresh();
      if (window.animationFrameIdAD) {
        window.cancelAnimationFrame(animationFrameIdAD);
      }
    }
    else {
      window.animationFrameIdAD = window.requestAnimationFrame(awaitPub);
    }
  }
</script>

  


  <script>
    if ('serviceWorker' in navigator) {
      window.addEventListener('load', function () {
        navigator.serviceWorker.getRegistrations().then(function (registeredSevices) {
          for (var i = 0; i < registeredSevices.length; i++) {
            registeredSevices[i].unregister().then(function (unregistered) {
              console.debug('ServiceWorker unregistration: ', unregistered);
            });
          }
        });
      });
    }
  </script>

  <script type="text/javascript">
    window._taboola = window._taboola || [];
    var trackingPageInfo = {"SiteSection":"schedules","SiteSubsection":"2020","PageDetail":"reg1","PageDetailSuffix":""};
    var utag_data = {
      page_type:'landing',
      siteName: 'nfl.com',
      teamAbbr: 'not-available',
      optimizedFor: 'desktop',
      responsiveState: window.innerWidth < 1024 ? 'mobile' : 'desktop',
      siteLanguage: 'English',
      partner: 'Deltatre',
      siteSection: trackingPageInfo.SiteSection,
      siteSubsection: trackingPageInfo.SiteSubsection,
      pageDetail: trackingPageInfo.PageDetail,
      pageState: 'no cp',
      adBlock: 'false',
      adPlacement:'na',
      buildNumber: '2.0.0.0'
    };
    var identityProvider = localStorage.getItem('identityProvider');
    if (identityProvider && identityProvider.length) {
      utag_data['identityProvider'] = JSON.parse(identityProvider);
    }
  </script>

        <script type="text/javascript">
          window.api_token = {"access_token":"eyJhbGciOiJIUzI1NiJ9.eyJ1aWQiOm51bGwsInVzZXJuYW1lIjpudWxsLCJkZXZpY2VJZCI6bnVsbCwiY2xpZW50SWQiOiJScjcwNXJQSFNGZk9RVjl5eHluUlBUMHZJaDJCSWh5UCIsImFkSWQiOm51bGwsImV4cCI6MTU5MDU5NzExMiwiaWF0IjoxNTkwNTkzNTEyfQ.mfV-ReGUH5Kr66qfxf90Y9xnwl6dMgLmLB8F-ztyxvE","token_type":"Bearer","expires_in":3600};
        </script>
      

  <script type="text/javascript">
    window.nflLibHostName = "p.nfltags.com";
    window.environment = "league";
  </script>

  <script type="text/javascript">
    var adBlockEnabled = false;
    try {
      var testAd = document.createElement('div');
      testAd.innerHTML = '&nbsp;';
      testAd.className = 'adsbox';
      document.body.appendChild(testAd);
      if (testAd.offsetHeight === 0) {
        adBlockEnabled = true;
      }
      testAd.remove();
    }
    catch (e) { }
    if (utag_data) {
      utag_data.adBlock = '' + adBlockEnabled;
    }
  </script>
  <script type="text/javascript">
    utag_data.pageDetail = trackingPageInfo.PageDetailSuffix ? (utag_data.pageDetail + ':' + trackingPageInfo.PageDetailSuffix) : utag_data.pageDetail;
    var utag_data = window.utag_data || {};
    var utagParts = [utag_data.siteName, utag_data.siteSection, utag_data.siteSubsection, utag_data.pageDetail];
    var pageName = [];
    for (var i = 0; i < utagParts.length; i++) {
      var part = utagParts[i];
      if (part != undefined) {
        pageName.push(part);
      }
    }
    utag_data.pageName = pageName.join(':');
  </script>

<script type="text/javascript">(function(win, doc, style, timeout) { var STYLE_ID = 'at-body-style'; function getParent() { return doc.getElementsByTagName('head')[0]; } function addStyle(parent, id, def) { if (!parent) {return;} var style = doc.createElement('style'); style.id = id; style.innerHTML = def; parent.appendChild(style); } function removeStyle(parent, id) { if (!parent) {return;} var style = doc.getElementById(id); if (!style) {return;} parent.removeChild(style); } addStyle(getParent(), STYLE_ID, style); setTimeout(function() { removeStyle(getParent(), STYLE_ID); }, timeout); }(window, document, "body {}", 3000));</script>    <script src="//assets.adobedtm.com/a5ea4e8f4344/1648c62d3986/launch-53368d199d70.min.js" async=""></script>

<link rel="preload" href="https://adservice.google.es/adsid/integrator.js?domain=www.nfl.com" as="script"><script type="text/javascript" src="https://adservice.google.es/adsid/integrator.js?domain=www.nfl.com"></script><link rel="preload" href="https://adservice.google.com/adsid/integrator.js?domain=www.nfl.com" as="script"><script type="text/javascript" src="https://adservice.google.com/adsid/integrator.js?domain=www.nfl.com"></script><script src="https://securepubads.g.doubleclick.net/gpt/pubads_impl_2020052101.js" async=""></script><script src="https://snippet.minute.ly/publishers/70000/mi-1.13.9.4.js" type="text/javascript" async="" crossorigin="anonymous"></script><script src="https://assets.adobedtm.com/extensions/EP308220a2a4c4403f97fc1960100db40f/AppMeasurement.min.js" async=""></script><iframe style="display: none;"></iframe><link rel="prefetch" href="https://d0218ee6d2b03e6dd44353638f1b3fec.safeframe.googlesyndication.com/safeframe/1-0-37/html/container.html"><link rel="prefetch" href="https://tpc.googlesyndication.com/safeframe/1-0-37/html/container.html"><script src="https://assets.adobedtm.com/extensions/EP308220a2a4c4403f97fc1960100db40f/AppMeasurement_Module_AudienceManagement.min.js" async=""></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="https://www.nfl.com/compiledassets/js/main.js?_t=11ff89d037be6b1242b9c71f6a639def" src="https://www.nfl.com/compiledassets/js/main.js?_t=11ff89d037be6b1242b9c71f6a639def"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="modules/adobeLaunch" src="https://www.nfl.com/compiledassets/js/fcc7de1102a28061941e3b3f7f9787fc/modules/adobeLaunch.js"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="modules/lazyload" src="https://www.nfl.com/compiledassets/js/fcc7de1102a28061941e3b3f7f9787fc/modules/lazyload.js"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="https://cdns.us1.gigya.com/js/gigya.js?apikey=3_Qa8TkWpIB8ESCBT8tY2TukbVKgO5F6BJVc7N1oComdwFzI7H2L9NOWdm11i_BY9f" src="https://cdns.us1.gigya.com/js/gigya.js?apikey=3_Qa8TkWpIB8ESCBT8tY2TukbVKgO5F6BJVc7N1oComdwFzI7H2L9NOWdm11i_BY9f"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="modules/common/gigyaLoggedInChecker" src="https://www.nfl.com/compiledassets/js/fcc7de1102a28061941e3b3f7f9787fc/modules/common/gigyaLoggedInChecker.js"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="modules/navigation" src="https://www.nfl.com/compiledassets/js/fcc7de1102a28061941e3b3f7f9787fc/modules/navigation.js"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="modules/search" src="https://www.nfl.com/compiledassets/js/fcc7de1102a28061941e3b3f7f9787fc/modules/search.js"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="modules/dropdown" src="https://www.nfl.com/compiledassets/js/fcc7de1102a28061941e3b3f7f9787fc/modules/dropdown.js"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="modules/footer" src="https://www.nfl.com/compiledassets/js/fcc7de1102a28061941e3b3f7f9787fc/modules/footer.js"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="modules/cookieConsent" src="https://www.nfl.com/compiledassets/js/fcc7de1102a28061941e3b3f7f9787fc/modules/cookieConsent.js"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="modules/unsupportedBrowsers" src="https://www.nfl.com/compiledassets/js/fcc7de1102a28061941e3b3f7f9787fc/modules/unsupportedBrowsers.js"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="modules/noconflict" src="https://www.nfl.com/compiledassets/js/fcc7de1102a28061941e3b3f7f9787fc/modules/noconflict.js"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="addons/services/adobeLaunchService" src="https://www.nfl.com/compiledassets/js/fcc7de1102a28061941e3b3f7f9787fc/addons/services/adobeLaunchService.js"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="modules/iconHelper" src="https://www.nfl.com/compiledassets/js/fcc7de1102a28061941e3b3f7f9787fc/modules/iconHelper.js"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="modules/helpers/browserHelper" src="https://www.nfl.com/compiledassets/js/fcc7de1102a28061941e3b3f7f9787fc/modules/helpers/browserHelper.js"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="jquery_ui" src="https://www.nfl.com/compiledassets/js/fcc7de1102a28061941e3b3f7f9787fc/vendor/jquery/jquery-ui.min.js"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="modules/core/tabsCore" src="https://www.nfl.com/compiledassets/js/fcc7de1102a28061941e3b3f7f9787fc/modules/core/tabsCore.js"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="modules/services/deferredService" src="https://www.nfl.com/compiledassets/js/fcc7de1102a28061941e3b3f7f9787fc/modules/services/deferredService.js"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="jquery" src="https://www.nfl.com/compiledassets/js/fcc7de1102a28061941e3b3f7f9787fc/vendor/jquery/jquery-3.2.1.min.js"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="cookies" src="https://www.nfl.com/compiledassets/js/fcc7de1102a28061941e3b3f7f9787fc/vendor/js-cookie/js.cookie.min.js"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="jquery_visible" src="https://www.nfl.com/compiledassets/js/fcc7de1102a28061941e3b3f7f9787fc/vendor/jquery/plugins/jquery.visible.min.js"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="autocomplete" src="https://www.nfl.com/compiledassets/js/fcc7de1102a28061941e3b3f7f9787fc/vendor/autocomplete/jquery.autocomplete.min.js"></script><script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="cookieconsent" src="https://www.nfl.com/compiledassets/js/fcc7de1102a28061941e3b3f7f9787fc/vendor/cookieconsent/cookieconsent.min.js"></script></head>
         <body class="nfl-has-secondary-nav"><div role="dialog" aria-label="cookieconsent" aria-describedby="cookieconsent:desc" class="cc-window d3-o-cookie cc-banner cc-type-info cc-theme-block cc-bottom  cc-invisible" style=""><span id="cookieconsent:desc" class="d3-o-cookie-message">We use our own and third-party cookies to improve your experience and our services. By continuing and using the site, including by remaining on the landing page, you consent to the use of cookies. <a aria-label="learn more about cookies" tabindex="0" class="d3-o-cookie-link" href="https://www.nfl.com/legal/privacy" target="_blank">Click here for more information on our <strong>Cookie Policy</strong>.</a></span><div class="cc-compliance"><a aria-label="dismiss cookie message" tabindex="0" class="cc-btn cc-dismiss d3-o-cookie-btn d3-o-cookie-btn--dismiss">Got it!</a></div></div>
  <a class="d3-u-block-bypass" href="#main-content" tabindex="0">
    <span>Skip to main content</span>
  </a>
    <!-- Interstitial OOP SLOT  -->
    <div id="oop" style="text-align: center;"></div>
      <script type="text/javascript">
        var googletag = window.googletag || {};
        googletag.cmd = window.googletag.cmd || [];
        googletag.cmd.push(function () {
          googletag.display('oop');
        });
      </script>
    <!-- Background-Skin SLOT -->
    <div id="background_skin" style="text-align: center; display: none;" data-google-query-id="CODRn6iv1OkCFY6LUQodO-4LFw"><div id="google_ads_iframe_/4595/nfl.schedules/2020/reg1/landing_1__container__" style="border: 0pt none; width: 1px; height: 1px;"></div></div>
    <script type="text/javascript">
      var googletag = window.googletag || {};
      googletag.cmd = window.googletag.cmd || [];
      googletag.cmd.push(function () {
        googletag.display('background_skin');
      });
    </script>
  

</header>


  <main role="main" id="main-content" style="font-family: Arial">




<h1 aria-label="NFL $(schedule_seasoninfo) Schedule | NFL.com" class="nfl-o-page-title nfl-o-page-title--visuallyhidden">NFL $(schedule_seasoninfo) Schedule | NFL.com</h1>

      <section class="d3-l-grid--outer d3-l-section-row">
        <div class="d3-l-grid--inner">
          

<div class="d3-l-col__col-12" data-min-enable="video page" data-min-method="true">
  <div class="d3-l-section-header">


      <div class="d3-o-section-adv">
          <span class="d3-o-section-adv__label">
            Presented by
          </span>

        <script type="text/javascript">
            var googletag = window.googletag || {};
            googletag.cmd = window.googletag.cmd || [];
            var slot = slot || {}; //some modules call this more than once so we check if slot is already set.
            googletag.cmd.push(function () {
              gptSlots.push(
                slot['834972dd-ffa4-4226-b61f-0e9d18eba2c3'] = googletag
                  .defineSlot(adUnit, gptconfig.adSizes, '834972dd-ffa4-4226-b61f-0e9d18eba2c3')
                  .setTargeting('partner', 'visa-schedule')
                  .setTargeting('slot', 'logo')
                  .defineSizeMapping(gptconfig.responsiveMappings['presented-by']).addService(googletag.pubads())
              );
            });
          
              window['834972dd-ffa4-4226-b61f-0e9d18eba2c3_refreshAdv'] = function() {
                googletag.pubads().refresh([slot['834972dd-ffa4-4226-b61f-0e9d18eba2c3']],{changeCorrelator: false});
              };</script><div class="d3-o-adv-block--refreshable d3-o-adv-block js-lightbox-refresh-ignore" id="834972dd-ffa4-4226-b61f-0e9d18eba2c3" data-google-query-id="CJuInqiv1OkCFcHg1QodsnMM3A" style="display: none;"><div id="google_ads_iframe_/4595/nfl.schedules/2020/reg1/landing_2__container__" style="border: 0pt none; width: 100px; height: 30px;"></div></div><script type="text/javascript">
        var googletag = window.googletag || {};
        googletag.cmd = window.googletag.cmd || [];
        googletag.cmd.push(function () {
          googletag.display('834972dd-ffa4-4226-b61f-0e9d18eba2c3');
        });</script>
      </div>
</div>
</div>

          <script>window.partnerCampaignID = window.partnerCampaignID || 'visa-schedule';</script>
        </div>
      </section>
    
        <section class="d3-l-grid--outer d3-l-adv-row">
          <h2 class="d3-o-section-title"> Advertising</h2>
          <div class="d3-l-grid--inner">
            <script></script><div class="  d3-o-adv-block" data-id="adv_inpage_1" id="adv_inpage_1" data-google-query-id="CN_Anqiv1OkCFcetUQod9QEBoA"><div id="google_ads_iframe_/4595/nfl.schedules/2020/reg1/landing_0__container__" style="border: 0pt none; display: inline-block; width: 728px; height: 90px;"><iframe frameborder="0" src="https://d0218ee6d2b03e6dd44353638f1b3fec.safeframe.googlesyndication.com/safeframe/1-0-37/html/container.html" id="google_ads_iframe_/4595/nfl.schedules/2020/reg1/landing_0" title="3rd party ad content" name="" scrolling="no" marginwidth="0" marginheight="0" width="728" height="90" data-is-safeframe="true" sandbox="allow-forms allow-pointer-lock allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts allow-top-navigation-by-user-activation" data-google-container-id="1" style="border: 0px; vertical-align: bottom;" data-load-complete="true"></iframe></div></div><script type="text/javascript">
        var googletag = window.googletag || {};
        googletag.cmd = window.googletag.cmd || [];
        googletag.cmd.push(function () {
          googletag.display('adv_inpage_1');
        });</script>
          </div>
        </section>

    



    <section class="d3-l-grid--outer d3-l-section-row nfl-o-matchup-group">
      <div class="d3-l-grid--inner">
        <div class="d3-l-col__col-12" data-min-enable="video page" data-min-method="true">
          







  <h2 class="d3-o-section-title">Thursday, September 10th</h2><div class="nfl-c-matchup-strip nfl-c-matchup-strip--pre-game">
    <div class="nfl-c-matchup-strip__left-area">
        <div class="nfl-c-matchup-strip__game-info">
          <p class="nfl-c-matchup-strip__date-info">
            <span class="nfl-c-matchup-strip__date-time">
5:20 PM            </span>

            <span class="nfl-c-matchup-strip__date-timezone">
PDT            </span>
          </p>
          <p class="nfl-c-matchup-strip__networks">NBC</p>
        </div>
          
<a class="nfl-c-matchup-strip__game"aria-label="Go to Chiefs vs Texans game page">  <div class="nfl-c-matchup-strip__team nfl-c-matchup-strip__team--opponent">
    <p class="nfl-c-matchup-strip__team-name">
      

      <span class="nfl-c-matchup-strip__team-logo">
        <picture><!--[if IE 9]><video st    yle="display: none; "><![endif]--><source media="(min-width:1024px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/bpx88i8nw4nnabuq0oob"><source media="(min-width:768px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/bpx88i8nw4nnabuq0oob"><source srcset="https://static.www.nfl.com/image/private/t_q-best/league/bpx88i8nw4nnabuq0oob"><!--[if IE 9]></video><![endif]--><img alt="Texans logo" class="img-responsive" role="presentation" src="https://static.www.nfl.com/image/private/t_q-best/league/bpx88i8nw4nnabuq0oob"></picture>
      </span>

      <span class="nfl-c-matchup-strip__team-abbreviation">
        HOU
      </span>
      <span class="nfl-c-matchup-strip__team-fullname">
        Texans
      </span>
    </p>
    <div class="nfl-c-matchup-strip__record"></div>
  </div>
  <div class="nfl-c-matchup-strip__team-separator">
    



<span class="nfl-o-icon nfl-o-icon--medium ">
  <svg class="nfl-o-icon--nfl-at" viewBox="0 0 24 24" aria-hidden="true">
    <use xlink:href="#nfl-at"></use>
  </svg>
</span>

  </div>
  <div class="nfl-c-matchup-strip__team">
    <p class="nfl-c-matchup-strip__team-name">
      

      <span class="nfl-c-matchup-strip__team-logo">
        <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source media="(min-width:1024px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/ujshjqvmnxce8m4obmvs"><source media="(min-width:768px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/ujshjqvmnxce8m4obmvs"><source srcset="https://static.www.nfl.com/image/private/t_q-best/league/ujshjqvmnxce8m4obmvs"><!--[if IE 9]></video><![endif]--><img alt="Chiefs logo" class="img-responsive" role="presentation" src="https://static.www.nfl.com/image/private/t_q-best/league/ujshjqvmnxce8m4obmvs"></picture>
      </span>

      <span class="nfl-c-matchup-strip__team-abbreviation">
        KC
      </span>
      <span class="nfl-c-matchup-strip__team-fullname">
        Chiefs
      </span>
    </p>
    <div class="nfl-c-matchup-strip__record"></div>
  </div>
</a>
        </div>
    <div class="nfl-c-matchup-strip__right-area">
                  <a class="nfl-o-cta nfl-o-cta--link">
           
          </a>      
<span class="nfl-o-icon nfl-o-icon--medium ">
  <svg class="nfl-o-icon--tickets" viewBox="0 0 24 24" aria-hidden="true">
   
  </svg>
</span>

  
          
          </a>
      </div>
        </div>
        </div>
      </div>
    </section>
    <section class="d3-l-grid--outer d3-l-section-row nfl-o-matchup-group">
      <div class="d3-l-grid--inner">
        <div class="d3-l-col__col-12" data-min-enable="video page" data-min-method="true">
          <h2 class="d3-o-section-title">Sunday, September 13th</h2>







  <div class="nfl-c-matchup-strip nfl-c-matchup-strip--pre-game">
    <div class="nfl-c-matchup-strip__left-area">
        <div class="nfl-c-matchup-strip__game-info">
          <p class="nfl-c-matchup-strip__date-info">
            <span class="nfl-c-matchup-strip__date-time">
10:00 AM            </span>

            <span class="nfl-c-matchup-strip__date-timezone">
PDT            </span>
          </p>
          <p class="nfl-c-matchup-strip__networks">FOX</p>
        </div>
          
<a class="nfl-c-matchup-strip__game" aria-label="Go to Redskins vs Eagles game page">  <div class="nfl-c-matchup-strip__team nfl-c-matchup-strip__team--opponent">
    <p class="nfl-c-matchup-strip__team-name">
      

      <span class="nfl-c-matchup-strip__team-logo">
        <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source media="(min-width:1024px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/puhrqgj71gobgdkdo6uq"><source media="(min-width:768px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/puhrqgj71gobgdkdo6uq"><source srcset="https://static.www.nfl.com/image/private/t_q-best/league/puhrqgj71gobgdkdo6uq"><!--[if IE 9]></video><![endif]--><img alt="Eagles logo" class="img-responsive" role="presentation" src="https://static.www.nfl.com/image/private/t_q-best/league/puhrqgj71gobgdkdo6uq"></picture>
      </span>

      <span class="nfl-c-matchup-strip__team-abbreviation">
        PHI
      </span>
      <span class="nfl-c-matchup-strip__team-fullname">
        Eagles
      </span>
    </p>
    <div class="nfl-c-matchup-strip__record"></div>
  </div>
  <div class="nfl-c-matchup-strip__team-separator">
    



<span class="nfl-o-icon nfl-o-icon--medium ">
  <svg class="nfl-o-icon--nfl-at" viewBox="0 0 24 24" aria-hidden="true">
    <use xlink:href="#nfl-at"></use>
  </svg>
</span>

  </div>
  <div class="nfl-c-matchup-strip__team">
    <p class="nfl-c-matchup-strip__team-name">
      

      <span class="nfl-c-matchup-strip__team-logo">
        <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source media="(min-width:1024px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/haxrowry8puwbrixjdmc"><source media="(min-width:768px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/haxrowry8puwbrixjdmc"><source srcset="https://static.www.nfl.com/image/private/t_q-best/league/haxrowry8puwbrixjdmc"><!--[if IE 9]></video><![endif]--><img alt="Redskins logo" class="img-responsive" role="presentation" src="https://static.www.nfl.com/image/private/t_q-best/league/haxrowry8puwbrixjdmc"></picture>
      </span>

      <span class="nfl-c-matchup-strip__team-abbreviation">
        WAS
      </span>
      <span class="nfl-c-matchup-strip__team-fullname">
        Redskins
      </span>
    </p>
    <div class="nfl-c-matchup-strip__record"></div>
  </div>
</a>
        </div>
    <div class="nfl-c-matchup-strip__right-area">
                  <a class="nfl-o-cta nfl-o-cta--link" href="/ways-to-watch/">
            
          </a>
            



<span class="nfl-o-icon nfl-o-icon--medium ">
  <svg class="nfl-o-icon--tickets" viewBox="0 0 24 24" aria-hidden="true">
  </svg>
</span>
        
      </div>
        </div>



  <div class="nfl-c-matchup-strip nfl-c-matchup-strip--pre-game">
    <div class="nfl-c-matchup-strip__left-area">
        <div class="nfl-c-matchup-strip__game-info">
          <p class="nfl-c-matchup-strip__date-info">
            <span class="nfl-c-matchup-strip__date-time">
10:00 AM            </span>

            <span class="nfl-c-matchup-strip__date-timezone">
PDT            </span>
          </p>
          <p class="nfl-c-matchup-strip__networks">CBS</p>
        </div>
          
<a class="nfl-c-matchup-strip__game"  aria-label="Go to Patriots vs Dolphins game page">  <div class="nfl-c-matchup-strip__team nfl-c-matchup-strip__team--opponent">
    <p class="nfl-c-matchup-strip__team-name">
      

      <span class="nfl-c-matchup-strip__team-logo">
        <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source media="(min-width:1024px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/lits6p8ycthy9to70bnt"><source media="(min-width:768px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/lits6p8ycthy9to70bnt"><source srcset="https://static.www.nfl.com/image/private/t_q-best/league/lits6p8ycthy9to70bnt"><!--[if IE 9]></video><![endif]--><img alt="Dolphins logo" class="img-responsive" role="presentation" src="https://static.www.nfl.com/image/private/t_q-best/league/lits6p8ycthy9to70bnt"></picture>
      </span>

      <span class="nfl-c-matchup-strip__team-abbreviation">
        MIA
      </span>
      <span class="nfl-c-matchup-strip__team-fullname">
        Dolphins
      </span>
    </p>
    <div class="nfl-c-matchup-strip__record"></div>
  </div>
  <div class="nfl-c-matchup-strip__team-separator">
    



<span class="nfl-o-icon nfl-o-icon--medium ">
  <svg class="nfl-o-icon--nfl-at" viewBox="0 0 24 24" aria-hidden="true">
    <use xlink:href="#nfl-at"></use>
  </svg>
</span>

  </div>
  <div class="nfl-c-matchup-strip__team">
    <p class="nfl-c-matchup-strip__team-name">
      

      <span class="nfl-c-matchup-strip__team-logo">
        <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source media="(min-width:1024px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/moyfxx3dq5pio4aiftnc"><source media="(min-width:768px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/moyfxx3dq5pio4aiftnc"><source srcset="https://static.www.nfl.com/image/private/t_q-best/league/moyfxx3dq5pio4aiftnc"><!--[if IE 9]></video><![endif]--><img alt="Patriots logo" class="img-responsive" role="presentation" src="https://static.www.nfl.com/image/private/t_q-best/league/moyfxx3dq5pio4aiftnc"></picture>
      </span>

      <span class="nfl-c-matchup-strip__team-abbreviation">
        NE
      </span>
      <span class="nfl-c-matchup-strip__team-fullname">
        Patriots
      </span>
    </p>
    <div class="nfl-c-matchup-strip__record"></div>
  </div>
</a>
        </div>
    <div class="nfl-c-matchup-strip__right-area">
                  <a class="nfl-o-cta nfl-o-cta--link" href="/ways-to-watch/">
          </a>
         
<span class="nfl-o-icon nfl-o-icon--medium ">
  <svg class="nfl-o-icon--tickets" viewBox="0 0 24 24" aria-hidden="true">
    
  </svg>
</span>
          </a>
      </div>
        </div>



  <div class="nfl-c-matchup-strip nfl-c-matchup-strip--pre-game">
    <div class="nfl-c-matchup-strip__left-area">
        <div class="nfl-c-matchup-strip__game-info">
          <p class="nfl-c-matchup-strip__date-info">
            <span class="nfl-c-matchup-strip__date-time">
10:00 AM            </span>

            <span class="nfl-c-matchup-strip__date-timezone">
PDT            </span>
          </p>
          <p class="nfl-c-matchup-strip__networks">FOX</p>
        </div>
          
<a class="nfl-c-matchup-strip__game" aria-label="Go to Vikings vs Packers game page">  <div class="nfl-c-matchup-strip__team nfl-c-matchup-strip__team--opponent">
    <p class="nfl-c-matchup-strip__team-name">
      

      <span class="nfl-c-matchup-strip__team-logo">
        <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source media="(min-width:1024px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/gppfvr7n8gljgjaqux2x"><source media="(min-width:768px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/gppfvr7n8gljgjaqux2x"><source srcset="https://static.www.nfl.com/image/private/t_q-best/league/gppfvr7n8gljgjaqux2x"><!--[if IE 9]></video><![endif]--><img alt="Packers logo" class="img-responsive" role="presentation" src="https://static.www.nfl.com/image/private/t_q-best/league/gppfvr7n8gljgjaqux2x"></picture>
      </span>

      <span class="nfl-c-matchup-strip__team-abbreviation">
        GB
      </span>
      <span class="nfl-c-matchup-strip__team-fullname">
        Packers
      </span>
    </p>
    <div class="nfl-c-matchup-strip__record"></div>
  </div>
  <div class="nfl-c-matchup-strip__team-separator">
    



<span class="nfl-o-icon nfl-o-icon--medium ">
  <svg class="nfl-o-icon--nfl-at" viewBox="0 0 24 24" aria-hidden="true">
    <use xlink:href="#nfl-at"></use>
  </svg>
</span>

  </div>
  <div class="nfl-c-matchup-strip__team">
    <p class="nfl-c-matchup-strip__team-name">
      

      <span class="nfl-c-matchup-strip__team-logo">
        <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source media="(min-width:1024px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/havlqjq5ynlvco4l9pgu"><source media="(min-width:768px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/havlqjq5ynlvco4l9pgu"><source srcset="https://static.www.nfl.com/image/private/t_q-best/league/havlqjq5ynlvco4l9pgu"><!--[if IE 9]></video><![endif]--><img alt="Vikings logo" class="img-responsive" role="presentation" src="https://static.www.nfl.com/image/private/t_q-best/league/havlqjq5ynlvco4l9pgu"></picture>
      </span>

      <span class="nfl-c-matchup-strip__team-abbreviation">
        MIN
      </span>
      <span class="nfl-c-matchup-strip__team-fullname">
        Vikings
      </span>
    </p>
    <div class="nfl-c-matchup-strip__record"></div>
  </div>
</a>
        </div>
    <div class="nfl-c-matchup-strip__right-area">
                  <a class="nfl-o-cta nfl-o-cta--link" href="/ways-to-watch/">
          </a>            
<span class="nfl-o-icon nfl-o-icon--medium ">
  <svg class="nfl-o-icon--tickets" viewBox="0 0 24 24" aria-hidden="true">
  </svg>
</span>
          </a>
      </div>
        </div>




  <div class="nfl-c-matchup-strip nfl-c-matchup-strip--pre-game">
    <div class="nfl-c-matchup-strip__left-area">
        <div class="nfl-c-matchup-strip__game-info">
          <p class="nfl-c-matchup-strip__date-info">
            <span class="nfl-c-matchup-strip__date-time">
10:00 AM            </span>

            <span class="nfl-c-matchup-strip__date-timezone">
PDT            </span>
          </p>
          <p class="nfl-c-matchup-strip__networks">CBS</p>
        </div>
          
<a class="nfl-c-matchup-strip__game"  aria-label="Go to Jaguars vs Colts game page">  <div class="nfl-c-matchup-strip__team nfl-c-matchup-strip__team--opponent">
    <p class="nfl-c-matchup-strip__team-name">
      

      <span class="nfl-c-matchup-strip__team-logo">
        <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source media="(min-width:1024px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/ketwqeuschqzjsllbid5"><source media="(min-width:768px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/ketwqeuschqzjsllbid5"><source srcset="https://static.www.nfl.com/image/private/t_q-best/league/ketwqeuschqzjsllbid5"><!--[if IE 9]></video><![endif]--><img alt="Colts logo" class="img-responsive" role="presentation" src="https://static.www.nfl.com/image/private/t_q-best/league/ketwqeuschqzjsllbid5"></picture>
      </span>

      <span class="nfl-c-matchup-strip__team-abbreviation">
        IND
      </span>
      <span class="nfl-c-matchup-strip__team-fullname">
        Colts
      </span>
    </p>
    <div class="nfl-c-matchup-strip__record"></div>
  </div>
  <div class="nfl-c-matchup-strip__team-separator">
    



<span class="nfl-o-icon nfl-o-icon--medium ">
  <svg class="nfl-o-icon--nfl-at" viewBox="0 0 24 24" aria-hidden="true">
    <use xlink:href="#nfl-at"></use>
  </svg>
</span>

  </div>
  <div class="nfl-c-matchup-strip__team">
    <p class="nfl-c-matchup-strip__team-name">
      

      <span class="nfl-c-matchup-strip__team-logo">
        <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source media="(min-width:1024px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/qycbib6ivrm9dqaexryk"><source media="(min-width:768px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/qycbib6ivrm9dqaexryk"><source srcset="https://static.www.nfl.com/image/private/t_q-best/league/qycbib6ivrm9dqaexryk"><!--[if IE 9]></video><![endif]--><img alt="Jaguars logo" class="img-responsive" role="presentation" src="https://static.www.nfl.com/image/private/t_q-best/league/qycbib6ivrm9dqaexryk"></picture>
      </span>

      <span class="nfl-c-matchup-strip__team-abbreviation">
        JAX
      </span>
      <span class="nfl-c-matchup-strip__team-fullname">
        Jaguars
      </span>
    </p>
    <div class="nfl-c-matchup-strip__record"></div>
  </div>
</a>
        </div>
    <div class="nfl-c-matchup-strip__right-area">
                  <a class="nfl-o-cta nfl-o-cta--link" href="/ways-to-watch/">            
          </a>           
<span class="nfl-o-icon nfl-o-icon--medium ">
  <svg class="nfl-o-icon--tickets" viewBox="0 0 24 24" aria-hidden="true">
  </svg>
</span>
          </a>
      </div>
        </div>


  <div class="nfl-c-matchup-strip nfl-c-matchup-strip--pre-game">
    <div class="nfl-c-matchup-strip__left-area">
        <div class="nfl-c-matchup-strip__game-info">
          <p class="nfl-c-matchup-strip__date-info">
            <span class="nfl-c-matchup-strip__date-time">
10:00 AM            </span>

            <span class="nfl-c-matchup-strip__date-timezone">
PDT            </span>
          </p>
          <p class="nfl-c-matchup-strip__networks">FOX</p>
        </div>
          
<a class="nfl-c-matchup-strip__game"  aria-label="Go to Lions vs Bears game page">  <div class="nfl-c-matchup-strip__team nfl-c-matchup-strip__team--opponent">
    <p class="nfl-c-matchup-strip__team-name">
      

      <span class="nfl-c-matchup-strip__team-logo">
        <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source media="(min-width:1024px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/ra0poq2ivwyahbaq86d2"><source media="(min-width:768px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/ra0poq2ivwyahbaq86d2"><source srcset="https://static.www.nfl.com/image/private/t_q-best/league/ra0poq2ivwyahbaq86d2"><!--[if IE 9]></video><![endif]--><img alt="Bears logo" class="img-responsive" role="presentation" src="https://static.www.nfl.com/image/private/t_q-best/league/ra0poq2ivwyahbaq86d2"></picture>
      </span>

      <span class="nfl-c-matchup-strip__team-abbreviation">
        CHI
      </span>
      <span class="nfl-c-matchup-strip__team-fullname">
        Bears
      </span>
    </p>
    <div class="nfl-c-matchup-strip__record"></div>
  </div>
  <div class="nfl-c-matchup-strip__team-separator">
    



<span class="nfl-o-icon nfl-o-icon--medium ">
  <svg class="nfl-o-icon--nfl-at" viewBox="0 0 24 24" aria-hidden="true">
    <use xlink:href="#nfl-at"></use>
  </svg>
</span>

  </div>
  <div class="nfl-c-matchup-strip__team">
    <p class="nfl-c-matchup-strip__team-name">
      

      <span class="nfl-c-matchup-strip__team-logo">
        <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source media="(min-width:1024px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/ocvxwnapdvwevupe4tpr"><source media="(min-width:768px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/ocvxwnapdvwevupe4tpr"><source srcset="https://static.www.nfl.com/image/private/t_q-best/league/ocvxwnapdvwevupe4tpr"><!--[if IE 9]></video><![endif]--><img alt="Lions logo" class="img-responsive" role="presentation" src="https://static.www.nfl.com/image/private/t_q-best/league/ocvxwnapdvwevupe4tpr"></picture>
      </span>

      <span class="nfl-c-matchup-strip__team-abbreviation">
        DET
      </span>
      <span class="nfl-c-matchup-strip__team-fullname">
        Lions
      </span>
    </p>
    <div class="nfl-c-matchup-strip__record"></div>
  </div>
</a>
        </div>
    <div class="nfl-c-matchup-strip__right-area">
                  <a class="nfl-o-cta nfl-o-cta--link" href="/ways-to-watch/">         
          </a>   
<span class="nfl-o-icon nfl-o-icon--medium ">
  <svg class="nfl-o-icon--tickets" viewBox="0 0 24 24" aria-hidden="true">
  </svg>
</span>
      </div>
        </div>
<script type="text/javascript">
            var googletag = window.googletag || {};
            googletag.cmd = window.googletag.cmd || [];
            var slot = slot || {}; //some modules call this more than once so we check if slot is already set.
            googletag.cmd.push(function () {
              gptSlots.push(
                slot['a9f7372d-7fa5-469d-9b38-744e6f49fe3d'] = googletag
                  .defineSlot(adUnit, gptconfig.adSizes, 'a9f7372d-7fa5-469d-9b38-744e6f49fe3d')
                  .setTargeting('slot', 'game_sched_inline')
                  .defineSizeMapping(gptconfig.responsiveMappings['game_sched_inline'])
.addService(googletag.pubads())
              );});</script><div class="  d3-o-adv-block" data-id="a9f7372d-7fa5-469d-9b38-744e6f49fe3d" id="a9f7372d-7fa5-469d-9b38-744e6f49fe3d" data-google-query-id="CMjrraiv1OkCFcmzUQod6OMMrA"><div id="google_ads_iframe_/4595/nfl.schedules/2020/reg1/landing_3__container__" style="border: 0pt none; display: inline-block; width: 728px; height: 90px;"><iframe frameborder="0" src="https://d0218ee6d2b03e6dd44353638f1b3fec.safeframe.googlesyndication.com/safeframe/1-0-37/html/container.html" id="google_ads_iframe_/4595/nfl.schedules/2020/reg1/landing_3" title="3rd party ad content" name="" scrolling="no" marginwidth="0" marginheight="0" width="728" height="90" data-is-safeframe="true" sandbox="allow-forms allow-pointer-lock allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts allow-top-navigation-by-user-activation" data-google-container-id="3" style="border: 0px; vertical-align: bottom;" data-load-complete="true"></iframe></div></div><script type="text/javascript">
        var googletag = window.googletag || {};
        googletag.cmd = window.googletag.cmd || [];
        googletag.cmd.push(function () {
          googletag.display('a9f7372d-7fa5-469d-9b38-744e6f49fe3d');
        });</script>






  <div class="nfl-c-matchup-strip nfl-c-matchup-strip--pre-game">
    <div class="nfl-c-matchup-strip__left-area">
        <div class="nfl-c-matchup-strip__game-info">
          <p class="nfl-c-matchup-strip__date-info">
            <span class="nfl-c-matchup-strip__date-time">
10:00 AM            </span>

            <span class="nfl-c-matchup-strip__date-timezone">
PDT            </span>
          </p>
          <p class="nfl-c-matchup-strip__networks">CBS</p>
        </div>
          
<a class="nfl-c-matchup-strip__game"  aria-label="Go to Panthers vs Raiders game page">  <div class="nfl-c-matchup-strip__team nfl-c-matchup-strip__team--opponent">
    <p class="nfl-c-matchup-strip__team-name">
      

      <span class="nfl-c-matchup-strip__team-logo">
        <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source media="(min-width:1024px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/y2saimpyifuahldhzetn"><source media="(min-width:768px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/y2saimpyifuahldhzetn"><source srcset="https://static.www.nfl.com/image/private/t_q-best/league/y2saimpyifuahldhzetn"><!--[if IE 9]></video><![endif]--><img alt="Raiders logo" class="img-responsive" role="presentation" src="https://static.www.nfl.com/image/private/t_q-best/league/y2saimpyifuahldhzetn"></picture>
      </span>

      <span class="nfl-c-matchup-strip__team-abbreviation">
        LV
      </span>
      <span class="nfl-c-matchup-strip__team-fullname">
        Raiders
      </span>
    </p>
    <div class="nfl-c-matchup-strip__record"></div>
  </div>
  <div class="nfl-c-matchup-strip__team-separator">
    



<span class="nfl-o-icon nfl-o-icon--medium ">
  <svg class="nfl-o-icon--nfl-at" viewBox="0 0 24 24" aria-hidden="true">
    <use xlink:href="#nfl-at"></use>
  </svg>
</span>

  </div>
  <div class="nfl-c-matchup-strip__team">
    <p class="nfl-c-matchup-strip__team-name">
      

      <span class="nfl-c-matchup-strip__team-logo">
        <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source media="(min-width:1024px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/ervfzgrqdpnc7lh5gqwq"><source media="(min-width:768px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/ervfzgrqdpnc7lh5gqwq"><source srcset="https://static.www.nfl.com/image/private/t_q-best/league/ervfzgrqdpnc7lh5gqwq"><!--[if IE 9]></video><![endif]--><img alt="Panthers logo" class="img-responsive" role="presentation" src="https://static.www.nfl.com/image/private/t_q-best/league/ervfzgrqdpnc7lh5gqwq"></picture>
      </span>

      <span class="nfl-c-matchup-strip__team-abbreviation">
        CAR
      </span>
      <span class="nfl-c-matchup-strip__team-fullname">
        Panthers
      </span>
    </p>
    <div class="nfl-c-matchup-strip__record"></div>
  </div>
</a>
        </div>
    <div class="nfl-c-matchup-strip__right-area">
                  <a class="nfl-o-cta nfl-o-cta--link" href="/ways-to-watch/">
          </a>            
<span class="nfl-o-icon nfl-o-icon--medium ">
  <svg class="nfl-o-icon--tickets" viewBox="0 0 24 24" aria-hidden="true">
  </svg>
</span>
      </div>
        </div>



  <div class="nfl-c-matchup-strip nfl-c-matchup-strip--pre-game">
    <div class="nfl-c-matchup-strip__left-area">
        <div class="nfl-c-matchup-strip__game-info">
          <p class="nfl-c-matchup-strip__date-info">
            <span class="nfl-c-matchup-strip__date-time">
10:00 AM            </span>

            <span class="nfl-c-matchup-strip__date-timezone">
PDT            </span>
          </p>
          <p class="nfl-c-matchup-strip__networks">CBS</p>
        </div>
          
<a class="nfl-c-matchup-strip__game" aria-label="Go to Bills vs Jets game page">  <div class="nfl-c-matchup-strip__team nfl-c-matchup-strip__team--opponent">
    <p class="nfl-c-matchup-strip__team-name">
      

      <span class="nfl-c-matchup-strip__team-logo">
        <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source media="(min-width:1024px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/ekijosiae96gektbo4iw"><source media="(min-width:768px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/ekijosiae96gektbo4iw"><source srcset="https://static.www.nfl.com/image/private/t_q-best/league/ekijosiae96gektbo4iw"><!--[if IE 9]></video><![endif]--><img alt="Jets logo" class="img-responsive" role="presentation" src="https://static.www.nfl.com/image/private/t_q-best/league/ekijosiae96gektbo4iw"></picture>
      </span>

      <span class="nfl-c-matchup-strip__team-abbreviation">
        NYJ
      </span>
      <span class="nfl-c-matchup-strip__team-fullname">
        Jets
      </span>
    </p>
    <div class="nfl-c-matchup-strip__record"></div>
  </div>
  <div class="nfl-c-matchup-strip__team-separator">
    



<span class="nfl-o-icon nfl-o-icon--medium ">
  <svg class="nfl-o-icon--nfl-at" viewBox="0 0 24 24" aria-hidden="true">
    <use xlink:href="#nfl-at"></use>
  </svg>
</span>

  </div>
  <div class="nfl-c-matchup-strip__team">
    <p class="nfl-c-matchup-strip__team-name">
      

      <span class="nfl-c-matchup-strip__team-logo">
        <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source media="(min-width:1024px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/giphcy6ie9mxbnldntsf"><source media="(min-width:768px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/giphcy6ie9mxbnldntsf"><source srcset="https://static.www.nfl.com/image/private/t_q-best/league/giphcy6ie9mxbnldntsf"><!--[if IE 9]></video><![endif]--><img alt="Bills logo" class="img-responsive" role="presentation" src="https://static.www.nfl.com/image/private/t_q-best/league/giphcy6ie9mxbnldntsf"></picture>
      </span>

      <span class="nfl-c-matchup-strip__team-abbreviation">
        BUF
      </span>
      <span class="nfl-c-matchup-strip__team-fullname">
        Bills
      </span>
    </p>
    <div class="nfl-c-matchup-strip__record"></div>
  </div>
</a>
        </div>
    <div class="nfl-c-matchup-strip__right-area">
                  <a class="nfl-o-cta nfl-o-cta--link" href="/ways-to-watch/">
          </a>            
<span class="nfl-o-icon nfl-o-icon--medium ">
  <svg class="nfl-o-icon--tickets" viewBox="0 0 24 24" aria-hidden="true">
  </svg>
</span>
      </div>
        </div>



  <div class="nfl-c-matchup-strip nfl-c-matchup-strip--pre-game">
    <div class="nfl-c-matchup-strip__left-area">
        <div class="nfl-c-matchup-strip__game-info">
          <p class="nfl-c-matchup-strip__date-info">
            <span class="nfl-c-matchup-strip__date-time">
10:00 AM            </span>

            <span class="nfl-c-matchup-strip__date-timezone">
PDT            </span>
          </p>
          <p class="nfl-c-matchup-strip__networks">CBS</p>
        </div>
          
<a class="nfl-c-matchup-strip__game"  aria-label="Go to Ravens vs Browns game page">  <div class="nfl-c-matchup-strip__team nfl-c-matchup-strip__team--opponent">
    <p class="nfl-c-matchup-strip__team-name">
      

      <span class="nfl-c-matchup-strip__team-logo">
        <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source media="(min-width:1024px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/grxy59mqoflnksp2kocc"><source media="(min-width:768px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/grxy59mqoflnksp2kocc"><source srcset="https://static.www.nfl.com/image/private/t_q-best/league/grxy59mqoflnksp2kocc"><!--[if IE 9]></video><![endif]--><img alt="Browns logo" class="img-responsive" role="presentation" src="https://static.www.nfl.com/image/private/t_q-best/league/grxy59mqoflnksp2kocc"></picture>
      </span>

      <span class="nfl-c-matchup-strip__team-abbreviation">
        CLE
      </span>
      <span class="nfl-c-matchup-strip__team-fullname">
        Browns
      </span>
    </p>
    <div class="nfl-c-matchup-strip__record"></div>
  </div>
  <div class="nfl-c-matchup-strip__team-separator">
    



<span class="nfl-o-icon nfl-o-icon--medium ">
  <svg class="nfl-o-icon--nfl-at" viewBox="0 0 24 24" aria-hidden="true">
    <use xlink:href="#nfl-at"></use>
  </svg>
</span>

  </div>
  <div class="nfl-c-matchup-strip__team">
    <p class="nfl-c-matchup-strip__team-name">
      

      <span class="nfl-c-matchup-strip__team-logo">
        <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source media="(min-width:1024px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/ucsdijmddsqcj1i9tddd"><source media="(min-width:768px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/ucsdijmddsqcj1i9tddd"><source srcset="https://static.www.nfl.com/image/private/t_q-best/league/ucsdijmddsqcj1i9tddd"><!--[if IE 9]></video><![endif]--><img alt="Ravens logo" class="img-responsive" role="presentation" src="https://static.www.nfl.com/image/private/t_q-best/league/ucsdijmddsqcj1i9tddd"></picture>
      </span>

      <span class="nfl-c-matchup-strip__team-abbreviation">
        BAL
      </span>
      <span class="nfl-c-matchup-strip__team-fullname">
        Ravens
      </span>
    </p>
    <div class="nfl-c-matchup-strip__record"></div>
  </div>
</a>
        </div>
    <div class="nfl-c-matchup-strip__right-area">
                  <a class="nfl-o-cta nfl-o-cta--link" href="/ways-to-watch/">
          </a>
<span class="nfl-o-icon nfl-o-icon--medium ">
  <svg class="nfl-o-icon--tickets" viewBox="0 0 24 24" aria-hidden="true">
  </svg>
</span>
      </div>
        </div>



  <div class="nfl-c-matchup-strip nfl-c-matchup-strip--pre-game">
    <div class="nfl-c-matchup-strip__left-area">
        <div class="nfl-c-matchup-strip__game-info">
          <p class="nfl-c-matchup-strip__date-info">
            <span class="nfl-c-matchup-strip__date-time">
10:00 AM            </span>

            <span class="nfl-c-matchup-strip__date-timezone">
PDT            </span>
          </p>
          <p class="nfl-c-matchup-strip__networks">FOX</p>
        </div>
          
<a class="nfl-c-matchup-strip__game"  aria-label="Go to Falcons vs Seahawks game page">  <div class="nfl-c-matchup-strip__team nfl-c-matchup-strip__team--opponent">
    <p class="nfl-c-matchup-strip__team-name">
      

      <span class="nfl-c-matchup-strip__team-logo">
        <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source media="(min-width:1024px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/gcytzwpjdzbpwnwxincg"><source media="(min-width:768px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/gcytzwpjdzbpwnwxincg"><source srcset="https://static.www.nfl.com/image/private/t_q-best/league/gcytzwpjdzbpwnwxincg"><!--[if IE 9]></video><![endif]--><img alt="Seahawks logo" class="img-responsive" role="presentation" src="https://static.www.nfl.com/image/private/t_q-best/league/gcytzwpjdzbpwnwxincg"></picture>
      </span>

      <span class="nfl-c-matchup-strip__team-abbreviation">
        SEA
      </span>
      <span class="nfl-c-matchup-strip__team-fullname">
        Seahawks
      </span>
    </p>
    <div class="nfl-c-matchup-strip__record"></div>
  </div>
  <div class="nfl-c-matchup-strip__team-separator">
    



<span class="nfl-o-icon nfl-o-icon--medium ">
  <svg class="nfl-o-icon--nfl-at" viewBox="0 0 24 24" aria-hidden="true">
    <use xlink:href="#nfl-at"></use>
  </svg>
</span>

  </div>
  <div class="nfl-c-matchup-strip__team">
    <p class="nfl-c-matchup-strip__team-name">
      

      <span class="nfl-c-matchup-strip__team-logo">
        <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source media="(min-width:1024px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/d8m7hzpsbrl6pnqht8op"><source media="(min-width:768px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/d8m7hzpsbrl6pnqht8op"><source srcset="https://static.www.nfl.com/image/private/t_q-best/league/d8m7hzpsbrl6pnqht8op"><!--[if IE 9]></video><![endif]--><img alt="Falcons logo" class="img-responsive" role="presentation" src="https://static.www.nfl.com/image/private/t_q-best/league/d8m7hzpsbrl6pnqht8op"></picture>
      </span>

      <span class="nfl-c-matchup-strip__team-abbreviation">
        ATL
      </span>
      <span class="nfl-c-matchup-strip__team-fullname">
        Falcons
      </span>
    </p>
    <div class="nfl-c-matchup-strip__record"></div>
  </div>
</a>
        </div>
    <div class="nfl-c-matchup-strip__right-area">
                  <a class="nfl-o-cta nfl-o-cta--link" href="/ways-to-watch/">
          </a>            
<span class="nfl-o-icon nfl-o-icon--medium ">
  <svg class="nfl-o-icon--tickets" viewBox="0 0 24 24" aria-hidden="true">
  </svg>
</span>
      </div>
        </div>


  <div class="nfl-c-matchup-strip nfl-c-matchup-strip--pre-game">
    <div class="nfl-c-matchup-strip__left-area">
        <div class="nfl-c-matchup-strip__game-info">
          <p class="nfl-c-matchup-strip__date-info">
            <span class="nfl-c-matchup-strip__date-time">
1:05 PM            </span>

            <span class="nfl-c-matchup-strip__date-timezone">
PDT            </span>
          </p>
          <p class="nfl-c-matchup-strip__networks">CBS</p>
        </div>
          
<a class="nfl-c-matchup-strip__game"  aria-label="Go to Bengals vs Chargers game page">  <div class="nfl-c-matchup-strip__team nfl-c-matchup-strip__team--opponent">
    <p class="nfl-c-matchup-strip__team-name">
      

      <span class="nfl-c-matchup-strip__team-logo">
        <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source media="(min-width:1024px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/dhfidtn8jrumakbogeu4"><source media="(min-width:768px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/dhfidtn8jrumakbogeu4"><source srcset="https://static.www.nfl.com/image/private/t_q-best/league/dhfidtn8jrumakbogeu4"><!--[if IE 9]></video><![endif]--><img alt="Chargers logo" class="img-responsive" role="presentation" src="https://static.www.nfl.com/image/private/t_q-best/league/dhfidtn8jrumakbogeu4"></picture>
      </span>

      <span class="nfl-c-matchup-strip__team-abbreviation">
        LAC
      </span>
      <span class="nfl-c-matchup-strip__team-fullname">
        Chargers
      </span>
    </p>
    <div class="nfl-c-matchup-strip__record"></div>
  </div>
  <div class="nfl-c-matchup-strip__team-separator">
    



<span class="nfl-o-icon nfl-o-icon--medium ">
  <svg class="nfl-o-icon--nfl-at" viewBox="0 0 24 24" aria-hidden="true">
    <use xlink:href="#nfl-at"></use>
  </svg>
</span>

  </div>
  <div class="nfl-c-matchup-strip__team">
    <p class="nfl-c-matchup-strip__team-name">
      

      <span class="nfl-c-matchup-strip__team-logo">
        <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source media="(min-width:1024px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/okxpteoliyayufypqalq"><source media="(min-width:768px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/okxpteoliyayufypqalq"><source srcset="https://static.www.nfl.com/image/private/t_q-best/league/okxpteoliyayufypqalq"><!--[if IE 9]></video><![endif]--><img alt="Bengals logo" class="img-responsive" role="presentation" src="https://static.www.nfl.com/image/private/t_q-best/league/okxpteoliyayufypqalq"></picture>
      </span>

      <span class="nfl-c-matchup-strip__team-abbreviation">
        CIN
      </span>
      <span class="nfl-c-matchup-strip__team-fullname">
        Bengals
      </span>
    </p>
    <div class="nfl-c-matchup-strip__record"></div>
  </div>
</a>
        </div>
    <div class="nfl-c-matchup-strip__right-area">
                  <a class="nfl-o-cta nfl-o-cta--link" href="/ways-to-watch/">
          </a>            
<span class="nfl-o-icon nfl-o-icon--medium ">
  <svg class="nfl-o-icon--tickets" viewBox="0 0 24 24" aria-hidden="true">
  </svg>
</span>
      </div>
        </div>


  <div class="nfl-c-matchup-strip nfl-c-matchup-strip--pre-game">
    <div class="nfl-c-matchup-strip__left-area">
        <div class="nfl-c-matchup-strip__game-info">
          <p class="nfl-c-matchup-strip__date-info">
            <span class="nfl-c-matchup-strip__date-time">
1:25 PM            </span>

            <span class="nfl-c-matchup-strip__date-timezone">
PDT            </span>
          </p>
          <p class="nfl-c-matchup-strip__networks">FOX</p>
        </div>
          
<a class="nfl-c-matchup-strip__game" aria-label="Go to 49ers vs Cardinals game page">  <div class="nfl-c-matchup-strip__team nfl-c-matchup-strip__team--opponent">
    <p class="nfl-c-matchup-strip__team-name">
      

      <span class="nfl-c-matchup-strip__team-logo">
        <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source media="(min-width:1024px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/u9fltoslqdsyao8cpm0k"><source media="(min-width:768px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/u9fltoslqdsyao8cpm0k"><source srcset="https://static.www.nfl.com/image/private/t_q-best/league/u9fltoslqdsyao8cpm0k"><!--[if IE 9]></video><![endif]--><img alt="Cardinals logo" class="img-responsive" role="presentation" src="https://static.www.nfl.com/image/private/t_q-best/league/u9fltoslqdsyao8cpm0k"></picture>
      </span>

      <span class="nfl-c-matchup-strip__team-abbreviation">
        ARI
      </span>
      <span class="nfl-c-matchup-strip__team-fullname">
        Cardinals
      </span>
    </p>
    <div class="nfl-c-matchup-strip__record"></div>
  </div>
  <div class="nfl-c-matchup-strip__team-separator">
    



<span class="nfl-o-icon nfl-o-icon--medium ">
  <svg class="nfl-o-icon--nfl-at" viewBox="0 0 24 24" aria-hidden="true">
    <use xlink:href="#nfl-at"></use>
  </svg>
</span>

  </div>
  <div class="nfl-c-matchup-strip__team">
    <p class="nfl-c-matchup-strip__team-name">
      

      <span class="nfl-c-matchup-strip__team-logo">
        <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source media="(min-width:1024px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/dxibuyxbk0b9ua5ih9hn"><source media="(min-width:768px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/dxibuyxbk0b9ua5ih9hn"><source srcset="https://static.www.nfl.com/image/private/t_q-best/league/dxibuyxbk0b9ua5ih9hn"><!--[if IE 9]></video><![endif]--><img alt="49ers logo" class="img-responsive" role="presentation" src="https://static.www.nfl.com/image/private/t_q-best/league/dxibuyxbk0b9ua5ih9hn"></picture>
      </span>

      <span class="nfl-c-matchup-strip__team-abbreviation">
        SF
      </span>
      <span class="nfl-c-matchup-strip__team-fullname">
        49ers
      </span>
    </p>
    <div class="nfl-c-matchup-strip__record"></div>
  </div>
</a>
        </div>
    <div class="nfl-c-matchup-strip__right-area">
                  <a class="nfl-o-cta nfl-o-cta--link" href="/ways-to-watch/">
          </a>            
<span class="nfl-o-icon nfl-o-icon--medium ">
  <svg class="nfl-o-icon--tickets" viewBox="0 0 24 24" aria-hidden="true">
  </svg>
</span>
      </div>
        </div>



  <div class="nfl-c-matchup-strip nfl-c-matchup-strip--pre-game">
    <div class="nfl-c-matchup-strip__left-area">
        <div class="nfl-c-matchup-strip__game-info">
          <p class="nfl-c-matchup-strip__date-info">
            <span class="nfl-c-matchup-strip__date-time">
1:25 PM            </span>

            <span class="nfl-c-matchup-strip__date-timezone">
PDT            </span>
          </p>
          <p class="nfl-c-matchup-strip__networks">FOX</p>
        </div>
          
<a class="nfl-c-matchup-strip__game"  aria-label="Go to Saints vs Buccaneers game page">  <div class="nfl-c-matchup-strip__team nfl-c-matchup-strip__team--opponent">
    <p class="nfl-c-matchup-strip__team-name">
      

      <span class="nfl-c-matchup-strip__team-logo">
        <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source media="(min-width:1024px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/v8uqiualryypwqgvwcih"><source media="(min-width:768px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/v8uqiualryypwqgvwcih"><source srcset="https://static.www.nfl.com/image/private/t_q-best/league/v8uqiualryypwqgvwcih"><!--[if IE 9]></video><![endif]--><img alt="Buccaneers logo" class="img-responsive" role="presentation" src="https://static.www.nfl.com/image/private/t_q-best/league/v8uqiualryypwqgvwcih"></picture>
      </span>

      <span class="nfl-c-matchup-strip__team-abbreviation">
        TB
      </span>
      <span class="nfl-c-matchup-strip__team-fullname">
        Buccaneers
      </span>
    </p>
    <div class="nfl-c-matchup-strip__record"></div>
  </div>
  <div class="nfl-c-matchup-strip__team-separator">
    



<span class="nfl-o-icon nfl-o-icon--medium ">
  <svg class="nfl-o-icon--nfl-at" viewBox="0 0 24 24" aria-hidden="true">
    <use xlink:href="#nfl-at"></use>
  </svg>
</span>

  </div>
  <div class="nfl-c-matchup-strip__team">
    <p class="nfl-c-matchup-strip__team-name">
      

      <span class="nfl-c-matchup-strip__team-logo">
        <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source media="(min-width:1024px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/grhjkahghjkk17v43hdx"><source media="(min-width:768px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/grhjkahghjkk17v43hdx"><source srcset="https://static.www.nfl.com/image/private/t_q-best/league/grhjkahghjkk17v43hdx"><!--[if IE 9]></video><![endif]--><img alt="Saints logo" class="img-responsive" role="presentation" src="https://static.www.nfl.com/image/private/t_q-best/league/grhjkahghjkk17v43hdx"></picture>
      </span>

      <span class="nfl-c-matchup-strip__team-abbreviation">
        NO
      </span>
      <span class="nfl-c-matchup-strip__team-fullname">
        Saints
      </span>
    </p>
    <div class="nfl-c-matchup-strip__record"></div>
  </div>
</a>
        </div>
    <div class="nfl-c-matchup-strip__right-area">
                  <a class="nfl-o-cta nfl-o-cta--link" href="/ways-to-watch/">
          </a>            
<span class="nfl-o-icon nfl-o-icon--medium ">
  <svg class="nfl-o-icon--tickets" viewBox="0 0 24 24" aria-hidden="true">
  </svg>
</span>
      </div>
        </div>







  <div class="nfl-c-matchup-strip nfl-c-matchup-strip--pre-game">
    <div class="nfl-c-matchup-strip__left-area">
        <div class="nfl-c-matchup-strip__game-info">
          <p class="nfl-c-matchup-strip__date-info">
            <span class="nfl-c-matchup-strip__date-time">
5:20 PM            </span>

            <span class="nfl-c-matchup-strip__date-timezone">
PDT            </span>
          </p>
          <p class="nfl-c-matchup-strip__networks">NBC</p>
        </div>
          
<a class="nfl-c-matchup-strip__game" aria-label="Go to Rams vs Cowboys game page">  <div class="nfl-c-matchup-strip__team nfl-c-matchup-strip__team--opponent">
    <p class="nfl-c-matchup-strip__team-name">
      

      <span class="nfl-c-matchup-strip__team-logo">
        <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source media="(min-width:1024px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/ieid8hoygzdlmzo0tnf6"><source media="(min-width:768px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/ieid8hoygzdlmzo0tnf6"><source srcset="https://static.www.nfl.com/image/private/t_q-best/league/ieid8hoygzdlmzo0tnf6"><!--[if IE 9]></video><![endif]--><img alt="Cowboys logo" class="img-responsive" role="presentation" src="https://static.www.nfl.com/image/private/t_q-best/league/ieid8hoygzdlmzo0tnf6"></picture>
      </span>

      <span class="nfl-c-matchup-strip__team-abbreviation">
        DAL
      </span>
      <span class="nfl-c-matchup-strip__team-fullname">
        Cowboys
      </span>
    </p>
    <div class="nfl-c-matchup-strip__record"></div>
  </div>
  <div class="nfl-c-matchup-strip__team-separator">
    



<span class="nfl-o-icon nfl-o-icon--medium ">
  <svg class="nfl-o-icon--nfl-at" viewBox="0 0 24 24" aria-hidden="true">
    <use xlink:href="#nfl-at"></use>
  </svg>
</span>

  </div>
  <div class="nfl-c-matchup-strip__team">
    <p class="nfl-c-matchup-strip__team-name">
      

      <span class="nfl-c-matchup-strip__team-logo">
        <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source media="(min-width:1024px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/ayvwcmluj2ohkdlbiegi"><source media="(min-width:768px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/ayvwcmluj2ohkdlbiegi"><source srcset="https://static.www.nfl.com/image/private/t_q-best/league/ayvwcmluj2ohkdlbiegi"><!--[if IE 9]></video><![endif]--><img alt="Rams logo" class="img-responsive" role="presentation" src="https://static.www.nfl.com/image/private/t_q-best/league/ayvwcmluj2ohkdlbiegi"></picture>
      </span>

      <span class="nfl-c-matchup-strip__team-abbreviation">
        LA
      </span>
      <span class="nfl-c-matchup-strip__team-fullname">
        Rams
      </span>
    </p>
    <div class="nfl-c-matchup-strip__record"></div>
  </div>
</a>
        </div>
    <div class="nfl-c-matchup-strip__right-area">
                  <a class="nfl-o-cta nfl-o-cta--link" href="/ways-to-watch/">
          </a>            
<span class="nfl-o-icon nfl-o-icon--medium ">
  <svg class="nfl-o-icon--tickets" viewBox="0 0 24 24" aria-hidden="true">
  </svg>
</span>
      </div>
        </div>
        </div>
      </div>
    </section>
    <section class="d3-l-grid--outer d3-l-section-row nfl-o-matchup-group">
      <div class="d3-l-grid--inner">
        <div class="d3-l-col__col-12" data-min-enable="video page" data-min-method="true">
          <h2 class="d3-o-section-title">Monday, September 14th</h2>







  <div class="nfl-c-matchup-strip nfl-c-matchup-strip--pre-game">
    <div class="nfl-c-matchup-strip__left-area">
        <div class="nfl-c-matchup-strip__game-info">
          <p class="nfl-c-matchup-strip__date-info">
            <span class="nfl-c-matchup-strip__date-time">
4:15 PM            </span>

            <span class="nfl-c-matchup-strip__date-timezone">
PDT            </span>
          </p>
          <p class="nfl-c-matchup-strip__networks">ESPN</p>
        </div>
          
<a class="nfl-c-matchup-strip__game"  aria-label="Go to Giants vs Steelers game page">  <div class="nfl-c-matchup-strip__team nfl-c-matchup-strip__team--opponent">
    <p class="nfl-c-matchup-strip__team-name">
      

      <span class="nfl-c-matchup-strip__team-logo">
        <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source media="(min-width:1024px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/xujg9t3t4u5nmjgr54wx"><source media="(min-width:768px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/xujg9t3t4u5nmjgr54wx"><source srcset="https://static.www.nfl.com/image/private/t_q-best/league/xujg9t3t4u5nmjgr54wx"><!--[if IE 9]></video><![endif]--><img alt="Steelers logo" class="img-responsive" role="presentation" src="https://static.www.nfl.com/image/private/t_q-best/league/xujg9t3t4u5nmjgr54wx"></picture>
      </span>

      <span class="nfl-c-matchup-strip__team-abbreviation">
        PIT
      </span>
      <span class="nfl-c-matchup-strip__team-fullname">
        Steelers
      </span>
    </p>
    <div class="nfl-c-matchup-strip__record"></div>
  </div>
  <div class="nfl-c-matchup-strip__team-separator">
    



<span class="nfl-o-icon nfl-o-icon--medium ">
  <svg class="nfl-o-icon--nfl-at" viewBox="0 0 24 24" aria-hidden="true">
    <use xlink:href="#nfl-at"></use>
  </svg>
</span>

  </div>
  <div class="nfl-c-matchup-strip__team">
    <p class="nfl-c-matchup-strip__team-name">
      

      <span class="nfl-c-matchup-strip__team-logo">
        <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source media="(min-width:1024px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/t6mhdmgizi6qhndh8b9p"><source media="(min-width:768px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/t6mhdmgizi6qhndh8b9p"><source srcset="https://static.www.nfl.com/image/private/t_q-best/league/t6mhdmgizi6qhndh8b9p"><!--[if IE 9]></video><![endif]--><img alt="Giants logo" class="img-responsive" role="presentation" src="https://static.www.nfl.com/image/private/t_q-best/league/t6mhdmgizi6qhndh8b9p"></picture>
      </span>

      <span class="nfl-c-matchup-strip__team-abbreviation">
        NYG
      </span>
      <span class="nfl-c-matchup-strip__team-fullname">
        Giants
      </span>
    </p>
    <div class="nfl-c-matchup-strip__record"></div>
  </div>
</a>
        </div>
    <div class="nfl-c-matchup-strip__right-area">
                  <a class="nfl-o-cta nfl-o-cta--link" href="/ways-to-watch/">
          </a>
<span class="nfl-o-icon nfl-o-icon--medium ">
  <svg class="nfl-o-icon--tickets" viewBox="0 0 24 24" aria-hidden="true">
  </svg>
</span>
      </div>
        </div>




  <div class="nfl-c-matchup-strip nfl-c-matchup-strip--pre-game">
    <div class="nfl-c-matchup-strip__left-area">
        <div class="nfl-c-matchup-strip__game-info">
          <p class="nfl-c-matchup-strip__date-info">
            <span class="nfl-c-matchup-strip__date-time">
7:10 PM            </span>

            <span class="nfl-c-matchup-strip__date-timezone">
PDT            </span>
          </p>
          <p class="nfl-c-matchup-strip__networks">ESPN</p>
        </div>
          
<a class="nfl-c-matchup-strip__game" aria-label="Go to Broncos vs Titans game page">  <div class="nfl-c-matchup-strip__team nfl-c-matchup-strip__team--opponent">
    <p class="nfl-c-matchup-strip__team-name">
      

      <span class="nfl-c-matchup-strip__team-logo">
        <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source media="(min-width:1024px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/pln44vuzugjgipyidsre"><source media="(min-width:768px)" srcset="https://static.www.nfl.com/image/private/t_q-best/league/pln44vuzugjgipyidsre"><source srcset="https://static.www.nfl.com/image/private/t_q-best/league/pln44vuzugjgipyidsre"><!--[if IE 9]></video><![endif]--><img alt="Titans logo" class="img-responsive" role="presentation" src="https://static.www.nfl.com/image/private/t_q-best/league/pln44vuzugjgipyidsre"></picture>
      </span>

      <span class="nfl-c-matchup-strip__team-abbreviation">
        TEN
      </span>
      <span class="nfl-c-matchup-strip__team-fullname">
        Titans
      </span>
    </p>
    <div class="nfl-c-matchup-strip__record"></div>
  </div>
  <div class="nfl-c-matchup-strip__team-separator">
    



<span class="nfl-o-icon nfl-o-icon--medium ">
  <svg class="nfl-o-icon--nfl-at" viewBox="0 0 24 24" aria-hidden="true">
    <use xlink:href="#nfl-at"></use>
  </svg>
</span>

  </div>
  <div class="nfl-c-matchup-strip__team">
    <p class="nfl-c-matchup-strip__team-name">
      

      <span class="nfl-c-matchup-strip__team-logo">
        <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source media="(min-width:1024px)"><source media="(min-width:768px)"><source><!--[if IE 9]></video><![endif]--><img alt="Broncos logo" class="img-responsive" role="presentation" src="https://static.www.nfl.com/image/private/t_q-best/league/t0p7m5cjdjy18rnzzqbx"></picture>
      </span>

      <span class="nfl-c-matchup-strip__team-abbreviation">
        DEN
      </span>
      <span class="nfl-c-matchup-strip__team-fullname">
        Broncos
      </span>
    </p>
    <div class="nfl-c-matchup-strip__record"></div>
  </div>
</a>
        </div>
    <div class="nfl-c-matchup-strip__right-area">
                              <span></span>
          </a>
            



<span class="nfl-o-icon nfl-o-icon--medium ">
  <svg class="nfl-o-icon--tickets" viewBox="0 0 24 24" aria-hidden="true">
   
  </svg>
</span>

           
            <span></span>
          </a>
      </div>
        </div>
        </div>
      </div>
    </section>





  <section class="d3-l-grid--outer d3-l-section-row">
    <div class="d3-l-grid--inner ">
      



                <div class="d3-l-col__col-3" data-min-enable="video page" data-min-method="true">
            



  <div class="nfl-c-custom-promo nfl-c-custom-promo--has-color-schema-all-black-on-white nfl-c-custom-promo--has-image-position-top nfl-c-custom-promo--has-desktop-size-33 nfl-c-custom-promo--has-mobile-size-33 nfl-c-custom-promo--has-text-align-center nfl-c-custom-promo--has-background-size-default nfl-c-custom-promo--has-padding nfl-c-custom-promo--has-cta-size-small nfl-c-custom-promo--has-cta-color-schema-button-color nfl-c-custom-promo--no-section-title">
    


    

    <div class="d3-o-media-object d3-o-media-object--horizontal d3-o-media-object--vertical-center nfl-c-custom-promo__content">


        <figure class="d3-o-media-object__figure nfl-c-custom-promo__figure">
          <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source media="(min-width:1024px)" srcset="https://static.www.nfl.com/image/private/f_auto/league/w5kay6amsvuibhlaz24w"><source media="(min-width:768px)" srcset="https://static.www.nfl.com/image/private/f_auto/league/w5kay6amsvuibhlaz24w"><source srcset="https://static.www.nfl.com/image/private/f_auto/league/w5kay6amsvuibhlaz24w"><!--[if IE 9]></video><![endif]--><img alt="NFL Gamepass" class="img-responsive" src="https://static.www.nfl.com/image/private/f_auto/league/w5kay6amsvuibhlaz24w"></picture>
        </figure>
              <div class="d3-o-media-object__body nfl-c-custom-promo__body">
            <h4 class="d3-o-media-object__roofline nfl-c-custom-promo__headline">
              <p>NFL Gamepass</p>

            </h4>
                                <div class="d3-o-media-object__cta nfl-c-custom-promo__cta">
                <a class="d3-o-media-object__link d3-o-button nfl-o-cta nfl-o-cta--primary" href="http://www.nfl.com/gamepass?nfl.com/gamepass?icampaign=gpg-edi-cpr-schedule_packages" target="_blank" aria-label="Complimentary Access - Opens new window">
                  Complimentary Access
                </a>
            </div>
        </div>
    </div>
  
  </div>

          </div>
          <div class="d3-l-col__col-3" data-min-enable="video page" data-min-method="true">
            



  <div class="nfl-c-custom-promo nfl-c-custom-promo--has-color-schema-all-black-on-white nfl-c-custom-promo--has-image-position-top nfl-c-custom-promo--has-desktop-size-33 nfl-c-custom-promo--has-mobile-size-33 nfl-c-custom-promo--has-text-align-center nfl-c-custom-promo--has-background-size-default nfl-c-custom-promo--has-padding nfl-c-custom-promo--has-cta-size-small nfl-c-custom-promo--has-cta-color-schema-link-color nfl-c-custom-promo--no-section-title">
    


    

    <div class="d3-o-media-object d3-o-media-object--horizontal d3-o-media-object--vertical-center nfl-c-custom-promo__content">


        <figure class="d3-o-media-object__figure nfl-c-custom-promo__figure">
          <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source media="(min-width:1024px)" srcset="https://static.www.nfl.com/image/private/f_auto/league/jzj3x4j5zyn64oogjcx0"><source media="(min-width:768px)" srcset="https://static.www.nfl.com/image/private/f_auto/league/jzj3x4j5zyn64oogjcx0"><source srcset="https://static.www.nfl.com/image/private/f_auto/league/jzj3x4j5zyn64oogjcx0"><!--[if IE 9]></video><![endif]--><img alt="Thursday Night Football" class="img-responsive" src="https://static.www.nfl.com/image/private/f_auto/league/jzj3x4j5zyn64oogjcx0"></picture>
        </figure>
              <div class="d3-o-media-object__body nfl-c-custom-promo__body">
            <h4 class="d3-o-media-object__roofline nfl-c-custom-promo__headline">
              <p>Thursday Night Football</p>

            </h4>
                                <div class="d3-o-media-object__cta nfl-c-custom-promo__cta">
                <a class="d3-o-media-object__link d3-o-button nfl-o-cta nfl-o-cta--primary" href="/tnf/" target="_self" aria-label="View Schedule">
                  
                </a>
            </div>
        </div>
    </div>
  
  </div>

          </div>
          <div class="d3-l-col__col-3" data-min-enable="video page" data-min-method="true">
            



  <div class="nfl-c-custom-promo nfl-c-custom-promo--has-color-schema-all-black-on-white nfl-c-custom-promo--has-image-position-top nfl-c-custom-promo--has-desktop-size-33 nfl-c-custom-promo--has-mobile-size-33 nfl-c-custom-promo--has-text-align-center nfl-c-custom-promo--has-background-size-default nfl-c-custom-promo--has-padding nfl-c-custom-promo--has-cta-size-small nfl-c-custom-promo--has-cta-color-schema-link-color nfl-c-custom-promo--no-section-title">
    


    

    <div class="d3-o-media-object d3-o-media-object--horizontal d3-o-media-object--vertical-center nfl-c-custom-promo__content">


        <figure class="d3-o-media-object__figure nfl-c-custom-promo__figure">
          <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source media="(min-width:1024px)" srcset="https://static.www.nfl.com/image/private/f_auto/league/mywogy71oluagu0qmgaz"><source media="(min-width:768px)" srcset="https://static.www.nfl.com/image/private/f_auto/league/mywogy71oluagu0qmgaz"><source srcset="https://static.www.nfl.com/image/private/f_auto/league/mywogy71oluagu0qmgaz"><!--[if IE 9]></video><![endif]--><img alt="Sunday Night Football" class="img-responsive" src="https://static.www.nfl.com/image/private/f_auto/league/mywogy71oluagu0qmgaz"></picture>
        </figure>
              <div class="d3-o-media-object__body nfl-c-custom-promo__body">
            <h4 class="d3-o-media-object__roofline nfl-c-custom-promo__headline">
              <p>Sunday Night Football</p>

            </h4>
                                <div class="d3-o-media-object__cta nfl-c-custom-promo__cta">
                <a class="d3-o-media-object__link d3-o-button nfl-o-cta nfl-o-cta--primary" title="" href="/schedules/sunday-night-football" target="_self" aria-label="View Schedule">
                  
                </a>
            </div>
        </div>
    </div>
  
  </div>

          </div>
          <div class="d3-l-col__col-3" data-min-enable="video page" data-min-method="true">
            



  <div class="nfl-c-custom-promo nfl-c-custom-promo--has-color-schema-all-black-on-white nfl-c-custom-promo--has-image-position-top nfl-c-custom-promo--has-desktop-size-33 nfl-c-custom-promo--has-mobile-size-33 nfl-c-custom-promo--has-text-align-center nfl-c-custom-promo--has-background-size-default nfl-c-custom-promo--has-padding nfl-c-custom-promo--has-cta-size-small nfl-c-custom-promo--has-cta-color-schema-link-color nfl-c-custom-promo--no-section-title">
    


    

    <div class="d3-o-media-object d3-o-media-object--horizontal d3-o-media-object--vertical-center nfl-c-custom-promo__content">


        <figure class="d3-o-media-object__figure nfl-c-custom-promo__figure">
          <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source media="(min-width:1024px)" srcset="https://static.www.nfl.com/image/private/f_auto/league/mpx0s6jwjrjbhfw9rfol"><source media="(min-width:768px)" srcset="https://static.www.nfl.com/image/private/f_auto/league/mpx0s6jwjrjbhfw9rfol"><source srcset="https://static.www.nfl.com/image/private/f_auto/league/mpx0s6jwjrjbhfw9rfol"><!--[if IE 9]></video><![endif]--><img alt="Monday Night Football" class="img-responsive" src="https://static.www.nfl.com/image/private/f_auto/league/mpx0s6jwjrjbhfw9rfol"></picture>
        </figure>
              <div class="d3-o-media-object__body nfl-c-custom-promo__body">
            <h4 class="d3-o-media-object__roofline nfl-c-custom-promo__headline">
              <p>Monday Night Football</p>

            </h4>
                                <div class="d3-o-media-object__cta nfl-c-custom-promo__cta">
                <a class="d3-o-media-object__link d3-o-button nfl-o-cta nfl-o-cta--primary" title="" href="/schedules/monday-night-football" target="_self" aria-label="View Schedule">
                  
                </a>
            </div>
        </div>
    </div>
  
  </div>

          </div>
    </div>
  </section>




  <section class="d3-l-grid--outer d3-l-section-row">
    <div class="d3-l-grid--inner ">
   
    </div>
  </section>

  </main>

  


<footer class="d3-o-footer" role="contentinfo" data-require="modules/footer">
  



  <section class="d3-l-grid--outer d3-l-section-row">
    <div class="d3-l-grid--inner d3-l-grid--no-gap">
      



                <div class="d3-l-col__col-1">
            

<div class="blank-placeholder" style="height:px;margin:0 auto"></div>
          </div>
          <div class="d3-l-col__col-6">
            

<section class="d3-o-club-partners">
  <div class="d3-l-grid--outer">
    <div class="d3-l-grid--inner">
      <div class="d3-l-col__col-12" data-min-enable="video page" data-min-method="true">

          <h3 class="d3-o-club-partners__section-title">
            <span class="d3-o-club-partners__section-title-label">
              GAME ACCESS
            </span>
          </h3>
      </div>
      <div class="d3-l-col__col-12" data-min-enable="video page" data-min-method="true">
          <nav class="d3-o-club-partners__nav" role="navigation">
            <ul class="d3-o-club-partners__nav-items">
                  <li>
                      <img alt="" data-src="https://static.www.nfl.com/league/sad1e1tyvgvsiuxekivu" data-tag="">
                  </li>
                  <li>
                      <img alt="" data-src="https://static.www.nfl.com/league/gu0ranuiqqh7mam3cubi" data-tag="">
                  </li>
                  <li>
                      <img alt="" data-src="https://static.www.nfl.com/league/eaxxuggoxoglwvsqzvvl" data-tag="">
                  </li>
                  <li>
                      <img alt="" data-src="https://static.www.nfl.com/league/oyqglvlvfzuze4rdpppa" data-tag="">
                  </li>
                  <li>
                      <img alt="" data-src="https://static.www.nfl.com/league/nkzu38j0rmd2vlnhzaog" data-tag="">
                  </li>
                  <li>
                      <img alt="" data-src="https://static.www.nfl.com/league/ab6zrhysskd3taluwsko" data-tag="">
                  </li>
            </ul>
          </nav>
      </div>
    </div>
  </div>
</section>
          </div>
          <div class="d3-l-col__col-5">
            



  <div class="nfl-c-custom-promo nfl-c-custom-promo--has-color-schema-all-black-on-white nfl-c-custom-promo--has-image-position-left nfl-c-custom-promo--has-desktop-size-33 nfl-c-custom-promo--has-mobile-size-33 nfl-c-custom-promo--has-text-align-center nfl-c-custom-promo--has-background-size-default nfl-c-custom-promo--has-padding nfl-c-custom-promo--has-cta-size-small nfl-c-custom-promo--has-cta-color-schema-link-color nfl-c-custom-promo--no-section-title">

  </div>

          </div>
    </div>
  </section>

<div class="d3-o-footer__apps">
    <span class="d3-o-footer__apps-label">
      Download apps
    </span>
  <nav role="navigation">
    <h2 class="d3-o-nav__title">Download Apps nav</h2>
    <ul class="d3-o-footer__apps-links">
      <li>
        <a href="https://itunes.apple.com/app/nfl/id389781154" target="_blank" aria-label="Download on the Apple Store - Opens new window">


          <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source data-srcset="https://www.nfl.com/compiledassets/assets/icons/app_store_badge.svg" media="(min-width:1024px)"><source data-srcset="https://www.nfl.com/compiledassets/assets/icons/app_store_badge.svg" media="(min-width:768px)"><source data-srcset="https://www.nfl.com/compiledassets/assets/icons/app_store_badge.svg"><!--[if IE 9]></video><![endif]--><img alt="Download on the Apple Store" class="d3-o-footer__apps-links-icon-apple img-responsive" data-src="https://www.nfl.com/compiledassets/assets/icons/app_store_badge.svg" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="></picture>
        </a>
      </li>
      <li>
        <a id="league-play-store-app" href="https://play.google.com/store/apps/details?id=com.gotv.nflgamecenter.us.lite" target="_blank" aria-label="Get it on Google Play - Opens new window">

          <picture><!--[if IE 9]><video style="display: none; "><![endif]--><source data-srcset="https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png" media="(min-width:1024px)"><source data-srcset="https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png" media="(min-width:768px)"><source data-srcset="https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png"><!--[if IE 9]></video><![endif]--><img alt="Get it on Google Play" class="d3-o-footer__apps-links-icon-google img-responsive" data-src="https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="></picture>
        </a>
      </li>
    </ul>
  </nav>
</div>
</footer>



<svg xmlns="http://www.w3.org/2000/svg" id="NFLicons" style="display: none">
<symbol id="bigLeft"><desc id="bigLeftDesc">
      Big left arrow icon
    </desc><polygon id="bigLeftArrow" points="15.1,0 16,0.9 1.7,16 16,31.1 15.1,32 0,16  "></polygon></symbol><symbol id="bigRight"><desc id="bigRightDesc">
      Big right arrow icon
    </desc><polygon id="bigRightArrow" points="0.9,0 0,0.9 14.3,16 0,31.1 0.9,32 16,16   "></polygon></symbol><symbol id="close"><desc id="closeDesc">
      Close icon
    </desc><polygon id="closeCross" points="12,9.8 2.2,0 0,2.2 9.8,12 0,21.8 2.2,24 12,14.2 21.8,24 24,21.8 14.2,12 24,2.2 21.8,0 "></polygon></symbol><symbol id="copyUrl"><desc id="copyUrlDesc">Copy Url</desc><path d="M8,4V16H20V4ZM18,14H10V6h8Z"></path><polygon points="6 8 4 8 4 18 4 20 6 20 16 20 16 18 6 18 6 8"></polygon></symbol><symbol id="dots"><desc id="dotsDesc">
      Three dots icon
    </desc><circle id="dotsDot3" cx="21.6" cy="12" r="2.4"></circle><circle id="dotsDot2" cx="12" cy="12" r="2.4"></circle><circle id="dotsDot1" cx="2.4" cy="12" r="2.4"></circle></symbol><symbol id="down"><desc id="downDesc">
      Down arrow icon
    </desc><polygon id="downArrow" points="12,9.8 20.8,1 21.9,0 24,2.1 23,3.1 13.1,12.9 12,14 12,14 12,14 10.9,13 1.1,3.1 0,2.1 2.1,0 3.2,1   "></polygon></symbol><symbol id="email"><desc id="emailDesc">
      Email icon
    </desc><path class="emailBackground" d="M24,0C10.7,0,0,10.7,0,24s10.7,24,24,24s24-10.7,24-24S37.3,0,24,0z"></path><path class="emailCircle" d="M24,0C10.7,0,0,10.7,0,24c0,13.3,10.7,24,24,24s24-10.7,24-24C48,10.7,37.3,0,24,0z M24,45.8 C12,45.8,2.2,36,2.2,24C2.2,12,12,2.2,24,2.2S45.8,12,45.8,24C45.8,36,36,45.8,24,45.8z"></path><g class="emailLetter">
  <polygon points="13.1,20.7 13.1,30.5 34.9,30.5 34.9,20.7 24,26.2 "></polygon>
  <polygon points="13.1,16.4 13.1,18.5 24,24 34.9,18.5 34.9,16.4 "></polygon>
</g><polygon points="13.1,20.7 13.1,30.5 34.9,30.5 34.9,20.7 24,26.2 "></polygon><polygon points="13.1,16.4 13.1,18.5 24,24 34.9,18.5 34.9,16.4 "></polygon></symbol><symbol id="email-no-circle"><desc id="email-no-circleDesc">
      Email icon
    </desc><path class="emailBackground" d="M24,0C10.7,0,0,10.7,0,24s10.7,24,24,24s24-10.7,24-24S37.3,0,24,0z"></path><g class="emailLetter">
  <polygon points="13.1,20.7 13.1,30.5 34.9,30.5 34.9,20.7 24,26.2 "></polygon>
  <polygon points="13.1,16.4 13.1,18.5 24,24 34.9,18.5 34.9,16.4 "></polygon>
</g><polygon points="13.1,20.7 13.1,30.5 34.9,30.5 34.9,20.7 24,26.2 "></polygon><polygon points="13.1,16.4 13.1,18.5 24,24 34.9,18.5 34.9,16.4 "></polygon></symbol><symbol id="exitFullScreen"><desc id="exitFullScreenDesc">
      Exit Fullscreen icon
    </desc><path id="exitFullScreenCorners" d="M18.7,5.2H24v1.3h-6.7V0h1.3V5.2z M18.7,15.8V21h-1.3v-6.6H24v1.3H18.7z M5.3,5.2 V0h1.3v6.6H0V5.2H5.3z M5.3,15.8H0v-1.3h6.7V21H5.3V15.8z"></path></symbol><symbol id="external"><desc id="externalDesc">
      External link icon
    </desc><polygon id="externalBox" points="11,13 0,13 0,2 5.5,2 5.5,3 1,3 1,12 10,11.9 10,7.5 11,7.5   "></polygon><polygon id="externalArrow" points="11.3,1 8,1 8,0 13,0 13,5 12,5 12,1.7 5.9,7.8 5.2,7.1  "></polygon></symbol><symbol id="facebook"><desc id="facebookDesc">
      Facebook logo
    </desc><path class="facebookBackground" d="M24,0C10.7,0,0,10.7,0,24s10.7,24,24,24s24-10.7,24-24S37.3,0,24,0z"></path><path class="facebookCircle_1_" d="M24,0C10.7,0,0,10.7,0,24s10.7,24,24,24s24-10.7,24-24S37.3,0,24,0z M24,45.8 C12,45.8,2.2,36,2.2,24S12,2.2,24,2.2S45.8,12,45.8,24S36,45.8,24,45.8z"></path><path class="facebookF" d="M28.8,24h-4v13.1H20V24h-4v-4.4h4V17c0-4,2.4-6.2,6-6.2c1.7,0,3.2,0.1,3.6,0.2v4.2h-2.5 c-1.9,0-2.3,0.9-2.3,2.3v2.1h4.6L28.8,24z"></path></symbol><symbol id="football"><desc id="footballDesc">Football icon</desc><path d="M10,15.5v-1h1.25v-2H10v-1h1.25v-2H10v-1h1.25V3.13C9.1,3.76,6,6.9,6,12s3.1,8.24,5.25,8.87V15.5Z"></path><path d="M12.75,3.13V8.5H14v1H12.75v2H14v1H12.75v2H14v1H12.75v5.37C14.9,20.24,18,17.1,18,12S14.9,3.76,12.75,3.13Z"></path><rect class="cls-1" x="10" y="11.5" width="1.25" height="1"></rect><rect class="cls-1" x="10" y="8.5" width="1.25" height="1"></rect><rect class="cls-1" x="12.75" y="14.5" width="1.25" height="1"></rect><rect class="cls-1" x="10" y="14.5" width="1.25" height="1"></rect><rect class="cls-1" x="12.75" y="11.5" width="1.25" height="1"></rect><rect class="cls-1" x="12.75" y="8.5" width="1.25" height="1"></rect></symbol><symbol id="facebook-no-circle"><desc id="facebook-no-circleDesc">
      Facebook logo
    </desc><path class="facebookBackground" d="M24,0C10.7,0,0,10.7,0,24s10.7,24,24,24s24-10.7,24-24S37.3,0,24,0z"></path><path class="facebookF" d="M28.8,24h-4v13.1H20V24h-4v-4.4h4V17c0-4,2.4-6.2,6-6.2c1.7,0,3.2,0.1,3.6,0.2v4.2h-2.5 c-1.9,0-2.3,0.9-2.3,2.3v2.1h4.6L28.8,24z"></path></symbol><symbol id="instagram"><desc id="instagramDesc">
      Instagram logo
    </desc><path class="instagramBackground" d="M24,0C10.7,0,0,10.7,0,24s10.7,24,24,24s24-10.7,24-24S37.3,0,24,0z"></path><path class="instagramCircle" d="M24,0C10.7,0,0,10.7,0,24c0,13.3,10.7,24,24,24c13.3,0,24-10.7,24-24 C48,10.7,37.3,0,24,0z M24,45.8C12,45.8,2.2,36,2.2,24C2.2,12,12,2.2,24,2.2S45.8,12,45.8,24C45.8,36,36,45.8,24,45.8z"></path><path class="instagramCamera" d="M34.8,19.5c-0.1-1.2-0.2-2-0.5-2.6c-0.3-0.7-0.7-1.3-1.3-1.9c-0.6-0.6-1.2-1-1.9-1.3 c-0.7-0.3-1.5-0.5-2.6-0.5c-1.2-0.1-1.5-0.1-4.5-0.1s-3.3,0-4.5,0.1c-1.2,0.1-2,0.2-2.6,0.5c-0.7,0.3-1.3,0.7-1.9,1.3 c-0.6,0.6-1,1.2-1.3,1.9c-0.3,0.7-0.5,1.5-0.5,2.6c-0.1,1.2-0.1,1.5-0.1,4.5s0,3.3,0.1,4.5c0.1,1.2,0.2,2,0.5,2.6 c0.3,0.7,0.7,1.3,1.3,1.9c0.6,0.6,1.2,1,1.9,1.3c0.7,0.3,1.5,0.5,2.6,0.5c1.2,0.1,1.5,0.1,4.5,0.1s3.3,0,4.5-0.1 c1.2-0.1,2-0.2,2.6-0.5c0.7-0.3,1.3-0.7,1.9-1.3c0.6-0.6,1-1.2,1.3-1.9c0.3-0.7,0.5-1.5,0.5-2.6c0.1-1.2,0.1-1.5,0.1-4.5 S34.9,20.7,34.8,19.5z M32.9,28.4c0,1.1-0.2,1.6-0.4,2c-0.2,0.5-0.4,0.9-0.8,1.3c-0.4,0.4-0.7,0.6-1.3,0.8c-0.4,0.1-1,0.3-2,0.4 c-1.1,0.1-1.5,0.1-4.4,0.1c-2.9,0-3.3,0-4.4-0.1c-1.1,0-1.6-0.2-2-0.4c-0.5-0.2-0.9-0.4-1.3-0.8c-0.4-0.4-0.6-0.7-0.8-1.3 c-0.1-0.4-0.3-1-0.4-2c-0.1-1.1-0.1-1.5-0.1-4.4s0-3.3,0.1-4.4c0-1.1,0.2-1.6,0.4-2c0.2-0.5,0.4-0.9,0.8-1.3 c0.4-0.4,0.7-0.6,1.3-0.8c0.4-0.1,1-0.3,2-0.4c1.1-0.1,1.5-0.1,4.4-0.1s3.3,0,4.4,0.1c1.1,0,1.6,0.2,2,0.4c0.5,0.2,0.9,0.4,1.3,0.8 c0.4,0.4,0.6,0.7,0.8,1.3c0.1,0.4,0.3,1,0.4,2c0.1,1.1,0.1,1.5,0.1,4.4S32.9,27.3,32.9,28.4z"></path><path class="instagramLens" d="M24,18.5c-3,0-5.5,2.4-5.5,5.5c0,3,2.4,5.5,5.5,5.5s5.5-2.4,5.5-5.5 C29.5,21,27,18.5,24,18.5z M24,27.5c-2,0-3.5-1.6-3.5-3.5c0-2,1.6-3.5,3.5-3.5s3.5,1.6,3.5,3.5C27.5,26,26,27.5,24,27.5z"></path></symbol><symbol id="snapchat"><desc id="snapchatDesc">
      Snapchat logo
    </desc><path class="snapchatBackground" d="M24,0C10.7,0,0,10.7,0,24s10.7,24,24,24s24-10.7,24-24S37.3,0,24,0z"></path><path class="snapchatCircle" d="M24,0C10.7,0,0,10.7,0,24c0,13.3,10.7,24,24,24c13.3,0,24-10.7,24-24C48,10.7,37.3,0,24,0 z M24,45.8C12,45.8,2.2,36,2.2,24C2.2,12,12,2.2,24,2.2S45.8,12,45.8,24C45.8,36,36,45.8,24,45.8z"></path><path class="snapchatGhost" d="M35.7,29.2c-3-0.7-4.2-2.8-4.7-3.9c-0.1-0.3,0-0.7,0.3-0.8l1.6-0.8c0.4-0.2,0.7-0.6,0.8-1 c0.1-0.4,0-0.9-0.2-1.3c-0.4-0.6-1.1-0.8-1.7-0.6l-1.1,0.3v-3c0,0,0,0,0,0c-0.3-5.3-5-6-6.5-6.1c0,0-0.1,0-0.1,0 c-1.5,0.1-6.2,0.8-6.5,6.1c0,0,0,0,0,0v3l-1.1-0.3c-0.6-0.2-1.3,0.1-1.7,0.6c-0.3,0.4-0.3,0.8-0.2,1.3c0.1,0.4,0.4,0.8,0.8,1 l1.6,0.8c0.3,0.1,0.4,0.5,0.3,0.8c-0.5,1.2-1.7,3.2-4.7,3.9c-0.3,0.1-0.5,0.4-0.4,0.6c0,0.1,0.4,2,3.2,2.1c0.1,0.5,0.3,1.2,0.6,1.5 c0.2,0.2,0.4,0.3,0.6,0.2c0,0,1.4-0.5,2.6-0.2c0,0,1.5,0.3,2.4,1.2c0.4,0.4,1,0.7,1.5,0.9c0.3,0.1,0.7,0.1,1.1,0.1s0.7,0,1.1-0.1 c0.6-0.1,1.1-0.4,1.5-0.9c0.9-0.9,2.4-1.2,2.4-1.2c1.3-0.2,2.6,0.2,2.6,0.2c0.2,0.1,0.5,0,0.6-0.2c0.2-0.3,0.4-0.9,0.6-1.5 c2.9-0.1,3.2-2,3.2-2.1C36.2,29.6,36,29.3,35.7,29.2z"></path></symbol><symbol id="youtube"><desc id="youtubeDesc">
      YouTube logo
    </desc><path class="youtubeBackground" d="M24,0C10.7,0,0,10.7,0,24s10.7,24,24,24s24-10.7,24-24S37.3,0,24,0z"></path><path class="youtubeCircle" d="M24,0C10.7,0,0,10.7,0,24c0,13.3,10.7,24,24,24c13.3,0,24-10.7,24-24C48,10.7,37.3,0,24,0z M24,45.8C12,45.8,2.2,36,2.2,24C2.2,12,12,2.2,24,2.2S45.8,12,45.8,24C45.8,36,36,45.8,24,45.8z"></path><path class="youtubeTube" d="M36.9,36.9c-0.3,1.4-1.5,2.5-2.9,2.6c-3.3,0.4-6.7,0.4-10,0.4c-3.3,0-6.7,0-10-0.4 c-1.4-0.2-2.6-1.2-2.9-2.6c-0.5-2-0.5-4.2-0.5-6.3c0-2.1,0-4.3,0.5-6.3c0.3-1.4,1.5-2.5,2.9-2.6c3.3-0.4,6.7-0.4,10-0.4 c3.3,0,6.7,0,10,0.4c1.4,0.2,2.6,1.2,2.9,2.6c0.5,2,0.5,4.2,0.5,6.3C37.4,32.6,37.4,34.8,36.9,36.9z M12.6,25.8h1.9v10.3h1.8V25.8 h1.9v-1.7h-5.6V25.8z M27.7,33.5c0,0.8-0.2,1.2-0.7,1.2c-0.3,0-0.5-0.1-0.8-0.4v-5.4c0.3-0.3,0.5-0.4,0.8-0.4 c0.5,0,0.7,0.4,0.7,1.2V33.5z M27.8,27c-0.6,0-1.2,0.3-1.7,1v-3.9h-1.6v12h1.6v-0.9c0.5,0.7,1.1,1,1.7,1c0.6,0,1.1-0.3,1.3-1 c0.1-0.4,0.2-1,0.2-1.8v-3.6c0-0.8-0.1-1.4-0.2-1.8C28.9,27.4,28.5,27,27.8,27z M21.5,34c-0.4,0.5-0.7,0.8-1,0.8 c-0.2,0-0.3-0.1-0.4-0.4c0-0.1,0-0.2,0-0.6v-6.6h-1.6v7.1c0,0.6,0.1,1.1,0.1,1.3c0.2,0.5,0.5,0.7,1,0.7c0.6,0,1.2-0.4,1.8-1.1v1 h1.6v-8.9h-1.6V34z M33.8,30.6h-1.6v-0.8c0-0.8,0.3-1.2,0.8-1.2c0.5,0,0.8,0.4,0.8,1.2V30.6z M33,27c-0.8,0-1.5,0.3-1.9,0.9 c-0.3,0.5-0.5,1.2-0.5,2.1v3.1c0,0.9,0.2,1.7,0.5,2.1c0.5,0.6,1.1,0.9,2,0.9c0.8,0,1.5-0.3,2-1c0.2-0.3,0.3-0.6,0.4-1 c0-0.2,0-0.5,0-1V33h-1.7c0,0.6,0,1,0,1.1c-0.1,0.4-0.3,0.6-0.7,0.6c-0.6,0-0.8-0.4-0.8-1.2v-1.6h3.2V30c0-1-0.2-1.7-0.5-2.1 C34.4,27.3,33.8,27,33,27z"></path><path class="youtubeU" d="M26.7,11.2h1.6v6.6c0,0.4,0,0.6,0,0.6c0,0.3,0.2,0.4,0.4,0.4c0.3,0,0.7-0.3,1-0.8v-6.9h1.6v9 h-1.6v-1c-0.7,0.7-1.3,1.1-1.9,1.1c-0.5,0-0.9-0.2-1.1-0.7c-0.1-0.3-0.1-0.7-0.1-1.3V11.2z"></path><path class="youtubeO" d="M20.5,14.2c0-1,0.2-1.7,0.5-2.1c0.5-0.6,1.1-0.9,1.9-0.9c0.8,0,1.5,0.3,1.9,0.9 c0.3,0.5,0.5,1.2,0.5,2.1v3.2c0,1-0.2,1.7-0.5,2.1c-0.5,0.6-1.1,0.9-1.9,0.9c-0.8,0-1.5-0.3-1.9-0.9c-0.3-0.5-0.5-1.2-0.5-2.1 V14.2z M22.9,18.9c0.5,0,0.8-0.4,0.8-1.3v-3.8c0-0.8-0.3-1.3-0.8-1.3c-0.5,0-0.8,0.4-0.8,1.3v3.8C22.1,18.5,22.4,18.9,22.9,18.9z"></path><path class="youtubeY" d="M16.1,8.1l1.3,4.8l1.2-4.8h1.8l-2.2,7.2v4.9h-1.8v-4.9c-0.2-0.9-0.5-2.1-1.1-3.8 c-0.4-1.1-0.8-2.3-1.2-3.4H16.1z"></path><g class="youtubeLogo">
  <path class="youtubeTube" d="M36.9,36.9c-0.3,1.4-1.5,2.5-2.9,2.6c-3.3,0.4-6.7,0.4-10,0.4c-3.3,0-6.7,0-10-0.4 c-1.4-0.2-2.6-1.2-2.9-2.6c-0.5-2-0.5-4.2-0.5-6.3c0-2.1,0-4.3,0.5-6.3c0.3-1.4,1.5-2.5,2.9-2.6c3.3-0.4,6.7-0.4,10-0.4 c3.3,0,6.7,0,10,0.4c1.4,0.2,2.6,1.2,2.9,2.6c0.5,2,0.5,4.2,0.5,6.3C37.4,32.6,37.4,34.8,36.9,36.9z M12.6,25.8h1.9v10.3h1.8V25.8 h1.9v-1.7h-5.6V25.8z M27.7,33.5c0,0.8-0.2,1.2-0.7,1.2c-0.3,0-0.5-0.1-0.8-0.4v-5.4c0.3-0.3,0.5-0.4,0.8-0.4 c0.5,0,0.7,0.4,0.7,1.2V33.5z M27.8,27c-0.6,0-1.2,0.3-1.7,1v-3.9h-1.6v12h1.6v-0.9c0.5,0.7,1.1,1,1.7,1c0.6,0,1.1-0.3,1.3-1 c0.1-0.4,0.2-1,0.2-1.8v-3.6c0-0.8-0.1-1.4-0.2-1.8C28.9,27.4,28.5,27,27.8,27z M21.5,34c-0.4,0.5-0.7,0.8-1,0.8 c-0.2,0-0.3-0.1-0.4-0.4c0-0.1,0-0.2,0-0.6v-6.6h-1.6v7.1c0,0.6,0.1,1.1,0.1,1.3c0.2,0.5,0.5,0.7,1,0.7c0.6,0,1.2-0.4,1.8-1.1v1 h1.6v-8.9h-1.6V34z M33.8,30.6h-1.6v-0.8c0-0.8,0.3-1.2,0.8-1.2c0.5,0,0.8,0.4,0.8,1.2V30.6z M33,27c-0.8,0-1.5,0.3-1.9,0.9 c-0.3,0.5-0.5,1.2-0.5,2.1v3.1c0,0.9,0.2,1.7,0.5,2.1c0.5,0.6,1.1,0.9,2,0.9c0.8,0,1.5-0.3,2-1c0.2-0.3,0.3-0.6,0.4-1 c0-0.2,0-0.5,0-1V33h-1.7c0,0.6,0,1,0,1.1c-0.1,0.4-0.3,0.6-0.7,0.6c-0.6,0-0.8-0.4-0.8-1.2v-1.6h3.2V30c0-1-0.2-1.7-0.5-2.1 C34.4,27.3,33.8,27,33,27z"></path>
  <path class="youtubeU" d="M26.7,11.2h1.6v6.6c0,0.4,0,0.6,0,0.6c0,0.3,0.2,0.4,0.4,0.4c0.3,0,0.7-0.3,1-0.8v-6.9h1.6v9 h-1.6v-1c-0.7,0.7-1.3,1.1-1.9,1.1c-0.5,0-0.9-0.2-1.1-0.7c-0.1-0.3-0.1-0.7-0.1-1.3V11.2z"></path>
  <path class="youtubeO" d="M20.5,14.2c0-1,0.2-1.7,0.5-2.1c0.5-0.6,1.1-0.9,1.9-0.9c0.8,0,1.5,0.3,1.9,0.9 c0.3,0.5,0.5,1.2,0.5,2.1v3.2c0,1-0.2,1.7-0.5,2.1c-0.5,0.6-1.1,0.9-1.9,0.9c-0.8,0-1.5-0.3-1.9-0.9c-0.3-0.5-0.5-1.2-0.5-2.1 V14.2z M22.9,18.9c0.5,0,0.8-0.4,0.8-1.3v-3.8c0-0.8-0.3-1.3-0.8-1.3c-0.5,0-0.8,0.4-0.8,1.3v3.8C22.1,18.5,22.4,18.9,22.9,18.9z"></path>
  <path class="youtubeY" d="M16.1,8.1l1.3,4.8l1.2-4.8h1.8l-2.2,7.2v4.9h-1.8v-4.9c-0.2-0.9-0.5-2.1-1.1-3.8 c-0.4-1.1-0.8-2.3-1.2-3.4H16.1z"></path>
</g></symbol><symbol id="grid"><desc id="gridDesc">
      Grid icon
    </desc><rect id="gridSquare_8" y="14" width="4" height="4"></rect><rect id="gridSquare_7" y="7" width="4" height="4"></rect><rect id="gridSquare_6" width="4" height="4"></rect><rect id="gridSquare_5" x="7" y="14" width="4" height="4"></rect><rect id="gridSquare_4" x="7" y="7" width="4" height="4"></rect><rect id="gridSquare_3" x="7" width="4" height="4"></rect><rect id="gridSquare_2" x="14" y="14" width="4" height="4"></rect><rect id="gridSquare_1" x="14" y="7" width="4" height="4"></rect><rect id="gridSquare" x="14" width="4" height="4"></rect></symbol><symbol id="key"><desc id="keyDesc">
      Key icon
    </desc><path id="keyKey" d="M21.6,3.6H11.5C10.6,1.5,8.5,0,6,0C2.7,0,0,2.7,0,6s2.7,6,6,6c2.5,0,4.6-1.5,5.5-3.6h1.7l1.2-1.2 l1.2,1.2H18l1.2-1.2l1.2,1.2h1.2L24,6L21.6,3.6z"></path></symbol><symbol id="left"><desc id="leftDesc">
      Left arrow icon
    </desc><polygon id="leftArrow" points="4.2,12 13,20.8 14,21.9 11.9,24 10.9,23 1.1,13.1 0,12 0,12 0,12 1,10.9 10.9,1.1 11.9,0 14,2.1 13,3.2  "></polygon></symbol><symbol id="link"><desc id="linkDesc">
      Link icon
    </desc><path id="linkCircle" d="M24,0C10.7,0,0,10.7,0,24c0,13.3,10.7,24,24,24s24-10.7,24-24C48,10.7,37.3,0,24,0z M24,45.8 C12,45.8,2.2,36,2.2,24C2.2,12,12,2.2,24,2.2S45.8,12,45.8,24C45.8,36,36,45.8,24,45.8z"></path><g id="linkChain">
  <rect x="21.9" y="18.1" transform="matrix(0.7071 -0.7071 0.7071 0.7071 -9.7152 23.4544)" width="3.1" height="10.8"></rect>
  <polygon points="29.5,20.7 27.3,22.9 32.7,28.4 28.4,32.7 22.9,27.3 20.7,29.5 28.4,37.1 37.1,28.4"></polygon>
  <polygon points="18.5,14.2 24,19.6 26.2,17.5 18.5,9.8 9.8,18.5 17.5,26.2 19.6,24 14.2,18.5"></polygon>
</g><polygon points="29.5,20.7 27.3,22.9 32.7,28.4 28.4,32.7 22.9,27.3 20.7,29.5 28.4,37.1 37.1,28.4"></polygon><polygon points="18.5,14.2 24,19.6 26.2,17.5 18.5,9.8 9.8,18.5 17.5,26.2 19.6,24 14.2,18.5"></polygon><rect x="21.9" y="18.1" transform="matrix(0.7071 -0.7071 0.7071 0.7071 -9.7152 23.4544)" width="3.1" height="10.8"></rect></symbol><symbol id="location"><desc id="locationDesc">
      Location icon
    </desc><path id="locationPin" d="M6,3.2c-1.5,0-2.7,1.2-2.7,2.6c0,1.4,1.2,2.6,2.7,2.6s2.7-1.2,2.7-2.6C8.7,4.4,7.5,3.2,6,3.2 z M6,0L6,0c3.3,0,6,2.6,6,5.8c0,1.2-0.8,2.9-1.3,3.9L6,18L1.3,9.6C0.8,8.7,0,6.9,0,5.8C0,2.6,2.7,0,6,0z"></path></symbol><symbol id="mail"><desc id="mailDesc">
      Mail icon
    </desc><path id="mailEnvelope" d="M0.7,11h18.5V1.1l-9,7c-0.1,0.1-0.2,0.1-0.2,0.1s-0.2,0-0.2-0.1l-9-7V11z M18.6,0.7H1.4 L10,7.4L18.6,0.7z M20,0.3C20,0.3,20,0.3,20,0.3l0,11c0,0.2-0.2,0.3-0.4,0.3H0.4c-0.2,0-0.4-0.2-0.4-0.3v-11c0,0,0,0,0,0 c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0 c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0h19.3c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0 c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0 c0,0,0,0,0,0C20,0.2,20,0.3,20,0.3C20,0.3,20,0.3,20,0.3C20,0.3,20,0.3,20,0.3z"></path></symbol><symbol id="menu"><desc id="menuDesc">
      Menu icon
    </desc><path id="menuHamburger" d="M0,6h24v2H0V6z M0,11h24v2H0V11z M0,16h24v2H0V16z"></path></symbol><symbol id="open"><desc id="openDesc">
      Open icon
    </desc><polygon id="openArrow" points="13.5,18.3 13.5,0 10.5,0 10.5,18.3 2.2,10.2 0,12.3 12,24 24,12.3 21.8,10.2 "></polygon></symbol><symbol id="phone"><desc id="phoneDesc">
      Phone icon
    </desc><path id="phoneHandset" d="M14.8,14.3c-0.7,0.7-1.4,1.1-2.4,1.1c-1.7,0-4-1.1-6.6-3.2c-2.6-2-5.5-5.1-5-8.7 C0.9,2.4,1.6,1,2.4,0.7l0,0c0.2,0,0.6,0.3,0.8,0.5l0.1,0.1c0.4,0.3,0.8,0.7,1.2,1.1l0.2,0.2C5,2.7,5.2,2.9,5.3,3.2 c0.3,0.4,0.2,0.6-0.3,1.4C4.9,4.8,4.4,5.3,4.1,5.6l0,0l0,0C3.8,6.1,4,6.6,4.5,7.2C4.7,7.6,4.9,8,5,8.1c0.4,0.6,0.9,1.1,1.4,1.6 c0.5,0.5,1.1,0.9,1.7,1.3c0.1,0.1,1.5,1.1,2,1.1c0.2,0,0.4-0.1,0.5-0.2l0.7-0.7c0.1-0.1,0.2-0.2,0.3-0.3c0.2-0.2,0.4-0.5,0.6-0.5 c0.3,0,1.1,0.7,1.4,0.9c0.2,0.1,0.3,0.3,0.4,0.3c0.6,0.4,1.3,1,1.3,1.6C15.3,13.4,15.3,13.7,14.8,14.3 M14.4,11.1 c-0.5-0.4-1.6-1.4-2.2-1.4c-0.6,0-1,0.6-1.3,1l-0.7,0.7c-0.1,0.1-1.5-0.8-1.7-0.9c-0.6-0.4-1.1-0.8-1.6-1.3c-0.5-0.5-0.9-1-1.3-1.5 c-0.1-0.2-1-1.5-0.9-1.6c0,0,0.8-0.8,1-1.2c0.5-0.7,0.8-1.3,0.3-2.1C5.7,2.5,5.5,2.3,5.2,2C4.7,1.5,4.3,1.1,3.7,0.7 C3.5,0.5,3,0.1,2.5,0C2.4,0,2.3,0,2.2,0C1,0.5,0.2,2.2,0.1,3.4c-0.6,3.8,2.5,7.1,5.2,9.3c2.5,1.9,7.2,5.1,10,2 c0.4-0.4,0.8-0.9,0.8-1.5C15.9,12.3,15.1,11.6,14.4,11.1"></path></symbol><symbol id="play"><desc id="playDesc">
      Play icon
    </desc><path id="playBackground" d="M24,0C10.7,0,0,10.7,0,24s10.7,24,24,24s24-10.7,24-24S37.3,0,24,0z M19.6,32.7V15.3   L32.7,24L19.6,32.7z"></path><polygon id="playForeground" points="19.6,32.7 32.7,24 19.6,15.3  "></polygon></symbol><symbol id="radio"><desc id="radioDesc">
      Radio icon
    </desc><path id="radioBody" d="M5,3.8V0H2.5v3.8H0v13.8h20V3.8H5z M5.6,13.8c-1.7,0-3.1-1.4-3.1-3.1s1.4-3.1,3.1-3.1 s3.1,1.4,3.1,3.1S7.4,13.8,5.6,13.8z M17.5,10H15V7.5h2.5V10z"></path></symbol><symbol id="rewind"><desc id="rewindDesc">
      Rewind icon
    </desc><path id="rewindArrow" d="M11.3,0c-3.5,0-6.7,1.4-9,3.7L0,1.4v5.6h5.6L3.3,4.7c2-2,4.9-3.3,8-3.3 c6.2,0,11.3,5.1,11.3,11.3S17.5,24,11.3,24c-3.1,0-5.9-1.3-8-3.3l-1,1c2.3,2.3,5.5,3.7,9,3.7c7,0,12.7-5.7,12.7-12.7S18.3,0,11.3,0 "></path></symbol><symbol id="right"><desc id="rightDesc">
      Right arrow icon
    </desc><polygon id="rightArrow" points="9.8,12 1,20.8 0,21.9 2.1,24 3.1,23 12.9,13.1 14,12 14,12 14,12 13,10.9 3.1,1.1 2.1,0 0,2.1 1,3.2   "></polygon></symbol><symbol id="search"><desc id="searchDesc">
      Search icon
    </desc><path id="searchLens" d="M9.7,17.7c4.3,0,7.9-3.5,7.9-7.9S14,1.8,9.7,1.8S1.8,5.4,1.8,9.7S5.3,17.7,9.7,17.7z M17.4,15.7l6.6,6.6L22.3,24l-6.6-6.6c-1.6,1.3-3.7,2.1-6,2.1C4.3,19.5,0,15.1,0,9.7S4.3,0,9.7,0c5.3,0,9.7,4.4,9.7,9.7 C19.4,12,18.6,14,17.4,15.7z"></path></symbol><symbol id="select"><desc id="selectDesc">
      Select icon
    </desc><path class="selectCircle" d="M12,23c6.1,0,11-4.9,11-11S18.1,1,12,1S1,5.9,1,12S5.9,23,12,23z M12,24C5.4,24,0,18.6,0,12 S5.4,0,12,0s12,5.4,12,12S18.6,24,12,24z"></path><circle class="selectBackground" cx="12" cy="12" r="12"></circle></symbol><symbol id="selected"><desc id="selectedDesc">
      Selected icon
    </desc><path class="selectedFlag" d="M10.1,16.8c2.9-2.9,5.8-5.9,8.7-8.9c-0.3-0.3-0.6-0.6-0.9-0.9c-2.6,2.6-5.2,5.3-7.8,8 c-1.1-1.1-2.1-2.2-3.2-3.2C6.6,12,6.3,12.3,6,12.6C7.4,14,8.7,15.4,10.1,16.8"></path><circle class="selectedBackground" cx="12" cy="12" r="12"></circle></symbol><symbol id="tv"><desc id="tvDesc">
      TV icon
    </desc><polygon id="tvScreen" points="0,0 0,11.2 8.8,11.2 8.8,13.8 5,13.8 5,15 15,15 15,13.8 11.2,13.8 11.2,11.2 20,11.2 20,0  "></polygon></symbol><symbol id="twitter"><desc id="twitterDesc">
      Twitter logo
    </desc><path class="twitterBackground" d="M24,0C10.7,0,0,10.7,0,24s10.7,24,24,24s24-10.7,24-24S37.3,0,24,0z"></path><path class="twitterCircle" d="M24,0C10.7,0,0,10.7,0,24c0,13.3,10.7,24,24,24s24-10.7,24-24C48,10.7,37.3,0,24,0z M24,45.8C12,45.8,2.2,36,2.2,24C2.2,12,12,2.2,24,2.2S45.8,12,45.8,24C45.8,36,36,45.8,24,45.8z"></path><path class="twitterBird" d="M34.7,17.3c-0.6,0.9-1.3,1.7-2.2,2.3c0,0.2,0,0.4,0,0.6c0,5.8-4.4,12.5-12.5,12.5 c-2.5,0-4.8-0.7-6.8-2c0.3,0,0.7,0.1,1.1,0.1c2.1,0,4-0.7,5.5-1.9c-1.9,0-3.6-1.3-4.1-3.1c0.3,0.1,0.5,0.1,0.8,0.1 c0.4,0,0.8-0.1,1.2-0.2c-2-0.4-3.5-2.2-3.5-4.3v-0.1c0.6,0.3,1.3,0.5,2,0.6c-1.2-0.8-2-2.1-2-3.7c0-0.8,0.2-1.6,0.6-2.2 c2.2,2.7,5.4,4.4,9.1,4.6c-0.1-0.3-0.1-0.7-0.1-1c0-2.4,2-4.4,4.4-4.4c1.3,0,2.4,0.5,3.2,1.4c1-0.2,1.9-0.6,2.8-1.1 c-0.3,1-1,1.9-1.9,2.4C33.1,17.9,33.9,17.7,34.7,17.3z"></path></symbol><symbol id="twitter-no-circle"><desc id="twitter-no-circleDesc">
      Twitter logo
    </desc><path class="twitterBackground" d="M24,0C10.7,0,0,10.7,0,24s10.7,24,24,24s24-10.7,24-24S37.3,0,24,0z"></path><path class="twitterBird" d="M34.7,17.3c-0.6,0.9-1.3,1.7-2.2,2.3c0,0.2,0,0.4,0,0.6c0,5.8-4.4,12.5-12.5,12.5 c-2.5,0-4.8-0.7-6.8-2c0.3,0,0.7,0.1,1.1,0.1c2.1,0,4-0.7,5.5-1.9c-1.9,0-3.6-1.3-4.1-3.1c0.3,0.1,0.5,0.1,0.8,0.1 c0.4,0,0.8-0.1,1.2-0.2c-2-0.4-3.5-2.2-3.5-4.3v-0.1c0.6,0.3,1.3,0.5,2,0.6c-1.2-0.8-2-2.1-2-3.7c0-0.8,0.2-1.6,0.6-2.2 c2.2,2.7,5.4,4.4,9.1,4.6c-0.1-0.3-0.1-0.7-0.1-1c0-2.4,2-4.4,4.4-4.4c1.3,0,2.4,0.5,3.2,1.4c1-0.2,1.9-0.6,2.8-1.1 c-0.3,1-1,1.9-1.9,2.4C33.1,17.9,33.9,17.7,34.7,17.3z"></path></symbol><symbol id="up"><desc id="upDesc">
      Up arrow icon
    </desc><polygon id="upArrow" points="12,4.2 20.8,13 21.9,14 24,11.9 23,10.9 13.1,1.1 12,0 12,0 12,0 10.9,1 1.1,10.9 0,11.9 2.1,14 3.2,13 "></polygon></symbol><symbol id="user"><desc id="userDesc">
      User icon
    </desc><path id="userAvatar" d="M24.2,24.1c-0.9-0.9-2-1.6-3.1-2.2c-0.4-0.4-0.6-1-0.5-1.6c0.2-0.8,0.8-0.9,1.4-2.4 c0.4-1.4,0.6-2.9,0.5-4.4c0,0-0.1-3.2-2.4-4.8c-1.2-0.8-2.6-1.2-4-1.2c-1.4,0-2.7,0.4-3.9,1.2c-2.3,1.6-2.4,4.8-2.4,4.8 c-0.2,1.5,0,3,0.5,4.4c0.5,1.5,1.2,1.6,1.4,2.4c0.1,0.6-0.1,1.2-0.5,1.6c-1.2,0.6-2.2,1.3-3.1,2.2c-0.7,0.8-1.4,1.7-1.9,2.6 C3,23.9,1.3,20.1,1.3,16C1.3,7.9,7.9,1.3,16,1.3S30.7,7.9,30.7,16c0,4-1.7,7.9-4.6,10.7C25.5,25.7,24.9,24.9,24.2,24.1z M16,0 C7.2,0,0,7.2,0,16s7.2,16,16,16s16-7.2,16-16c0-4.2-1.7-8.3-4.7-11.3C24.3,1.7,20.2,0,16,0z"></path></symbol><symbol id="audio"><desc id="audioDesc">
      Audio icon
    </desc><path id="audioBackground" d="M12,1C5.9,1,1,5.9,1,12s4.9,11,11,11s11-4.9,11-11S18.1,1,12,1z"></path><path id="audioForeground" d="M14.1,14.3c-0.5,0.5-1.1,0.8-1.8,0.9v1.1h1v0.6h-2.6v-0.6h1v-1.1c-0.7-0.1-1.3-0.4-1.8-0.9    C9.3,13.8,9,13,9,12.2l0.6,0c0,1.3,1.1,2.4,2.4,2.4s2.4-1.1,2.4-2.4l0.6,0C15,13,14.7,13.8,14.1,14.3z M14.1,9.4h-1V10h1v0.5h-1    v0.6h1v0.5h-1v0.6h1c0,1.1-0.9,2.1-2.1,2.1s-2.1-0.9-2.1-2.1h1v-0.6h-1v-0.5h1v-0.6h-1V10h1V9.4h-1V9.2c0-1.1,0.9-2.1,2.1-2.1    s2.1,0.9,2.1,2.1V9.4z"></path></symbol><symbol id="tickets"><desc id="ticketsDesc">Tickets icon</desc><path d="M13.2283,12.722 L13.9873,15.058 L12.0003,13.614 L10.0123,15.058 L10.7713,12.722 L8.7843,11.278 L11.2413,11.278 L12.0003,8.942 L12.7593,11.278 L15.2153,11.278 L13.2283,12.722 Z M14.0003,3 C14.0003,4.105 13.1043,5 12.0003,5 C10.8953,5 10.0003,4.105 10.0003,3 L7.0003,3 L7.0003,21 L10.0003,21 C10.0003,19.895 10.8953,19 12.0003,19 C13.1043,19 14.0003,19.895 14.0003,21 L17.0003,21 L17.0003,3 L14.0003,3 Z"></path></symbol><symbol id="addToCalendar"><desc id="addToCalendarDesc">Add to calendar icon</desc><path d="M13.8507463,8.26268657 L13.8507463,1.91044776 L13.8507463,1.43283582 L10.9850746,1.43283582 L10.9850746,0 L10.5074627,0 L8.8358209,0 C8.66069652,0 8.50149254,0 8.35820896,0 L8.35820896,1.43283582 L5.49253731,1.43283582 L5.49253731,0 C5.34925373,0 5.19004975,0 5.01492537,0 L3.34328358,0 C3.1681592,0 3.00895522,0 2.86567164,0 L2.86567164,1.43283582 L0,1.43283582 L0,1.91044776 L0,12.8955224 L0,13.3731343 L8.74029851,13.3731343 C9.36119403,14.638806 10.6746269,15.5223881 12.1791045,15.5223881 C14.280597,15.5223881 16,13.8029851 16,11.7014925 C16,10.1970149 15.1164179,8.88358209 13.8507463,8.26268657 Z M9.31343284,0.955223881 L10.0298507,0.955223881 L10.0298507,2.86567164 L9.31343284,2.86567164 L9.31343284,0.955223881 Z M3.82089552,0.955223881 L4.53731343,0.955223881 L4.53731343,2.86567164 L3.82089552,2.86567164 L3.82089552,0.955223881 Z M2.86567164,2.3880597 L2.86567164,3.82089552 L3.34328358,3.82089552 L5.01492537,3.82089552 C5.19004975,3.82089552 5.34925373,3.82089552 5.49253731,3.82089552 L5.49253731,2.3880597 L8.35820896,2.3880597 L8.35820896,3.82089552 C8.50149254,3.82089552 8.66069652,3.82089552 8.8358209,3.82089552 L10.5074627,3.82089552 C10.6825871,3.82089552 10.841791,3.82089552 10.9850746,3.82089552 L10.9850746,2.3880597 L12.8955224,2.3880597 L12.8955224,4.7761194 L0.955223881,4.7761194 L0.955223881,2.3880597 L2.86567164,2.3880597 Z M0.955223881,12.4179104 L0.955223881,5.73134328 L12.8955224,5.73134328 L12.8955224,7.95223881 C12.6567164,7.90447761 12.4179104,7.88059701 12.1791045,7.88059701 C10.0776119,7.88059701 8.35820896,9.6 8.35820896,11.7014925 C8.35820896,11.9402985 8.38208955,12.1791045 8.42985075,12.4179104 L0.955223881,12.4179104 Z M12.1791045,14.5671642 C10.6029851,14.5671642 9.31343284,13.2776119 9.31343284,11.7014925 C9.31343284,10.1253731 10.6029851,8.8358209 12.1791045,8.8358209 C13.7552239,8.8358209 15.0447761,10.1253731 15.0447761,11.7014925 C15.0447761,13.2776119 13.7552239,14.5671642 12.1791045,14.5671642 Z M13.8507463,11.7014925 L13.8507463,12.1791045 L12.6567164,12.1791045 L12.6567164,13.3731343 L12.1791045,13.3731343 L11.7014925,13.3731343 L11.7014925,12.1791045 L10.5074627,12.1791045 L10.5074627,11.7014925 L10.5074627,11.2238806 L11.7014925,11.2238806 L11.7014925,10.0298507 L12.1791045,10.0298507 L12.6567164,10.0298507 L12.6567164,11.2238806 L13.8507463,11.2238806 L13.8507463,11.7014925 Z"></path></symbol><symbol id="NFC"><desc id="NFCDesc">NFC icon</desc><path d="M0 0h500v500h-500z"></path><path fill="#fff" d="M276.127 95.338v68.563l-65.7-68.563h-165.427v83.011l29.028 6.106v134.612l-27.907 6.928v78.554h189.703v-67.909l67.166 67.95h127.018v-220.539l24.992-5.903v-82.81z"></path><path d="M300.744 283.954l7.439 23.488-20.08-15.205-20.067 14.415 7.767-22.965-18.901-15.205h23.495l7.706-21.979 7.679 21.979h24.43l-19.469 15.471zm47.035 19.514l7.692 22.036h24.469l-19.493 15.385 7.439 23.545-20.106-15.248-20.061 14.436 7.827-22.947-18.928-15.172h23.483l7.679-22.036zm-119.417-68.251l-20.094 14.436 7.778-22.945-18.864-15.19h23.47l7.71-22.014 7.622 22.014h24.493l-19.481 15.387 7.39 23.539-20.024-15.227zm-47.105-65.299l7.446 23.537-20.088-15.207-20.067 14.458 7.79-23.018-18.897-15.164h23.477l7.698-21.971 7.655 21.971h24.508l-19.52 15.393zm110.042-3.809l20.248 6.138v50.549l-107.594-112.285h-143.781v55.521l29.028 6.104v158.797l-27.907 6.93v51.511h159.359v-51.388l-23.844-6.996v-55.292l112.516 113.697h105.511v-217.357l24.992-5.901v-55.628h-148.529v55.597z" fill="#013369"></path><path d="M176.27 154.525h24.508l-19.52 15.393 7.446 23.537-20.088-15.207-20.067 14.458 7.79-23.018-18.897-15.164h23.477l7.698-21.971 7.655 21.971zm84.208 56.993l-19.481 15.387 7.39 23.539-20.024-15.227-20.094 14.436 7.778-22.945-18.864-15.19h23.47l7.71-22.014 7.622 22.014h24.493zm27.626 34.986l7.679 21.979h24.43l-19.469 15.471 7.439 23.488-20.08-15.205-20.067 14.415 7.767-22.965-18.901-15.205h23.495l7.706-21.979zm47.441 94.173l-18.928-15.172h23.483l7.679-22.036 7.692 22.036h24.469l-19.493 15.385 7.439 23.545-20.106-15.248-20.061 14.436 7.827-22.947z" fill="#fff"></path><g fill="none">
  <path d="M0 0h500v500h-500z"></path>
  <path fill="#fff" d="M276.127 95.338v68.563l-65.7-68.563h-165.427v83.011l29.028 6.106v134.612l-27.907 6.928v78.554h189.703v-67.909l67.166 67.95h127.018v-220.539l24.992-5.903v-82.81z"></path>
  <path d="M300.744 283.954l7.439 23.488-20.08-15.205-20.067 14.415 7.767-22.965-18.901-15.205h23.495l7.706-21.979 7.679 21.979h24.43l-19.469 15.471zm47.035 19.514l7.692 22.036h24.469l-19.493 15.385 7.439 23.545-20.106-15.248-20.061 14.436 7.827-22.947-18.928-15.172h23.483l7.679-22.036zm-119.417-68.251l-20.094 14.436 7.778-22.945-18.864-15.19h23.47l7.71-22.014 7.622 22.014h24.493l-19.481 15.387 7.39 23.539-20.024-15.227zm-47.105-65.299l7.446 23.537-20.088-15.207-20.067 14.458 7.79-23.018-18.897-15.164h23.477l7.698-21.971 7.655 21.971h24.508l-19.52 15.393zm110.042-3.809l20.248 6.138v50.549l-107.594-112.285h-143.781v55.521l29.028 6.104v158.797l-27.907 6.93v51.511h159.359v-51.388l-23.844-6.996v-55.292l112.516 113.697h105.511v-217.357l24.992-5.901v-55.628h-148.529v55.597z" fill="#013369"></path>
  <path d="M176.27 154.525h24.508l-19.52 15.393 7.446 23.537-20.088-15.207-20.067 14.458 7.79-23.018-18.897-15.164h23.477l7.698-21.971 7.655 21.971zm84.208 56.993l-19.481 15.387 7.39 23.539-20.024-15.227-20.094 14.436 7.778-22.945-18.864-15.19h23.47l7.71-22.014 7.622 22.014h24.493zm27.626 34.986l7.679 21.979h24.43l-19.469 15.471 7.439 23.488-20.08-15.205-20.067 14.415 7.767-22.965-18.901-15.205h23.495l7.706-21.979zm47.441 94.173l-18.928-15.172h23.483l7.679-22.036 7.692 22.036h24.469l-19.493 15.385 7.439 23.545-20.106-15.248-20.061 14.436 7.827-22.947z" fill="#fff"></path>
</g></symbol><symbol id="AFC"><desc id="AFCDesc">AFC icon</desc><path d="M0 0h500v500h-500z"></path><path fill="#fff" d="M217.375 264.305h37.842l-20.046-59.716-17.796 59.716"></path><path fill="#fff" d="M431.992 317.729l-56.233-125.998 29.937-7.517v-88.645h-310.385v88.593l29.539 7.505-56.991 126.129-32.859 8.019v78.325h431v-78.436z"></path><path d="M378.261 372.971l-19.436-14.734-19.384 14.027 7.519-22.221-18.302-14.682h22.752l7.415-21.205 7.418 21.205h23.647l-18.809 14.906 7.18 22.703zm-79.323-98.369h22.718l7.461-21.22 7.372 21.22h23.675l-18.83 14.917 7.206 22.692-19.423-14.719-19.436 14.036 7.555-22.245-18.298-14.682zm-28.976-62.4h22.757l7.424-21.213 7.396 21.213h23.66l-18.839 14.927 7.18 22.688-19.397-14.729-19.423 13.984 7.527-22.202-18.285-14.669zm-20.828-24.692l7.53-22.18-18.218-14.675h22.699l7.443-21.241 7.383 21.241h23.683l-18.858 14.895 7.198 22.714-19.406-14.734-19.453 13.98zm-31.758 76.796l17.796-59.716 20.046 59.716h-37.842zm137.2-82.863l35.965-9.035v-61.701h-280.072v61.684l35.631 9.052-67.464 149.308-28.478 6.949v51.301h171.527v-51.301l-24.306-6.915 7.782-21.864h60.521l7.241 21.907-22.421 6.872v51.301h200.348v-51.301l-29.636-6.949-66.637-149.308z" fill="#D50A0A"></path><path d="M280.795 165.549l7.198 22.714-19.406-14.734-19.453 13.98 7.53-22.18-18.218-14.675h22.699l7.443-21.241 7.383 21.241h23.683l-18.858 14.895zm19.348 69.539l-19.423 13.984 7.527-22.202-18.285-14.669h22.757l7.424-21.213 7.396 21.213h23.66l-18.839 14.927 7.18 22.688-19.397-14.729zm28.974 62.405l-19.436 14.036 7.555-22.245-18.298-14.682h22.718l7.461-21.22 7.372 21.22h23.675l-18.83 14.917 7.206 22.692-19.423-14.719zm37.126 37.87h23.647l-18.809 14.906 7.18 22.703-19.436-14.734-19.384 14.027 7.519-22.221-18.302-14.682h22.752l7.415-21.205 7.418 21.205z" fill="#fff"></path><g fill="none">
  <path d="M0 0h500v500h-500z"></path>
  <path fill="#fff" d="M217.375 264.305h37.842l-20.046-59.716-17.796 59.716"></path>
  <path fill="#fff" d="M431.992 317.729l-56.233-125.998 29.937-7.517v-88.645h-310.385v88.593l29.539 7.505-56.991 126.129-32.859 8.019v78.325h431v-78.436z"></path>
  <path d="M378.261 372.971l-19.436-14.734-19.384 14.027 7.519-22.221-18.302-14.682h22.752l7.415-21.205 7.418 21.205h23.647l-18.809 14.906 7.18 22.703zm-79.323-98.369h22.718l7.461-21.22 7.372 21.22h23.675l-18.83 14.917 7.206 22.692-19.423-14.719-19.436 14.036 7.555-22.245-18.298-14.682zm-28.976-62.4h22.757l7.424-21.213 7.396 21.213h23.66l-18.839 14.927 7.18 22.688-19.397-14.729-19.423 13.984 7.527-22.202-18.285-14.669zm-20.828-24.692l7.53-22.18-18.218-14.675h22.699l7.443-21.241 7.383 21.241h23.683l-18.858 14.895 7.198 22.714-19.406-14.734-19.453 13.98zm-31.758 76.796l17.796-59.716 20.046 59.716h-37.842zm137.2-82.863l35.965-9.035v-61.701h-280.072v61.684l35.631 9.052-67.464 149.308-28.478 6.949v51.301h171.527v-51.301l-24.306-6.915 7.782-21.864h60.521l7.241 21.907-22.421 6.872v51.301h200.348v-51.301l-29.636-6.949-66.637-149.308z" fill="#D50A0A"></path>
  <path d="M280.795 165.549l7.198 22.714-19.406-14.734-19.453 13.98 7.53-22.18-18.218-14.675h22.699l7.443-21.241 7.383 21.241h23.683l-18.858 14.895zm19.348 69.539l-19.423 13.984 7.527-22.202-18.285-14.669h22.757l7.424-21.213 7.396 21.213h23.66l-18.839 14.927 7.18 22.688-19.397-14.729zm28.974 62.405l-19.436 14.036 7.555-22.245-18.298-14.682h22.718l7.461-21.22 7.372 21.22h23.675l-18.83 14.917 7.206 22.692-19.423-14.719zm37.126 37.87h23.647l-18.809 14.906 7.18 22.703-19.436-14.734-19.384 14.027 7.519-22.221-18.302-14.682h22.752l7.415-21.205 7.418 21.205z" fill="#fff"></path>
</g></symbol><symbol id="NFL"><desc id="NFLDesc">NFL icon</desc><path fill="#FFFFFF" d="M167.963,22.75c-6.259,4.49-18.23,10.61-33.632,6.815C107.64,22.99,100,0,100,0     s-7.639,22.994-34.331,29.565C50.26,33.36,38.291,27.238,32.038,22.75h-4.819v123.974c0.002,3.137,0.766,9.309,5.852,15.488     c6.281,7.63,16.935,12.578,31.658,14.705c11.729,1.697,20.4,5.384,26.516,11.269C95.737,192.511,100,200,100,200     s4.426-7.646,8.756-11.813c6.115-5.885,14.791-9.571,26.516-11.269c14.723-2.127,25.377-7.075,31.659-14.705     c5.082-6.175,5.847-12.354,5.851-15.488V22.75H167.963z"></path><path fill="#013369" d="M168.414,27.584c0,0-14.59,11.355-35.143,6.22C109.309,27.815,100,9.868,100,9.868     s-9.308,17.947-33.271,23.936c-20.552,5.136-35.142-6.22-35.142-6.22v119.134c0.003,4.049,1.695,21.243,33.768,25.878     c12.663,1.833,22.122,5.903,28.918,12.443c2.448,2.348,4.308,4.739,5.728,6.933c1.424-2.193,3.276-4.585,5.721-6.933     c6.804-6.54,16.263-10.61,28.926-12.443c32.07-4.635,33.762-21.829,33.768-25.878V27.584z"></path><path fill="#FFFFFF" enable-background="new    " d="M100,179.844c0,0,9.308-11.473,33.495-15.204     c27.705-4.275,26.882-17.284,26.884-17.943V84.304H39.622v62.392c0,0.656-0.818,13.667,26.883,17.943     C90.691,168.372,100,179.844,100,179.844z"></path><path fill="#D50A0A" d="M71.852,129.904l-0.004-29.673l-3.726-2.087V90.45H85.08v7.694l-3.546,2.087v60.527l-9.064-2.717       l-13.104-38.663l-0.002,25.489l4.161,3.196v7.855l-18.193-4.624v-6.696l4.163-1.628v-42.714l-4.265-2.112V90.45h13.768       L71.852,129.904z"></path><path fill="#D50A0A" d="M103.066,157.041l3.632,4.351v7.693l-18.021-6.733v-6.531l3.978-1.629v-53.96l-3.954-2V90.45h31.291       v15.659h-6.878l-2.084-6.429h-7.964v18.106h5.977l2.352-3.438h4.796v16.112h-4.796l-2.264-3.258h-6.064V157.041z"></path><path fill="#D50A0A" d="M122.96,159.765v-7.694l4.476-3.674v-48.166l-3.822-2V90.45h18.044v7.782l-3.546,2v45.188l7.754-2.095       l1.487-11.843h7.418v19.785L122.96,159.765z"></path><path fill="#FFFFFF" d="M42.761,71.54L37.3,67.431h6.674l2.204-6.323l2.204,6.323h6.674l-5.461,4.11l2.009,6.418l-5.426-3.812         l-5.426,3.812L42.761,71.54z"></path><path fill="#FFFFFF" d="M66.456,71.54l-5.461-4.109h6.674l2.203-6.323l2.204,6.323h6.675l-5.461,4.11l2.009,6.418l-5.427-3.812         l-5.426,3.812L66.456,71.54z"></path><path fill="#FFFFFF" d="M42.761,50.156L37.3,46.047h6.674l2.204-6.324l2.204,6.324h6.674l-5.461,4.109l2.009,6.418l-5.426-3.813         l-5.426,3.813L42.761,50.156z"></path><path fill="#FFFFFF" d="M66.456,50.156l-5.461-4.109h6.674l2.203-6.324l2.204,6.324h6.675l-5.461,4.109l2.009,6.418l-5.427-3.813         l-5.426,3.813L66.456,50.156z"></path><path fill="#FFFFFF" d="M159.25,77.959l-5.427-3.813l-5.426,3.813l2.008-6.419l-5.46-4.11h6.674l2.204-6.323l2.203,6.323h6.674         l-5.46,4.11L159.25,77.959z"></path><path fill="#FFFFFF" d="M135.555,77.959l-5.427-3.813l-5.427,3.813l2.01-6.419l-5.462-4.11h6.675l2.204-6.323l2.204,6.323h6.674         l-5.462,4.11L135.555,77.959z"></path><path fill="#FFFFFF" d="M159.25,56.575l-5.427-3.813l-5.426,3.813l2.008-6.419l-5.46-4.11h6.674l2.204-6.323l2.203,6.323h6.674         l-5.46,4.11L159.25,56.575z"></path><path fill="#FFFFFF" d="M135.555,56.575l-5.427-3.813l-5.427,3.813l2.01-6.419l-5.462-4.11h6.675l2.204-6.323l2.204,6.323h6.674         l-5.462,4.11L135.555,56.575z"></path><path fill="#FFFFFF" d="M115.044,33.001c0,0-2.829,1.474-6.776,5.356c0.891,0.503,1.893,1.208,2.467,1.684     c-0.596,0.529-1.206,1.099-1.772,1.636c-2.568-2.117-5.243-3.192-7.315-3.698c0.688-0.493,1.418-0.978,2.14-1.448     c1.114,0.284,2.19,0.722,2.19,0.722c4.459-3.394,8.315-4.858,8.315-4.858s-0.446-0.219-1.639-0.204     c-17.528,0.227-33.678,13.314-30.629,35.706c0.326,2.375,1.249,6.694,2.422,8.588c0.391-3.727,1.79-11.973,5.946-20.808     c-1.444-0.518-2.643-0.731-2.643-0.731c0.424-0.89,1.014-2.025,1.014-2.025c3.075,0.58,5.792,1.866,8.145,3.73     c-0.565,0.912-0.947,1.508-1.216,1.977c-0.825-0.649-1.658-1.21-2.577-1.719c-4.67,8.165-6.838,15.871-7.717,19.916     c0.048,0.249,0.557,0.378,0.966,0.325c19.207-2.486,28.199-12.283,30.935-22.9c1.8-6.965,1.211-14.248-1.199-20.187     C115.932,33.645,115.603,32.952,115.044,33.001z M98.529,54.153c-2.498-2.082-5.412-3.358-8.271-3.903c0,0,0.877-1.431,1.376-2.174     c3.497,0.692,6.336,2.327,8.321,4.007C99.445,52.812,98.961,53.51,98.529,54.153z M101.683,49.725     c-2.316-1.945-5.134-3.396-8.327-4.034c0.488-0.63,1.061-1.301,1.608-1.921c2.437,0.433,5.562,1.717,8.286,3.965     C102.692,48.401,102.173,49.08,101.683,49.725z M105.194,45.5c-2.431-2.001-5.156-3.297-8.052-3.914     c0.701-0.663,1.419-1.231,2.05-1.757c3.618,0.87,6.054,2.491,7.713,3.869C106.312,44.305,105.738,44.897,105.194,45.5z"></path><g>
  <path fill="#FFFFFF" d="M167.963,22.75c-6.259,4.49-18.23,10.61-33.632,6.815C107.64,22.99,100,0,100,0     s-7.639,22.994-34.331,29.565C50.26,33.36,38.291,27.238,32.038,22.75h-4.819v123.974c0.002,3.137,0.766,9.309,5.852,15.488     c6.281,7.63,16.935,12.578,31.658,14.705c11.729,1.697,20.4,5.384,26.516,11.269C95.737,192.511,100,200,100,200     s4.426-7.646,8.756-11.813c6.115-5.885,14.791-9.571,26.516-11.269c14.723-2.127,25.377-7.075,31.659-14.705     c5.082-6.175,5.847-12.354,5.851-15.488V22.75H167.963z"></path>
  <path fill="#013369" d="M168.414,27.584c0,0-14.59,11.355-35.143,6.22C109.309,27.815,100,9.868,100,9.868     s-9.308,17.947-33.271,23.936c-20.552,5.136-35.142-6.22-35.142-6.22v119.134c0.003,4.049,1.695,21.243,33.768,25.878     c12.663,1.833,22.122,5.903,28.918,12.443c2.448,2.348,4.308,4.739,5.728,6.933c1.424-2.193,3.276-4.585,5.721-6.933     c6.804-6.54,16.263-10.61,28.926-12.443c32.07-4.635,33.762-21.829,33.768-25.878V27.584z"></path>
  <path fill="#FFFFFF" enable-background="new    " d="M100,179.844c0,0,9.308-11.473,33.495-15.204     c27.705-4.275,26.882-17.284,26.884-17.943V84.304H39.622v62.392c0,0.656-0.818,13.667,26.883,17.943     C90.691,168.372,100,179.844,100,179.844z"></path>
  <g>
    <path fill="#D50A0A" d="M71.852,129.904l-0.004-29.673l-3.726-2.087V90.45H85.08v7.694l-3.546,2.087v60.527l-9.064-2.717       l-13.104-38.663l-0.002,25.489l4.161,3.196v7.855l-18.193-4.624v-6.696l4.163-1.628v-42.714l-4.265-2.112V90.45h13.768       L71.852,129.904z"></path>
    <path fill="#D50A0A" d="M103.066,157.041l3.632,4.351v7.693l-18.021-6.733v-6.531l3.978-1.629v-53.96l-3.954-2V90.45h31.291       v15.659h-6.878l-2.084-6.429h-7.964v18.106h5.977l2.352-3.438h4.796v16.112h-4.796l-2.264-3.258h-6.064V157.041z"></path>
    <path fill="#D50A0A" d="M122.96,159.765v-7.694l4.476-3.674v-48.166l-3.822-2V90.45h18.044v7.782l-3.546,2v45.188l7.754-2.095       l1.487-11.843h7.418v19.785L122.96,159.765z"></path>
  </g>
  <g>
    <g>
      <path fill="#FFFFFF" d="M42.761,71.54L37.3,67.431h6.674l2.204-6.323l2.204,6.323h6.674l-5.461,4.11l2.009,6.418l-5.426-3.812         l-5.426,3.812L42.761,71.54z"></path>
    </g>
    <g>
      <path fill="#FFFFFF" d="M66.456,71.54l-5.461-4.109h6.674l2.203-6.323l2.204,6.323h6.675l-5.461,4.11l2.009,6.418l-5.427-3.812         l-5.426,3.812L66.456,71.54z"></path>
    </g>
    <g>
      <path fill="#FFFFFF" d="M42.761,50.156L37.3,46.047h6.674l2.204-6.324l2.204,6.324h6.674l-5.461,4.109l2.009,6.418l-5.426-3.813         l-5.426,3.813L42.761,50.156z"></path>
    </g>
    <g>
      <path fill="#FFFFFF" d="M66.456,50.156l-5.461-4.109h6.674l2.203-6.324l2.204,6.324h6.675l-5.461,4.109l2.009,6.418l-5.427-3.813         l-5.426,3.813L66.456,50.156z"></path>
    </g>
  </g>
  <g>
    <g>
      <path fill="#FFFFFF" d="M159.25,77.959l-5.427-3.813l-5.426,3.813l2.008-6.419l-5.46-4.11h6.674l2.204-6.323l2.203,6.323h6.674         l-5.46,4.11L159.25,77.959z"></path>
    </g>
    <g>
      <path fill="#FFFFFF" d="M135.555,77.959l-5.427-3.813l-5.427,3.813l2.01-6.419l-5.462-4.11h6.675l2.204-6.323l2.204,6.323h6.674         l-5.462,4.11L135.555,77.959z"></path>
    </g>
    <g>
      <path fill="#FFFFFF" d="M159.25,56.575l-5.427-3.813l-5.426,3.813l2.008-6.419l-5.46-4.11h6.674l2.204-6.323l2.203,6.323h6.674         l-5.46,4.11L159.25,56.575z"></path>
    </g>
    <g>
      <path fill="#FFFFFF" d="M135.555,56.575l-5.427-3.813l-5.427,3.813l2.01-6.419l-5.462-4.11h6.675l2.204-6.323l2.204,6.323h6.674         l-5.462,4.11L135.555,56.575z"></path>
    </g>
  </g>
  <path fill="#FFFFFF" d="M115.044,33.001c0,0-2.829,1.474-6.776,5.356c0.891,0.503,1.893,1.208,2.467,1.684     c-0.596,0.529-1.206,1.099-1.772,1.636c-2.568-2.117-5.243-3.192-7.315-3.698c0.688-0.493,1.418-0.978,2.14-1.448     c1.114,0.284,2.19,0.722,2.19,0.722c4.459-3.394,8.315-4.858,8.315-4.858s-0.446-0.219-1.639-0.204     c-17.528,0.227-33.678,13.314-30.629,35.706c0.326,2.375,1.249,6.694,2.422,8.588c0.391-3.727,1.79-11.973,5.946-20.808     c-1.444-0.518-2.643-0.731-2.643-0.731c0.424-0.89,1.014-2.025,1.014-2.025c3.075,0.58,5.792,1.866,8.145,3.73     c-0.565,0.912-0.947,1.508-1.216,1.977c-0.825-0.649-1.658-1.21-2.577-1.719c-4.67,8.165-6.838,15.871-7.717,19.916     c0.048,0.249,0.557,0.378,0.966,0.325c19.207-2.486,28.199-12.283,30.935-22.9c1.8-6.965,1.211-14.248-1.199-20.187     C115.932,33.645,115.603,32.952,115.044,33.001z M98.529,54.153c-2.498-2.082-5.412-3.358-8.271-3.903c0,0,0.877-1.431,1.376-2.174     c3.497,0.692,6.336,2.327,8.321,4.007C99.445,52.812,98.961,53.51,98.529,54.153z M101.683,49.725     c-2.316-1.945-5.134-3.396-8.327-4.034c0.488-0.63,1.061-1.301,1.608-1.921c2.437,0.433,5.562,1.717,8.286,3.965     C102.692,48.401,102.173,49.08,101.683,49.725z M105.194,45.5c-2.431-2.001-5.156-3.297-8.052-3.914     c0.701-0.663,1.419-1.231,2.05-1.757c3.618,0.87,6.054,2.491,7.713,3.869C106.312,44.305,105.738,44.897,105.194,45.5z"></path>
</g><g>
  <path fill="#D50A0A" d="M71.852,129.904l-0.004-29.673l-3.726-2.087V90.45H85.08v7.694l-3.546,2.087v60.527l-9.064-2.717       l-13.104-38.663l-0.002,25.489l4.161,3.196v7.855l-18.193-4.624v-6.696l4.163-1.628v-42.714l-4.265-2.112V90.45h13.768       L71.852,129.904z"></path>
  <path fill="#D50A0A" d="M103.066,157.041l3.632,4.351v7.693l-18.021-6.733v-6.531l3.978-1.629v-53.96l-3.954-2V90.45h31.291       v15.659h-6.878l-2.084-6.429h-7.964v18.106h5.977l2.352-3.438h4.796v16.112h-4.796l-2.264-3.258h-6.064V157.041z"></path>
  <path fill="#D50A0A" d="M122.96,159.765v-7.694l4.476-3.674v-48.166l-3.822-2V90.45h18.044v7.782l-3.546,2v45.188l7.754-2.095       l1.487-11.843h7.418v19.785L122.96,159.765z"></path>
</g><g>
  <g>
    <path fill="#FFFFFF" d="M42.761,71.54L37.3,67.431h6.674l2.204-6.323l2.204,6.323h6.674l-5.461,4.11l2.009,6.418l-5.426-3.812         l-5.426,3.812L42.761,71.54z"></path>
  </g>
  <g>
    <path fill="#FFFFFF" d="M66.456,71.54l-5.461-4.109h6.674l2.203-6.323l2.204,6.323h6.675l-5.461,4.11l2.009,6.418l-5.427-3.812         l-5.426,3.812L66.456,71.54z"></path>
  </g>
  <g>
    <path fill="#FFFFFF" d="M42.761,50.156L37.3,46.047h6.674l2.204-6.324l2.204,6.324h6.674l-5.461,4.109l2.009,6.418l-5.426-3.813         l-5.426,3.813L42.761,50.156z"></path>
  </g>
  <g>
    <path fill="#FFFFFF" d="M66.456,50.156l-5.461-4.109h6.674l2.203-6.324l2.204,6.324h6.675l-5.461,4.109l2.009,6.418l-5.427-3.813         l-5.426,3.813L66.456,50.156z"></path>
  </g>
</g><g>
  <path fill="#FFFFFF" d="M42.761,71.54L37.3,67.431h6.674l2.204-6.323l2.204,6.323h6.674l-5.461,4.11l2.009,6.418l-5.426-3.812         l-5.426,3.812L42.761,71.54z"></path>
</g><g>
  <path fill="#FFFFFF" d="M66.456,71.54l-5.461-4.109h6.674l2.203-6.323l2.204,6.323h6.675l-5.461,4.11l2.009,6.418l-5.427-3.812         l-5.426,3.812L66.456,71.54z"></path>
</g><g>
  <path fill="#FFFFFF" d="M42.761,50.156L37.3,46.047h6.674l2.204-6.324l2.204,6.324h6.674l-5.461,4.109l2.009,6.418l-5.426-3.813         l-5.426,3.813L42.761,50.156z"></path>
</g><g>
  <path fill="#FFFFFF" d="M66.456,50.156l-5.461-4.109h6.674l2.203-6.324l2.204,6.324h6.675l-5.461,4.109l2.009,6.418l-5.427-3.813         l-5.426,3.813L66.456,50.156z"></path>
</g><g>
  <g>
    <path fill="#FFFFFF" d="M159.25,77.959l-5.427-3.813l-5.426,3.813l2.008-6.419l-5.46-4.11h6.674l2.204-6.323l2.203,6.323h6.674         l-5.46,4.11L159.25,77.959z"></path>
  </g>
  <g>
    <path fill="#FFFFFF" d="M135.555,77.959l-5.427-3.813l-5.427,3.813l2.01-6.419l-5.462-4.11h6.675l2.204-6.323l2.204,6.323h6.674         l-5.462,4.11L135.555,77.959z"></path>
  </g>
  <g>
    <path fill="#FFFFFF" d="M159.25,56.575l-5.427-3.813l-5.426,3.813l2.008-6.419l-5.46-4.11h6.674l2.204-6.323l2.203,6.323h6.674         l-5.46,4.11L159.25,56.575z"></path>
  </g>
  <g>
    <path fill="#FFFFFF" d="M135.555,56.575l-5.427-3.813l-5.427,3.813l2.01-6.419l-5.462-4.11h6.675l2.204-6.323l2.204,6.323h6.674         l-5.462,4.11L135.555,56.575z"></path>
  </g>
</g><g>
  <path fill="#FFFFFF" d="M159.25,77.959l-5.427-3.813l-5.426,3.813l2.008-6.419l-5.46-4.11h6.674l2.204-6.323l2.203,6.323h6.674         l-5.46,4.11L159.25,77.959z"></path>
</g><g>
  <path fill="#FFFFFF" d="M135.555,77.959l-5.427-3.813l-5.427,3.813l2.01-6.419l-5.462-4.11h6.675l2.204-6.323l2.204,6.323h6.674         l-5.462,4.11L135.555,77.959z"></path>
</g><g>
  <path fill="#FFFFFF" d="M159.25,56.575l-5.427-3.813l-5.426,3.813l2.008-6.419l-5.46-4.11h6.674l2.204-6.323l2.203,6.323h6.674         l-5.46,4.11L159.25,56.575z"></path>
</g><g>
  <path fill="#FFFFFF" d="M135.555,56.575l-5.427-3.813l-5.427,3.813l2.01-6.419l-5.462-4.11h6.675l2.204-6.323l2.204,6.323h6.674         l-5.462,4.11L135.555,56.575z"></path>
</g></symbol><symbol id="carousel"><desc id="carouselDesc">Carousel Icon</desc><polygon points="10 4 10 6 4 6 4 10 2 10 2 4 10 4"></polygon><polygon points="2 20 10 20 10 18 4 18 4 14 2 14 2 20"></polygon><polygon points="22 4 14 4 14 6 20 6 20 10 22 10 22 4"></polygon><polygon points="14 18 14 20 22 20 22 14 20 14 20 18 14 18"></polygon></symbol><symbol id="list"><desc id="listDesc">List View</desc><rect x="4" y="4" width="16" height="7"></rect><rect x="4" y="13" width="16" height="7"></rect></symbol><symbol id="website"><desc id="websiteDesc">Website</desc><path fill-rule="evenodd" d="M8.65,16.3A16.87,16.87,0,0,1,11.6,16v4C10.38,19.7,9.3,18.29,8.65,16.3Zm1,3.34a8.26,8.26,0,0,1-1.76-3.17,8.51,8.51,0,0,0-1.94.73A8,8,0,0,0,9.62,19.64Zm4.76,0a8,8,0,0,0,3.7-2.44,8.51,8.51,0,0,0-1.94-.73A8.26,8.26,0,0,1,14.38,19.64ZM11.6,15.19V12.38H8a14.8,14.8,0,0,0,.42,3.15A18.23,18.23,0,0,1,11.6,15.19Zm0-3.61V8.76a18.27,18.27,0,0,1-3.18-.33A14.71,14.71,0,0,0,8,11.58Zm-2-7.26a8,8,0,0,0-3.7,2.44,9,9,0,0,0,1.94.73A8.26,8.26,0,0,1,9.62,4.32Zm2-.32c-1.22.26-2.3,1.67-2.95,3.66A16.87,16.87,0,0,0,11.6,8Zm.8,16c1.22-.26,2.3-1.67,2.95-3.66A16.87,16.87,0,0,0,12.4,16Zm0-11.2v2.82H16a14.71,14.71,0,0,0-.42-3.15A18.27,18.27,0,0,1,12.4,8.76ZM7.64,15.7a15,15,0,0,1-.44-3.32H4a8,8,0,0,0,1.42,4.18A9.44,9.44,0,0,1,7.64,15.7Zm4.76-3.32v2.81a18.23,18.23,0,0,1,3.18.34A14.8,14.8,0,0,0,16,12.38Zm-5.2-.8a15.06,15.06,0,0,1,.44-3.33A9,9,0,0,1,5.42,7.4,8,8,0,0,0,4,11.58Zm9.6.8a15,15,0,0,1-.44,3.32,9.44,9.44,0,0,1,2.22.86A8,8,0,0,0,20,12.38Zm-.44-4.13a15.06,15.06,0,0,1,.44,3.33H20A8,8,0,0,0,18.58,7.4,9,9,0,0,1,16.36,8.25Zm-2-3.93a8.26,8.26,0,0,1,1.76,3.17,9,9,0,0,0,1.94-.73A8,8,0,0,0,14.38,4.32ZM12.4,4V8a16.87,16.87,0,0,0,2.95-.3C14.7,5.67,13.62,4.26,12.4,4Z"></path><g id="Bell">
  <rect id="Bounds" fill="none" width="24" height="24"></rect>
</g><rect id="Bounds" fill="none" width="24" height="24"></rect><rect fill="none" width="24" height="24"></rect><rect fill="none" width="24" height="24"></rect></symbol><symbol id="nfl-instagram"><desc id="nfl-instagramDesc">Instagram</desc><path d="M12,7.77a4.24,4.24,0,0,1,3.46,1.79H19V7.19A2.19,2.19,0,0,0,16.84,5H7.19A2.19,2.19,0,0,0,5,7.19V9.57H8.55A4.24,4.24,0,0,1,12,7.77ZM16,6.53a.39.39,0,0,1,.39-.39h1.17a.39.39,0,0,1,.39.39V7.71a.39.39,0,0,1-.39.39H16.34A.39.39,0,0,1,16,7.71Z"></path><path d="M16.26,12a4.24,4.24,0,1,1-8.4-.82H5v5.64A2.19,2.19,0,0,0,7.19,19h9.65A2.19,2.19,0,0,0,19,16.84V11.2H16.18A4.35,4.35,0,0,1,16.26,12Z"></path><rect fill="none" width="24" height="24"></rect><circle cx="12.02" cy="12.01" r="2.61"></circle></symbol><symbol id="nfl-twitter"><desc id="nfl-twitterDesc">Twitter</desc><path fill-rule="evenodd" d="M18.92,7.71a5.66,5.66,0,0,1-1.63.45,2.84,2.84,0,0,0,1.25-1.57,5.68,5.68,0,0,1-1.8.69,2.84,2.84,0,0,0-4.91,1.94,2.87,2.87,0,0,0,.07.65A8.06,8.06,0,0,1,6,6.89a2.84,2.84,0,0,0,.88,3.79,2.83,2.83,0,0,1-1.29-.36v0a2.84,2.84,0,0,0,2.28,2.78,2.82,2.82,0,0,1-1.28,0,2.84,2.84,0,0,0,2.65,2,5.7,5.7,0,0,1-3.53,1.22,5.84,5.84,0,0,1-.68,0A8.08,8.08,0,0,0,17.51,9.54c0-.12,0-.25,0-.37a5.78,5.78,0,0,0,1.42-1.47"></path><rect fill="none" width="24" height="24"></rect><rect fill="none" width="24" height="24"></rect></symbol><symbol id="nfl-facebook"><desc id="nfl-facebookDesc">Facebook</desc><path fill-rule="evenodd" d="M14.86,12l.32-2.33H12.72V8.54c0-.72.2-1.21,1.23-1.21h1.32V5.1A17.59,17.59,0,0,0,13.35,5a3,3,0,0,0-3.2,3.29V9.67H8V12h2.15v7h2.57V12h2.14"></path><rect fill="none" width="24" height="24"></rect></symbol><symbol id="nfl-snapchat"><desc id="nfl-snapchatDesc">Snapchat</desc><path d="M12.16,4.38h-.32A4.31,4.31,0,0,0,7.83,7a8.11,8.11,0,0,0-.2,3.28l0,.39a.65.65,0,0,1-.32.07,1.81,1.81,0,0,1-.75-.2.6.6,0,0,0-.26-.05.8.8,0,0,0-.79.54c0,.23.06.57.81.87l.24.08c.31.1.78.25.91.55a.72.72,0,0,1-.08.59v0a4.77,4.77,0,0,1-3.24,2.72.34.34,0,0,0-.28.35.45.45,0,0,0,0,.15c.17.39.86.67,2.13.87a1.58,1.58,0,0,1,.11.38c0,.12.05.25.09.38a.38.38,0,0,0,.39.29A2,2,0,0,0,7,18.2a4.28,4.28,0,0,1,.86-.1,3.81,3.81,0,0,1,.62.05,2.94,2.94,0,0,1,1.16.6,3.66,3.66,0,0,0,2.23.87h.2a3.66,3.66,0,0,0,2.23-.87,2.94,2.94,0,0,1,1.16-.6,3.8,3.8,0,0,1,.62-.05,4.31,4.31,0,0,1,.86.09,2,2,0,0,0,.37,0h0a.37.37,0,0,0,.37-.28c0-.13.07-.25.09-.38a1.57,1.57,0,0,1,.11-.38c1.27-.2,2-.48,2.13-.87a.45.45,0,0,0,0-.15.34.34,0,0,0-.28-.35,4.77,4.77,0,0,1-3.24-2.72v0a.72.72,0,0,1-.08-.59c.13-.3.6-.45.91-.55l.24-.08c.55-.22.83-.48.82-.79a.66.66,0,0,0-.49-.57h0a.9.9,0,0,0-.34-.06.75.75,0,0,0-.31.06,1.89,1.89,0,0,1-.7.2.62.62,0,0,1-.27-.07l0-.35v0A8.11,8.11,0,0,0,16.17,7a4.32,4.32,0,0,0-4-2.59Z"></path><rect fill="none" width="24" height="24"></rect></symbol><symbol id="shop"><desc id="shopDesc">Shop Icon</desc><path d="M12,6,9,4H6v6H7V20H17V10h1V4H15Zm0,1a.75.75,0,1,1-.75.75A.75.75,0,0,1,12,7Zm2,3.7a.35.35,0,0,1,0,.11l-2,5.11a.12.12,0,0,1-.13.08H10.66c-.06,0-.08,0-.06-.1l2-5.1v0H11.28a0,0,0,0,0-.06.06v.67a.09.09,0,0,1-.09.09h-1a.09.09,0,0,1-.09-.09V9.72a.09.09,0,0,1,.09-.09h3.81a.09.09,0,0,1,.09.09Z"></path><rect x="3" y="4" width="2" height="6"></rect><rect x="19" y="4" width="2" height="6"></rect></symbol><symbol id="profile-header__overlay"><desc id="profile-header__overlayDesc">Profile Overlay</desc><g>
  <polygon class="profile-header__overlay-stripe" points="109.818 0 16 225 0 225 93.818 0"></polygon>
  <polygon class="profile-header__overlay-polygon" points="16 225 109.818 0 800 0 800 225"></polygon>
</g><polygon class="profile-header__overlay-stripe" points="109.818 0 16 225 0 225 93.818 0"></polygon><polygon class="profile-header__overlay-polygon" points="16 225 109.818 0 800 0 800 225"></polygon></symbol><symbol id="nfl-avatar"><desc id="nfl-avatarDesc">Avatar</desc><path d="M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm5.22,16-.88-2.19a2,2,0,0,0-1.22-1.15l-1.17-.39a6.16,6.16,0,0,0-3.9,0l-1.17.39a2,2,0,0,0-1.22,1.15L6.78,18a8,8,0,1,1,10.44,0Z"></path><circle cx="12" cy="10" r="3"></circle></symbol><symbol id="nfl-add"><desc id="nfl-addDesc">Add</desc><polygon points="19 11 13 11 13 5 11 5 11 11 5 11 5 13 11 13 11 19 13 19 13 13 19 13 19 11"></polygon></symbol><symbol id="nfl-airplay"><desc id="nfl-airplayDesc">Airplay</desc><polygon points="12 13 4 21 20 21 12 13"></polygon><polygon points="2 4 22 4 22 18 19 18 17 16 20 16 20 6 4 6 4 16 7 16 5 18 2 18 2 4"></polygon></symbol><symbol id="nfl-arrow-left"><desc id="nfl-arrow-leftDesc">Arrow Left</desc><polygon points="19 11 7.83 11 11.12 7.71 9.71 6.29 4 12 9.71 17.71 11.12 16.29 7.83 13 19 13 19 11"></polygon></symbol><symbol id="nfl-arrow-right"><desc id="nfl-arrow-rightDesc">Arrow Right</desc><polygon points="14.29 6.29 12.88 7.71 16.17 11 5 11 5 13 16.17 13 12.88 16.29 14.29 17.71 20 12 14.29 6.29"></polygon></symbol><symbol id="nfl-arrow-up"><desc id="nfl-arrow-upDesc">Arrow Up</desc><polygon points="17.71 9.71 12 4 6.29 9.71 7.71 11.12 11 7.83 11 19 13 19 13 7.83 16.29 11.12 17.71 9.71"></polygon></symbol><symbol id="nfl-arrow-down"><desc id="nfl-arrow-downDesc">Arrow Down</desc><polygon points="16.29 12.88 13 16.17 13 5 11 5 11 16.17 7.71 12.88 6.29 14.29 12 20 17.71 14.29 16.29 12.88"></polygon></symbol><symbol id="nfl-audio"><desc id="nfl-audioDesc">Audio</desc><path d="M20,12A8,8,0,0,0,4,12a7.88,7.88,0,0,0,.83,3.53l-.24,0,.67,3.95,1.09-.19.16,1,2.92-.49-1-5.92-2,.34A5.84,5.84,0,0,1,6,12a6,6,0,0,1,12,0,5.84,5.84,0,0,1-.43,2.21l-2-.34-1,5.92,2.92.49.16-1,1.09.19.67-3.95-.24,0A7.88,7.88,0,0,0,20,12Z"></path></symbol><symbol id="nfl-back-5s"><desc id="nfl-back-5sDesc">Back 5s</desc><path d="M12.82,12.71c0-.49-.23-.62-.77-.62a1,1,0,0,0-.88.37l-1-.22V9.43h3.49v1.09H11.26v.87a1.69,1.69,0,0,1,1.15-.32,1.46,1.46,0,0,1,1.65,1.64v.22c0,1.18-.78,1.64-2.19,1.64a3.82,3.82,0,0,1-2-.48l.41-.92a3.07,3.07,0,0,0,1.49.4c.65,0,1-.11,1-.62Z"></path><path d="M12,2A9.93,9.93,0,0,0,4.94,4.94L3,3V8H8L6.36,6.36a8,8,0,1,1,0,11.3L4.93,19.07A10,10,0,1,0,12,2Z"></path></symbol><symbol id="nfl-back-10s"><desc id="nfl-back-10sDesc">Back 10s</desc><path d="M10.47,14.56H9.2V10.43l-.82.06v-.8l1.23-.32h.85Z"></path><path d="M15.49,11.44v1.11c0,1.49-.54,2.07-2,2.07s-2-.58-2-2.07V11.44c0-1.49.54-2.07,2-2.07S15.49,10,15.49,11.44Zm-2.74.16v.8c0,.84,0,1.18.73,1.18s.73-.34.73-1.18v-.8c0-.84,0-1.18-.73-1.18S12.75,10.76,12.75,11.6Z"></path><path d="M12,2A9.93,9.93,0,0,0,4.94,4.94L3,3V8H8L6.36,6.36a8,8,0,1,1,0,11.3L4.93,19.07A10,10,0,1,0,12,2Z"></path></symbol><symbol id="nfl-back-30s"><desc id="nfl-back-30sDesc">Back 30s</desc><path d="M7.53,14.14l.41-.93a3.19,3.19,0,0,0,1.53.4c.69,0,1-.16,1-.58h0c0-.38-.17-.59-.76-.59H8.54v-1h1c.49,0,.65-.21.65-.52h0c0-.27-.12-.5-.7-.5a3.05,3.05,0,0,0-1.44.37l-.41-.93a3.79,3.79,0,0,1,2-.46c1.42,0,1.81.6,1.81,1.51v0a1.11,1.11,0,0,1-.64,1,1.13,1.13,0,0,1,.86,1.18h0c0,1-.73,1.54-2.19,1.54A3.84,3.84,0,0,1,7.53,14.14Z"></path><path d="M16.29,11.44v1.11c0,1.49-.54,2.07-2,2.07s-2-.58-2-2.07V11.44c0-1.49.54-2.07,2-2.07S16.29,10,16.29,11.44Zm-2.74.16v.8c0,.84,0,1.18.73,1.18s.73-.34.73-1.18v-.8c0-.84,0-1.18-.73-1.18S13.55,10.76,13.55,11.6Z"></path><path d="M12,2A9.93,9.93,0,0,0,4.94,4.94L3,3V8H8L6.36,6.36a8,8,0,1,1,0,11.3L4.93,19.07A10,10,0,1,0,12,2Z"></path></symbol><symbol id="nfl-calendar"><desc id="nfl-calendarDesc">Calendar</desc><path d="M18,5V3H16V5H8V3H6V5H4V19H20V5Zm1,13H5V8H19Z"></path><rect x="7" y="10" width="2" height="2"></rect><rect x="11" y="10" width="2" height="2"></rect><rect x="15" y="10" width="2" height="2"></rect><rect x="7" y="14" width="2" height="2"></rect><rect x="11" y="14" width="2" height="2"></rect><rect x="15" y="14" width="2" height="2"></rect></symbol><symbol id="nfl-chart"><desc id="nfl-chartDesc">Chart</desc><rect x="6" y="12" width="2" height="8"></rect><rect x="16" y="8" width="2" height="12"></rect><rect x="11" y="4" width="2" height="16"></rect></symbol><symbol id="nfl-check"><desc id="nfl-checkDesc">Check</desc><path d="M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2ZM10,16.414,6.293,12.707l1.414-1.414L10,13.586l6.293-6.293,1.414,1.414Z"></path></symbol><symbol id="nfl-chevron-down"><desc id="nfl-chevron-downDesc">Down</desc><path d="m12 17-6.71-6.71 1.42-1.41 5.29 5.29 5.29-5.29 1.42 1.41z"></path><path d="m0 0h24v24h-24z" fill="none"></path></symbol><symbol id="nfl-chevron-left"><desc id="nfl-chevron-leftDesc">Left</desc><polygon points="13.71 18.71 7 12 13.71 5.29 15.12 6.71 9.83 12 15.12 17.29 13.71 18.71"></polygon></symbol><symbol id="nfl-chevron-right"><desc id="nfl-chevron-rightDesc">Right</desc><polygon points="10.29 18.71 8.88 17.29 14.17 12 8.88 6.71 10.29 5.29 17 12 10.29 18.71"></polygon></symbol><symbol id="nfl-chevron-up"><desc id="nfl-chevron-upDesc">Up</desc><polygon points="17.29 15.12 12 9.83 6.71 15.12 5.29 13.71 12 7 18.71 13.71 17.29 15.12"></polygon></symbol><symbol id="nfl-chromecast-off"><desc id="nfl-chromecast-offDesc">Chromecast Off</desc><path d="M6,21H8a6,6,0,0,0-6-6v2A4,4,0,0,1,6,21Z"></path><path d="M10,21h2A10,10,0,0,0,2,11v2A8,8,0,0,1,10,21Z"></path><path d="M3,3V9.05a11.9,11.9,0,0,1,2,.34V5H20V18H13.61a11.9,11.9,0,0,1,.34,2H22V3Z"></path><path d="M4,21a2,2,0,0,0-2-2v2Z"></path></symbol><symbol id="nfl-chromecast-on"><desc id="nfl-chromecast-onDesc">Chromecast On</desc><path d="M6,21H8a6,6,0,0,0-6-6v2A4,4,0,0,1,6,21Z"></path><path d="M10,21h2A10,10,0,0,0,2,11v2A8,8,0,0,1,10,21Z"></path><path d="M3,3V9.05a11.9,11.9,0,0,1,2,.34V5H20V18H13.61a11.9,11.9,0,0,1,.34,2H22V3Z"></path><path d="M7,7v3.11A12.06,12.06,0,0,1,12.89,16H18V7Z"></path><path d="M4,21a2,2,0,0,0-2-2v2Z"></path></symbol><symbol id="nfl-close"><desc id="nfl-closeDesc">Close</desc><polygon points="13.41 12 18.21 7.21 16.79 5.79 12 10.59 7.21 5.79 5.79 7.21 10.59 12 5.79 16.79 7.21 18.21 12 13.41 16.79 18.21 18.21 16.79 13.41 12"></polygon></symbol><symbol id="nfl-closed-captions"><desc id="nfl-closed-captionsDesc">Closed Captions</desc><path d="M10,14.35a3.08,3.08,0,0,0,1.85-.53l-.46-1a2.5,2.5,0,0,1-1.41.41c-.87,0-1-.44-1-1V10.84c0-.56.18-1,1-1a2.5,2.5,0,0,1,1.41.41l.46-1A3.08,3.08,0,0,0,10,8.69c-1.77,0-2.35.78-2.35,2v1.57C7.6,13.57,8.18,14.35,10,14.35Z"></path><path d="M14.55,14.35a3.08,3.08,0,0,0,1.85-.53l-.46-1a2.5,2.5,0,0,1-1.41.41c-.87,0-1-.44-1-1V10.84c0-.56.18-1,1-1a2.5,2.5,0,0,1,1.41.41l.46-1a3.08,3.08,0,0,0-1.85-.53c-1.77,0-2.35.78-2.35,2v1.57C12.2,13.57,12.78,14.35,14.55,14.35Z"></path><path d="M3,5V18H15v3l3-3h3V5ZM19,16H5V7H19Z"></path></symbol><symbol id="nfl-bench-off"><desc id="nfl-bench-offDesc">Bench Off</desc><path d="M14,20.25a1.2,1.2,0,0,1-1.25-1.139V4.889a1.255,1.255,0,0,1,2.5,0V10.75h7v2.5h-7v5.861A1.2,1.2,0,0,1,14,20.25Zm0-16a.7.7,0,0,0-.75.639V19.111a.76.76,0,0,0,1.5,0V4.889A.7.7,0,0,0,14,4.25Zm1.25,8.5h6.5v-1.5h-6.5ZM10,20.25a1.2,1.2,0,0,1-1.25-1.139V4.889A1.2,1.2,0,0,1,10,3.75a1.2,1.2,0,0,1,1.25,1.139V19.111A1.2,1.2,0,0,1,10,20.25Zm0-16a.7.7,0,0,0-.75.639V19.111a.7.7,0,0,0,.75.639.7.7,0,0,0,.75-.639V4.889A.7.7,0,0,0,10,4.25Zm-4,16a1.2,1.2,0,0,1-1.25-1.139V13.25H3a1.25,1.25,0,0,1,0-2.5H4.75V4.889A1.2,1.2,0,0,1,6,3.75,1.2,1.2,0,0,1,7.25,4.889V19.111A1.2,1.2,0,0,1,6,20.25Zm0-16a.7.7,0,0,0-.75.639V19.111A.7.7,0,0,0,6,19.75a.7.7,0,0,0,.75-.639V4.889A.7.7,0,0,0,6,4.25Zm-3,7a.75.75,0,0,0,0,1.5H4.75v-1.5Z"></path></symbol><symbol id="nfl-bench-on"><desc id="nfl-bench-onDesc">Bench On</desc><path d="M7,4.889V19.111A.95.95,0,0,1,6,20a.95.95,0,0,1-1-.889V4.889A.95.95,0,0,1,6,4,.95.95,0,0,1,7,4.889ZM10,4a.95.95,0,0,0-1,.889V19.111A.95.95,0,0,0,10,20a.95.95,0,0,0,1-.889V4.889A.95.95,0,0,0,10,4Zm4,0a.95.95,0,0,0-1,.889V19.111a1.007,1.007,0,0,0,2,0V4.889A.95.95,0,0,0,14,4ZM2,12a1,1,0,0,0,1,1H4V11H3A1,1,0,0,0,2,12Zm14-1v2h6V11Z"></path></symbol><symbol id="nfl-broad-off"><desc id="nfl-broad-offDesc">Broad Off</desc><path d="M21.657,21.25H4.743a1.256,1.256,0,0,1-1.2-.891l-2.4-8a1.248,1.248,0,0,1,1.2-1.608H19.257a1.256,1.256,0,0,1,1.2.891l2.4,8a1.249,1.249,0,0,1-1.2,1.608Zm-19.314-10a.75.75,0,0,0-.718.964l2.4,8a.754.754,0,0,0,.717.534H21.657a.75.75,0,0,0,.718-.964l-2.4-8a.754.754,0,0,0-.717-.534ZM21,20.25a1.629,1.629,0,0,1-1.335-1.1l-2.3-6.127a1.062,1.062,0,0,1,.05-.98A.684.684,0,0,1,18,11.75a1.629,1.629,0,0,1,1.335,1.1l2.3,6.127a1.06,1.06,0,0,1-.05.979A.683.683,0,0,1,21,20.25Zm-3-8a.19.19,0,0,0-.173.079.6.6,0,0,0,.008.52l2.3,6.127A1.159,1.159,0,0,0,21,19.75a.19.19,0,0,0,.173-.079.6.6,0,0,0-.008-.52l-2.3-6.127A1.159,1.159,0,0,0,18,12.25Zm-2,8a1.629,1.629,0,0,1-1.335-1.1l-2.3-6.127a1.062,1.062,0,0,1,.05-.98A.684.684,0,0,1,13,11.75a1.629,1.629,0,0,1,1.335,1.1l2.3,6.127a1.06,1.06,0,0,1-.05.979A.683.683,0,0,1,16,20.25Zm-3-8a.19.19,0,0,0-.173.079.6.6,0,0,0,.008.52l2.3,6.127A1.159,1.159,0,0,0,16,19.75a.19.19,0,0,0,.173-.079.6.6,0,0,0-.008-.52l-2.3-6.127A1.159,1.159,0,0,0,13,12.25Zm-2,8a1.629,1.629,0,0,1-1.335-1.1l-2.3-6.127a1.062,1.062,0,0,1,.05-.98A.684.684,0,0,1,8,11.75a1.629,1.629,0,0,1,1.335,1.1l2.3,6.127a1.06,1.06,0,0,1-.05.979A.683.683,0,0,1,11,20.25Zm-3-8a.19.19,0,0,0-.173.079.6.6,0,0,0,.008.52l2.3,6.127A1.159,1.159,0,0,0,11,19.75a.19.19,0,0,0,.173-.079.6.6,0,0,0-.008-.52l-2.3-6.127A1.159,1.159,0,0,0,8,12.25Zm-2,8a1.629,1.629,0,0,1-1.335-1.1l-2.3-6.127a1.062,1.062,0,0,1,.05-.98A.684.684,0,0,1,3,11.75a1.629,1.629,0,0,1,1.335,1.1l2.3,6.127a1.06,1.06,0,0,1-.05.979A.683.683,0,0,1,6,20.25Zm-3-8a.19.19,0,0,0-.173.079.6.6,0,0,0,.008.52l2.3,6.127A1.159,1.159,0,0,0,6,19.75a.19.19,0,0,0,.173-.079.6.6,0,0,0-.008-.52l-2.3-6.127A1.159,1.159,0,0,0,3,12.25Zm14,4a1.629,1.629,0,0,1-1.335-1.1l-.8-2.127a1.062,1.062,0,0,1,.05-.98.684.684,0,0,1,.583-.294,1.629,1.629,0,0,1,1.335,1.1l.8,2.127a1.06,1.06,0,0,1-.05.979A.683.683,0,0,1,17,16.25Zm-1.5-4a.19.19,0,0,0-.173.079.6.6,0,0,0,.008.52l.8,2.127A1.159,1.159,0,0,0,17,15.75a.19.19,0,0,0,.173-.079.6.6,0,0,0-.008-.52l-.8-2.127A1.159,1.159,0,0,0,15.5,12.25Zm-3.5,4a1.629,1.629,0,0,1-1.335-1.1l-.8-2.127a1.062,1.062,0,0,1,.05-.98.684.684,0,0,1,.583-.294,1.629,1.629,0,0,1,1.335,1.1l.8,2.127a1.06,1.06,0,0,1-.05.979A.683.683,0,0,1,12,16.25Zm-1.5-4a.19.19,0,0,0-.173.079.6.6,0,0,0,.008.52l.8,2.127A1.159,1.159,0,0,0,12,15.75a.19.19,0,0,0,.173-.079.6.6,0,0,0-.008-.52l-.8-2.127A1.159,1.159,0,0,0,10.5,12.25Zm-3.5,4a1.629,1.629,0,0,1-1.335-1.1l-.8-2.127a1.062,1.062,0,0,1,.05-.98A.684.684,0,0,1,5.5,11.75a1.629,1.629,0,0,1,1.335,1.1l.8,2.127a1.06,1.06,0,0,1-.05.979A.683.683,0,0,1,7,16.25Zm-1.5-4a.19.19,0,0,0-.173.079.6.6,0,0,0,.008.52l.8,2.127A1.159,1.159,0,0,0,7,15.75a.19.19,0,0,0,.173-.079.6.6,0,0,0-.008-.52l-.8-2.127A1.159,1.159,0,0,0,5.5,12.25ZM3,10.472a.978.978,0,0,1-.438-.1A1,1,0,0,1,2.1,9.044,12.2,12.2,0,0,1,3.334,7.2a.993.993,0,0,1,.671-.37,1.009,1.009,0,0,1,.735.213,1,1,0,0,1,.158,1.4,10.267,10.267,0,0,0-1,1.473A1,1,0,0,1,3,10.472ZM4.117,7.323a.525.525,0,0,0-.056,0,.5.5,0,0,0-.336.186A11.435,11.435,0,0,0,2.549,9.261a.492.492,0,0,0,.232.66.507.507,0,0,0,.67-.228A10.466,10.466,0,0,1,4.507,8.135a.5.5,0,0,0-.39-.812Zm11.977.654a1.058,1.058,0,0,1-.991-.7.911.911,0,0,1,.641-1.192l.867-.224c-.281-.144-.541-.267-.787-.373A1,1,0,0,1,15.3,4.177a1,1,0,0,1,1.312-.527c.309.132.634.287.983.468l-.307-.894a.912.912,0,0,1,.641-1.193,1.058,1.058,0,0,1,1.239.666l1.1,3.209A.911.911,0,0,1,19.625,7.1l-3.284.846A.952.952,0,0,1,16.094,7.977Zm.123-3.907a.5.5,0,0,0-.46.3.494.494,0,0,0,0,.383.487.487,0,0,0,.268.272c.417.18.874.408,1.395.7l.547.3-2.094.54a.43.43,0,0,0-.271.206.422.422,0,0,0-.023.34.546.546,0,0,0,.641.345L19.5,6.614a.412.412,0,0,0,.294-.546l-1.1-3.208a.551.551,0,0,0-.641-.344.412.412,0,0,0-.294.546l.728,2.118-.571-.32a16.2,16.2,0,0,0-1.5-.75A.52.52,0,0,0,16.217,4.07ZM5.614,7.23A1,1,0,0,1,4.87,6.9a1,1,0,0,1,.076-1.412,11.545,11.545,0,0,1,3.21-2.069,1,1,0,0,1,.777,1.844A9.513,9.513,0,0,0,6.282,6.975,1,1,0,0,1,5.614,7.23ZM8.543,3.839a.508.508,0,0,0-.193.039,11.071,11.071,0,0,0-3.07,1.98.5.5,0,0,0-.038.706.516.516,0,0,0,.706.039A10.016,10.016,0,0,1,8.739,4.8a.5.5,0,0,0-.2-.961Zm5.582,1.047a1.017,1.017,0,0,1-.22-.025A8.452,8.452,0,0,0,10.8,4.729a.966.966,0,0,1-.74-.179,1,1,0,0,1,.438-1.8,10.419,10.419,0,0,1,3.85.157,1,1,0,0,1-.219,1.976Zm-2.146-.744a9.289,9.289,0,0,1,2.036.231.5.5,0,0,0,.22-.975,9.924,9.924,0,0,0-3.666-.151.5.5,0,0,0-.327.2.5.5,0,0,0,.109.7.489.489,0,0,0,.368.089A8.5,8.5,0,0,1,11.979,4.142Z"></path></symbol><symbol id="nfl-broad-on"><desc id="nfl-broad-onDesc">Broad On</desc><path d="M22.614,19.714l-2.4-8A1,1,0,0,0,19.257,11H2.343a1,1,0,0,0-.957,1.286l2.4,8A1,1,0,0,0,4.743,21H21.657A1,1,0,0,0,22.614,19.714ZM6,20a1.4,1.4,0,0,1-1.1-.936L2.6,12.936C2.408,12.421,2.588,12,3,12a1.393,1.393,0,0,1,1.1.936l2.3,6.128C6.592,19.579,6.413,20,6,20Zm1-4a1.4,1.4,0,0,1-1.1-.936l-.8-2.128c-.193-.515-.013-.936.4-.936a1.393,1.393,0,0,1,1.1.936l.8,2.128C7.592,15.579,7.413,16,7,16Zm4,4a1.4,1.4,0,0,1-1.1-.936L7.6,12.936C7.408,12.421,7.588,12,8,12a1.393,1.393,0,0,1,1.1.936l2.3,6.128C11.592,19.579,11.413,20,11,20Zm1-4a1.4,1.4,0,0,1-1.1-.936l-.8-2.128c-.193-.515-.013-.936.4-.936a1.393,1.393,0,0,1,1.1.936l.8,2.128C12.592,15.579,12.413,16,12,16Zm4,4a1.4,1.4,0,0,1-1.1-.936l-2.3-6.128c-.193-.515-.013-.936.4-.936a1.393,1.393,0,0,1,1.1.936l2.3,6.128C16.592,19.579,16.413,20,16,20Zm1-4a1.4,1.4,0,0,1-1.1-.936l-.8-2.128c-.193-.515-.013-.936.4-.936a1.393,1.393,0,0,1,1.1.936l.8,2.128C17.592,15.579,17.413,16,17,16Zm4,4a1.4,1.4,0,0,1-1.1-.936l-2.3-6.128c-.193-.515-.013-.936.4-.936a1.393,1.393,0,0,1,1.1.936l2.3,6.128C21.592,19.579,21.413,20,21,20ZM3,10.222a.748.748,0,0,1-.675-1.07,11.658,11.658,0,0,1,1.206-1.8A.75.75,0,0,1,4.7,8.291,10.4,10.4,0,0,0,3.674,9.806.758.758,0,0,1,3,10.222ZM5.614,6.98a.749.749,0,0,1-.5-1.307,11.3,11.3,0,0,1,3.14-2.026A.75.75,0,0,1,8.836,5.03,9.786,9.786,0,0,0,6.115,6.788.744.744,0,0,1,5.614,6.98Zm8.511-2.344a.727.727,0,0,1-.165-.019,8.686,8.686,0,0,0-3.2-.135A.75.75,0,0,1,10.532,3a10.15,10.15,0,0,1,3.758.154.75.75,0,0,1-.165,1.482ZM18,6.227a.747.747,0,0,1-.379-.1,15.292,15.292,0,0,0-1.7-.863.75.75,0,1,1,.589-1.379,16.611,16.611,0,0,1,1.868.949A.75.75,0,0,1,18,6.227Zm-2.661.97a.661.661,0,0,1,.467-.869l2.581-.665-.866-2.52a.661.661,0,0,1,.468-.869.8.8,0,0,1,.939.506l1.1,3.207a.661.661,0,0,1-.467.869L16.279,7.7A.8.8,0,0,1,15.339,7.2Z"></path></symbol><symbol id="nfl-vertical-off"><desc id="nfl-vertical-offDesc">Vertical Off</desc><path d="M12,10.25H3a1.25,1.25,0,0,1,0-2.5h9a1.25,1.25,0,0,1,0,2.5Zm-9-2a.75.75,0,0,0,0,1.5h9a.75.75,0,0,0,0-1.5Zm7,7H3a1.25,1.25,0,0,1,0-2.5h7a1.25,1.25,0,0,1,0,2.5Zm-7-2a.75.75,0,0,0,0,1.5h7a.75.75,0,0,0,0-1.5Zm5,7H3a1.25,1.25,0,0,1,0-2.5H8a1.25,1.25,0,0,1,0,2.5Zm-5-2a.75.75,0,0,0,0,1.5H8a.75.75,0,0,0,0-1.5Zm14-4a1,1,0,0,1-1-1V9.75a1,1,0,0,1,2,0v3.5A1,1,0,0,1,17,14.25Zm0-5a.5.5,0,0,0-.5.5v3.5a.5.5,0,0,0,1,0V9.75A.5.5,0,0,0,17,9.25Zm0,11a1,1,0,0,1-1-1v-3.5a1,1,0,0,1,2,0v3.5A1,1,0,0,1,17,20.25Zm0-5a.5.5,0,0,0-.5.5v3.5a.5.5,0,0,0,1,0v-3.5A.5.5,0,0,0,17,15.25Zm0-7a1,1,0,0,1-1-1V6.272l-.625.677a.935.935,0,0,1-1.354,0,1.053,1.053,0,0,1,0-1.407l2.3-2.491A.913.913,0,0,1,17,2.75h0a.914.914,0,0,1,.677.3l2.3,2.491a1.053,1.053,0,0,1,0,1.407.935.935,0,0,1-1.354,0L18,6.272V7.25A1,1,0,0,1,17,8.25ZM16.5,5V7.25a.5.5,0,0,0,1,0V5l1.492,1.614a.42.42,0,0,0,.62,0,.55.55,0,0,0,0-.727l-2.3-2.491A.422.422,0,0,0,17,3.25h0a.42.42,0,0,0-.309.141l-2.3,2.491a.55.55,0,0,0,0,.727.42.42,0,0,0,.62,0Zm-3.14.255H3a1.25,1.25,0,0,1,0-2.5H15.646ZM3,3.25a.75.75,0,0,0,0,1.5H13.116L14.5,3.25Zm18,2h-.36l-.119-.161L18.354,2.75H21a1.25,1.25,0,0,1,0,2.5Zm-.116-.5H21a.75.75,0,0,0,0-1.5H19.5Z"></path></symbol><symbol id="nfl-vertical-on"><desc id="nfl-vertical-onDesc">Vertical On</desc><path d="M13,9a1,1,0,0,1-1,1H3A1,1,0,0,1,3,8h9A1,1,0,0,1,13,9Zm-3,4H3a1,1,0,0,0,0,2h7a1,1,0,0,0,0-2ZM8,18H3a1,1,0,0,0,0,2H8a1,1,0,0,0,0-2Zm9-9a.75.75,0,0,0-.75.75v3.5a.75.75,0,0,0,1.5,0V9.75A.75.75,0,0,0,17,9Zm0,6a.75.75,0,0,0-.75.75v3.5a.75.75,0,0,0,1.5,0v-3.5A.75.75,0,0,0,17,15Zm.493-11.779a.66.66,0,0,0-.986,0L14.2,5.712a.8.8,0,0,0,0,1.067.661.661,0,0,0,.987,0L16.25,5.633V7.25a.75.75,0,0,0,1.5,0V5.633l1.059,1.146a.661.661,0,0,0,.987,0,.8.8,0,0,0,0-1.067ZM13.272,4.949,15.074,3H3A1,1,0,0,0,3,5H13.241C13.254,4.984,13.258,4.964,13.272,4.949ZM21,3H18.926l1.8,1.949c.014.015.018.035.031.051H21a1,1,0,0,0,0-2Z"></path></symbol><symbol id="nfl-comment"><desc id="nfl-commentDesc">Comment</desc><path d="M3,4V18H6l3,3V18H21V4ZM5,6H19V16H5Z"></path><rect x="7" y="8" width="10" height="2"></rect><rect x="7" y="12" width="8" height="2"></rect></symbol><symbol id="nfl-dock"><desc id="nfl-dockDesc">Dock</desc><path d="M2,4V20H22V4ZM20,18H4V6H20Z"></path><rect x="11" y="11" width="7" height="5"></rect></symbol><symbol id="nfl-done"><desc id="nfl-doneDesc">Done</desc><polygon points="9 17.414 4.293 12.707 5.707 11.293 9 14.586 17.293 6.293 18.707 7.707 9 17.414"></polygon></symbol><symbol id="nfl-download"><desc id="nfl-downloadDesc">Download</desc><polygon points="13 14 13 4 11 4 11 14 8 14 12 18 16 14 13 14"></polygon><rect x="2" y="20" width="20" height="2"></rect></symbol><symbol id="nfl-draft"><desc id="nfl-draftDesc">Draft</desc><path d="M21,7a26.91,26.91,0,0,0-5.25-1.84V8H21Z"></path><path d="M22,9H15.71a2.3,2.3,0,0,1-.41,1,2.64,2.64,0,0,1-1.81,1,.94.94,0,0,0-.51.2,2.28,2.28,0,0,0-.32.32l-.66,1-.61-1a2.42,2.42,0,0,0-.36-.32,1,1,0,0,0-.52-.21A2.67,2.67,0,0,1,8.7,10a2.3,2.3,0,0,1-.41-1H2v6H22Z"></path><path d="M18,16H6a21.91,21.91,0,0,0,5.65,4.8L12,21l.35-.2C15.42,19,17.2,17.35,18,16Z"></path><path d="M8.25,5.17A31.83,31.83,0,0,0,3,7V8H8.25Z"></path><path d="M10.59,4.18A1.73,1.73,0,0,1,9.2,3.91H9v5a1,1,0,0,0,.24.62,2,2,0,0,0,1.3.58,2,2,0,0,1,1.09.45A2.48,2.48,0,0,1,12,11a2.94,2.94,0,0,1,.36-.47,1.93,1.93,0,0,1,1.09-.45,2,2,0,0,0,1.3-.59A1,1,0,0,0,15,8.86v-5h-.2a1.7,1.7,0,0,1-1.38.27A2,2,0,0,1,12,3,2,2,0,0,1,10.59,4.18Z"></path></symbol><symbol id="nfl-fantasy"><desc id="nfl-fantasyDesc">Fantasy</desc><path d="M16.91,5,17,4H7l.09,1H3a9,9,0,0,0,4.81,8L8,15l3,1.5V19H9L7,21H17l-2-2H13V16.5L16,15l.19-2A9,9,0,0,0,21,5ZM5.29,7h2l.31,3.4A7,7,0,0,1,5.29,7Zm11.12,3.4L16.73,7h2A7,7,0,0,1,16.42,10.4Z"></path></symbol><symbol id="nfl-filter"><desc id="nfl-filterDesc">Filter</desc><polygon points="13 6 13 4 11 4 11 6 9.5 6 9.5 8 14.5 8 14.5 6 13 6"></polygon><polygon points="7 15 7 4 5 4 5 15 3.5 15 3.5 17 8.5 17 8.5 15 7 15"></polygon><polygon points="19 11 19 4 17 4 17 11 15.5 11 15.5 13 20.5 13 20.5 11 19 11"></polygon><rect x="11" y="9" width="2" height="11"></rect><rect x="5" y="18" width="2" height="2"></rect><rect x="17" y="14" width="2" height="6"></rect></symbol><symbol id="nfl-forward-5s"><desc id="nfl-forward-5sDesc">Forward 5s</desc><path d="M12.82,12.71c0-.49-.23-.62-.77-.62a1,1,0,0,0-.88.37l-1-.22V9.43h3.49v1.09H11.26v.87a1.69,1.69,0,0,1,1.15-.32,1.46,1.46,0,0,1,1.65,1.64v.22c0,1.18-.78,1.64-2.19,1.64a3.82,3.82,0,0,1-2-.48l.41-.92a3.07,3.07,0,0,0,1.49.4c.65,0,1-.11,1-.62Z"></path><path d="M12,2a9.93,9.93,0,0,1,7.06,2.94L21,3V8H16l1.64-1.64a8,8,0,1,0,0,11.3l1.42,1.42A10,10,0,1,1,12,2Z"></path></symbol><symbol id="nfl-forward-10s"><desc id="nfl-forward-10sDesc">Forward 10s</desc><path d="M10.47,14.56H9.2V10.43l-.82.06v-.8l1.23-.32h.85Z"></path><path d="M15.49,11.44v1.11c0,1.49-.54,2.07-2,2.07s-2-.58-2-2.07V11.44c0-1.49.54-2.07,2-2.07S15.49,10,15.49,11.44Zm-2.74.16v.8c0,.84,0,1.18.73,1.18s.73-.34.73-1.18v-.8c0-.84,0-1.18-.73-1.18S12.75,10.76,12.75,11.6Z"></path><path d="M12,2a9.93,9.93,0,0,1,7.06,2.94L21,3V8H16l1.64-1.64a8,8,0,1,0,0,11.3l1.42,1.42A10,10,0,1,1,12,2Z"></path></symbol><symbol id="nfl-forward-30s"><desc id="nfl-forward-30sDesc">Forward 30s</desc><path d="M7.53,14.14l.41-.93a3.19,3.19,0,0,0,1.53.4c.69,0,1-.16,1-.58h0c0-.38-.17-.59-.76-.59H8.54v-1h1c.49,0,.65-.21.65-.52h0c0-.27-.12-.5-.7-.5a3.05,3.05,0,0,0-1.44.37l-.41-.93a3.79,3.79,0,0,1,2-.46c1.42,0,1.81.6,1.81,1.51v0a1.11,1.11,0,0,1-.64,1,1.13,1.13,0,0,1,.86,1.18h0c0,1-.73,1.54-2.19,1.54A3.84,3.84,0,0,1,7.53,14.14Z"></path><path d="M16.29,11.44v1.11c0,1.49-.54,2.07-2,2.07s-2-.58-2-2.07V11.44c0-1.49.54-2.07,2-2.07S16.29,10,16.29,11.44Zm-2.74.16v.8c0,.84,0,1.18.73,1.18s.73-.34.73-1.18v-.8c0-.84,0-1.18-.73-1.18S13.55,10.76,13.55,11.6Z"></path><path d="M12,2a9.93,9.93,0,0,1,7.06,2.94L21,3V8H16l1.64-1.64a8,8,0,1,0,0,11.3l1.42,1.42A10,10,0,1,1,12,2Z"></path></symbol><symbol id="nfl-full-screen-off"><desc id="nfl-full-screen-offDesc">Full Screen Off</desc><polygon points="22 14 22 16 16 16 16 20 14 20 14 14 22 14"></polygon><polygon points="14 10 22 10 22 8 16 8 16 4 14 4 14 10"></polygon><polygon points="10 14 2 14 2 16 8 16 8 20 10 20 10 14"></polygon><polygon points="2 8 2 10 10 10 10 4 8 4 8 8 2 8"></polygon></symbol><symbol id="nfl-full-screen-on"><desc id="nfl-full-screen-onDesc">Full Screen On</desc><polygon points="10 4 10 6 4 6 4 10 2 10 2 4 10 4"></polygon><polygon points="2 20 10 20 10 18 4 18 4 14 2 14 2 20"></polygon><polygon points="22 4 14 4 14 6 20 6 20 10 22 10 22 4"></polygon><polygon points="14 18 14 20 22 20 22 14 20 14 20 18 14 18"></polygon></symbol><symbol id="nfl-gamepass"><desc id="nfl-gamepassDesc">Gamepass</desc><path d="M13.63,7.14H9.56v9.72h2.26V13.72h1.8c2.36,0,3.29-1.19,3.29-3.28v0C16.92,8.33,16,7.14,13.63,7.14Zm-.28,4.71H11.82V9h1.53c.93,0,1.24.42,1.24,1.4S14.28,11.85,13.35,11.85Z"></path><path d="M5,13.14H6.7V14.5a3.89,3.89,0,0,1-2,.47c-1.53,0-1.78-.78-1.78-1.76V10.79c0-1,.25-1.76,2-1.76a6,6,0,0,1,3,.75L8.74,7.9A6.84,6.84,0,0,0,4.93,7C1.61,7,.66,8.37.66,10.61v2.78c0,2.24,1,3.61,3.82,3.61a3.88,3.88,0,0,0,2.42-.78v.64H8.78V11.4H5Z"></path><polygon points="17.59 7.14 17.59 9.68 20.24 11.85 17.59 14.15 17.59 16.86 19.01 15.63 23.34 11.85 17.59 7.14"></polygon></symbol><symbol id="nfl-games"><desc id="nfl-gamesDesc">Games</desc><path d="M10,15.5v-1h1.25v-2H10v-1h1.25v-2H10v-1h1.25V3.13C9.1,3.76,6,6.9,6,12s3.1,8.24,5.25,8.87V15.5Z"></path><path d="M12.75,3.13V8.5H14v1H12.75v2H14v1H12.75v2H14v1H12.75v5.37C14.9,20.24,18,17.1,18,12S14.9,3.76,12.75,3.13Z"></path><rect class="cls-1" x="10" y="11.5" width="1.25" height="1"></rect><rect class="cls-1" x="10" y="8.5" width="1.25" height="1"></rect><rect class="cls-1" x="12.75" y="14.5" width="1.25" height="1"></rect><rect class="cls-1" x="10" y="14.5" width="1.25" height="1"></rect><rect class="cls-1" x="12.75" y="11.5" width="1.25" height="1"></rect><rect class="cls-1" x="12.75" y="8.5" width="1.25" height="1"></rect></symbol><symbol id="nfl-insights"><desc id="nfl-insightsDesc">Insights</desc><path d="M13,9V4H6V20H18V9Zm3,4.5H14.36l-1.32,4-2.09-5.23-.64,1.28H8v-1H9.69l1.36-2.72L13,14.55l.68-2H16Z"></path><polygon points="18 8 14 4 14 8 18 8"></polygon></symbol><symbol id="nfl-key"><desc id="nfl-keyDesc">Key</desc><path d="M20,10H12.58a5,5,0,1,0,0,4H14l1-1,1,1h1l1-1,1,1h1l2-2ZM6.5,13.5A1.5,1.5,0,1,1,8,12,1.5,1.5,0,0,1,6.5,13.5Z"></path></symbol><symbol id="nfl-leave"><desc id="nfl-leaveDesc">Leave</desc><polygon points="14 15 14 18 6 18 6 10 10 10 10 8 4 8 4 18 4 20 6 20 16 20 16 15 14 15"></polygon><polygon points="12 4 12 6 16.59 6 9.29 13.29 10.71 14.71 18 7.41 18 12 20 12 20 4 12 4"></polygon></symbol><symbol id="nfl-live"><desc id="nfl-liveDesc">Live</desc><path d="M2,6V18H22V6Zm5.75,8.5H5.25v-5h1v4h1.5Zm2.5,0h-1v-5h1Zm3.5,0h-1l-1.5-5h1l1,3.333,1-3.333h1Zm5-4h-1.5v1h1v1h-1v1h1.5v1h-2.5v-5h2.5Z"></path></symbol><symbol id="nfl-menu-combine"><desc id="nfl-menu-combineDesc">Combine</desc><path d="M15.23,7.8A6.983,6.983,0,0,0,13,7.079V5h2V3H9V5h2V7.08a6.97,6.97,0,1,0,4.23.716ZM12,18.5A4.5,4.5,0,1,1,16.5,14,4.5,4.5,0,0,1,12,18.5ZM16.273,7.252l1.561-1.561,2.475,2.475L18.733,9.742a7.9,7.9,0,0,0-.442-.67A8.015,8.015,0,0,0,16.273,7.252ZM13,14a1,1,0,0,1-2,0,1.013,1.013,0,0,1,.039-.277h0L12,10.84l.961,2.883h0A1.013,1.013,0,0,1,13,14Z"></path></symbol><symbol id="nfl-menu-draft"><desc id="nfl-menu-draftDesc">Draft</desc><path d="M5.5,21l1-2h11l1,2ZM21,3V5H3V3ZM5.5,6,7,18H17L18.5,6Zm9.25,7.875a.882.882,0,0,1-.223.569,1.846,1.846,0,0,1-1.2.541,1.77,1.77,0,0,0-1,.416,2.685,2.685,0,0,0-.328.432,2.256,2.256,0,0,0-.328-.432,1.808,1.808,0,0,0-1-.416,1.858,1.858,0,0,1-1.2-.535.884.884,0,0,1-.223-.57V9.331h.188a1.583,1.583,0,0,0,1.272.251,1.823,1.823,0,0,0,1.3-1.082,1.823,1.823,0,0,0,1.3,1.082,1.562,1.562,0,0,0,1.266-.251h.182Z"></path></symbol><symbol id="nfl-menu-fantasy"><desc id="nfl-menu-fantasyDesc">Fantasy</desc><path d="M16.909,5,17,4H7l.091,1H3a9.038,9.038,0,0,0,4.815,7.96L8,15l3,1.5V19H9L7,21H17l-2-2H13V16.5L16,15l.185-2.04A9.038,9.038,0,0,0,21,5ZM5.3,7H7.273l.309,3.4A7.01,7.01,0,0,1,5.3,7Zm11.123,3.4L16.727,7H18.7A7.01,7.01,0,0,1,16.418,10.4Z"></path></symbol><symbol id="nfl-menu-games"><desc id="nfl-menu-gamesDesc">Menu Games</desc><path d="M10,15.5v-1h1.25v-2H10v-1h1.25v-2H10v-1h1.25V3.131C9.1,3.763,6,6.9,6,12s3.1,8.237,5.25,8.869V15.5Z"></path><path d="M12.75,3.131V8.5H14v1H12.75v2H14v1H12.75v2H14v1H12.75v5.369C14.9,20.237,18,17.1,18,12S14.9,3.763,12.75,3.131Z"></path></symbol><symbol id="nfl-menu-network"><desc id="nfl-menu-networkDesc">Menu Network</desc><path d="M3,4V16h8v2H6v2H18V18H13V16h8V4ZM20,15H4V5H20Z"></path><rect x="5" y="6" width="14" height="8"></rect></symbol><symbol id="nfl-menu-news"><desc id="nfl-menu-newsDesc">Menu News</desc><rect x="4" y="4" width="16" height="8"></rect><rect x="4" y="14" width="16" height="2"></rect><rect x="4" y="18" width="8" height="2"></rect></symbol><symbol id="nfl-menu-playoffs"><desc id="nfl-menu-playoffsDesc">Menu Playoffs</desc><polygon points="22 6 22 4 17.5 4 17.5 6.25 13.5 6.25 13.5 11 10.5 11 10.5 6.25 6.5 6.25 6.5 4 2 4 2 6 4.5 6 4.5 8.5 2 8.5 2 10.5 6.5 10.5 6.5 8.25 8.5 8.25 8.5 15.75 6.5 15.75 6.5 13.5 2 13.5 2 15.5 4.5 15.5 4.5 18 2 18 2 20 6.5 20 6.5 17.75 10.5 17.75 10.5 13 13.5 13 13.5 17.75 17.5 17.75 17.5 20 22 20 22 18 19.5 18 19.5 15.5 22 15.5 22 13.5 17.5 13.5 17.5 15.75 15.5 15.75 15.5 8.25 17.5 8.25 17.5 10.5 22 10.5 22 8.5 19.5 8.5 19.5 6 22 6"></polygon></symbol><symbol id="nfl-menu-probowl"><desc id="nfl-menu-probowlDesc">Menu Pro Bowl</desc><path d="M6.924,8.2a7.451,7.451,0,0,0-.213.8,8.98,8.98,0,0,0,1.141,6.639c.076.114-.377.789-.377.789H6.721L6,18H18l-.721-1.579h-.754s-.453-.675-.377-.789a8.98,8.98,0,0,0,1.141-6.639,7.451,7.451,0,0,0-.213-.8A8.311,8.311,0,0,0,12,3,8.311,8.311,0,0,0,6.924,8.2ZM9.412,9.236a4.6,4.6,0,0,1,1.8-3.754A6.388,6.388,0,0,1,12,4.974a6.388,6.388,0,0,1,.786.508,4.6,4.6,0,0,1,1.8,3.754,4.471,4.471,0,0,1-2.048,4.1l-.54.323-.54-.323A4.471,4.471,0,0,1,9.412,9.236Z"></path><rect x="4" y="19" width="16" height="2"></rect></symbol><symbol id="nfl-menu-shop"><desc id="nfl-menu-shopDesc">Menu Shop</desc><path d="M12,6,9,4H6v6H7V20H17V10h1V4H15Zm0,1a.75.75,0,1,1-.75.75A.75.75,0,0,1,12,7Zm2,3.7a.328.328,0,0,1-.019.112l-2.023,5.106a.122.122,0,0,1-.131.084H10.656c-.056,0-.085-.037-.056-.1l2.023-5.1v-.028h-1.34a.05.05,0,0,0-.056.056v.665a.089.089,0,0,1-.094.094H10.094A.089.089,0,0,1,10,11.494V9.724a.088.088,0,0,1,.094-.094h3.812A.088.088,0,0,1,14,9.724Z"></path><rect x="3" y="4" width="2" height="6"></rect><rect x="19" y="4" width="2" height="6"></rect></symbol><symbol id="nfl-menu-standings"><desc id="nfl-menu-standingsDesc">Menu Standings</desc><rect x="10" y="5" width="10" height="2"></rect><rect x="10" y="11" width="10" height="2"></rect><rect x="10" y="17" width="10" height="2"></rect><rect x="4" y="4" width="4" height="4"></rect><rect x="4" y="10" width="4" height="4"></rect><rect x="4" y="16" width="4" height="4"></rect></symbol><symbol id="nfl-menu-stats"><desc id="nfl-menu-statsDesc">Menu Stats</desc><rect x="15.5" y="8" width="3" height="12"></rect><rect x="10.5" y="4" width="3" height="16"></rect><rect x="5.5" y="12" width="3" height="8"></rect></symbol><symbol id="nfl-menu-superbowl"><desc id="nfl-menu-superbowlDesc">Menu Super Bowl</desc><path d="M7.99,15.54l-.74,5.473a10.118,10.118,0,0,0,3.855,1.133V15.4a10.953,10.953,0,0,1-2.025.191C8.718,15.592,8.354,15.572,7.99,15.54Z"></path><path d="M16.387,4.387A12.1,12.1,0,0,0,14.939,4.3a8.852,8.852,0,0,0-5.71,1.864,8.765,8.765,0,0,0-2.86,6.612A1.125,1.125,0,0,0,7.323,13.9a10.875,10.875,0,0,0,1.757.145,8.075,8.075,0,0,0,5.243-1.763,9.535,9.535,0,0,0,3.046-6.717A1.12,1.12,0,0,0,16.387,4.387Z"></path><path d="M12.652,14.983v7.175a10.125,10.125,0,0,0,4.07-1.132L15.383,13.4c-.025.022-.044.047-.07.068A8.866,8.866,0,0,1,12.652,14.983Z"></path></symbol><symbol id="nfl-menu-teams"><desc id="nfl-menu-teamsDesc">Menu Teams</desc><path d="M15,10h6c0-2-2-7-9-7S3,9,3,13l2,5c2,0,3,2,5,2s2.854-1.146,3.854-2.146L16,20h5V13H13.875Zm-3.939,7.646a1.207,1.207,0,1,1,0-1.707A1.206,1.206,0,0,1,11.061,17.646ZM19,15v3H17l-3-3Z"></path></symbol><symbol id="nfl-menu-tickets"><desc id="nfl-menu-ticketsDesc">Menu Tickets</desc><path d="M14,3a2,2,0,0,1-4,0H7V21h3a2,2,0,0,1,4,0h3V3Zm-.772,9.722.759,2.336L12,13.614l-1.987,1.444.759-2.336L8.784,11.278h2.457L12,8.942l.759,2.336h2.457Z"></path></symbol><symbol id="nfl-menu"><desc id="nfl-menuDesc">Menu</desc><rect x="4" y="6" width="16" height="2"></rect><rect x="4" y="11" width="16" height="2"></rect><rect x="4" y="16" width="16" height="2"></rect></symbol><symbol id="nfl-more-horizontal"><desc id="nfl-more-horizontalDesc">More Horizontal</desc><circle cx="12" cy="12" r="2"></circle><circle cx="19" cy="12" r="2"></circle><circle cx="5" cy="12" r="2"></circle></symbol><symbol id="nfl-more-vertical"><desc id="nfl-more-verticalDesc">More Vertical</desc><circle cx="12" cy="12" r="2"></circle><circle cx="12" cy="19" r="2"></circle><circle cx="12" cy="5" r="2"></circle></symbol><symbol id="nfl-my-location"><desc id="nfl-my-locationDesc">My Location</desc><path d="M20,11H17.91A6,6,0,0,0,13,6.09V4H11V6.09A6,6,0,0,0,6.09,11H4v2H6.09A6,6,0,0,0,11,17.91V20h2V17.91A6,6,0,0,0,17.91,13H20Zm-8,5a4,4,0,1,1,4-4A4,4,0,0,1,12,16Z"></path></symbol><symbol id="nfl-network"><desc id="nfl-networkDesc">Network</desc><path d="M3,4V16h8v2H6v2H18V18H13V16h8V4ZM20,15H4V5H20Z"></path><rect x="5" y="6" width="14" height="8"></rect></symbol><symbol id="nfl-news"><desc id="nfl-newsDesc">News</desc><rect x="4" y="4" width="16" height="8"></rect><rect x="4" y="14" width="16" height="2"></rect><rect x="4" y="18" width="8" height="2"></rect></symbol><symbol id="nfl-pause"><desc id="nfl-pauseDesc">Pause</desc><rect x="6" y="4" width="4" height="16"></rect><rect x="14" y="4" width="4" height="16"></rect></symbol><symbol id="nfl-play"><desc id="nfl-playDesc">play</desc><polygon points="7 4 7 20 19 12 7 4"></polygon></symbol><symbol id="nfl-player-multi"><desc id="nfl-player-multiDesc">Multiple Players</desc><path d="M9,8A1,1,0,0,0,8,7H5A1,1,0,0,0,4,8v4H5v4H8V9.91A1.9,1.9,0,0,1,9,8.24Z"></path><path d="M19,7H16a1,1,0,0,0-1,1v.24a1.9,1.9,0,0,1,1,1.67V16h3V12h1V8A1,1,0,0,0,19,7Z"></path><path d="M14,9H10a1,1,0,0,0-1,1v5h1v5h4V15h1V10A1,1,0,0,0,14,9Z"></path><circle cx="12" cy="6" r="2"></circle><circle cx="17.5" cy="4.5" r="1.5"></circle><circle cx="6.5" cy="4.5" r="1.5"></circle></symbol><symbol id="nfl-player-single"><desc id="nfl-player-singleDesc">Single Player</desc><path d="M14,9H10a1,1,0,0,0-1,1v5h1v5h4V15h1V10A1,1,0,0,0,14,9Z"></path><circle cx="12" cy="6" r="2"></circle></symbol><symbol id="nfl-playlist"><desc id="nfl-playlistDesc">Playlist</desc><path class="cls-2" d="M12,1A11,11,0,1,0,23,12,11,11,0,0,0,12,1ZM7.5,8.75l2,1-2,1Zm9,6.5h-9v-1h9Zm0-2.5h-9v-1h9Zm0-2.5h-6v-1h6Z"></path><path class="cls-3" d="M16.5,9.25v1h-6v-1Zm-9,3.5h9v-1h-9Zm0,2.5h9v-1h-9Zm0-6.5v2l2-1Z"></path></symbol><symbol id="nfl-playoffs"><desc id="nfl-playoffsDesc">Playoffs</desc><path d="M12.95,8.79A1.33,1.33,0,0,1,12,8a1.33,1.33,0,0,1-.94.79,1.15,1.15,0,0,1-.93-.18H10v3.31a.64.64,0,0,0,.16.41,1.35,1.35,0,0,0,.87.39,1.31,1.31,0,0,1,.73.3,1.65,1.65,0,0,1,.24.31,2,2,0,0,1,.24-.31,1.29,1.29,0,0,1,.73-.3,1.34,1.34,0,0,0,.87-.39.64.64,0,0,0,.16-.41V8.6h-.13A1.14,1.14,0,0,1,12.95,8.79Z"></path><path d="M7.19,6.73C4.27,10.42,4.82,16.13,5.71,18h7.46a9.37,9.37,0,0,0,3.64-2.73C20.1,11.12,19,4.4,18,3.49S10.48,2.58,7.19,6.73Zm9.06,6.55c-1.52,2.63-4.63,3.68-7,2.34s-3-4.56-1.45-7.19,4.63-3.68,7-2.34S17.77,10.64,16.26,13.27Z"></path><rect x="4" y="19" width="16" height="2"></rect></symbol><symbol id="nfl-probowl"><desc id="nfl-probowlDesc">Pro Bowl</desc><path d="M6.92,8.19a7.52,7.52,0,0,0-.21.8,9,9,0,0,0,1.14,6.64c.08.11-.38.79-.38.79H6.72L6,18H18l-.72-1.58h-.75s-.45-.68-.38-.79A9,9,0,0,0,17.29,9a7.52,7.52,0,0,0-.21-.8A8.31,8.31,0,0,0,12,3,8.31,8.31,0,0,0,6.92,8.19Zm2.49,1a4.6,4.6,0,0,1,1.8-3.75A6.34,6.34,0,0,1,12,5a6.34,6.34,0,0,1,.79.51,4.6,4.6,0,0,1,1.8,3.75,4.47,4.47,0,0,1-2,4.1l-.54.32-.54-.32A4.47,4.47,0,0,1,9.41,9.24Z"></path><rect x="4" y="19" width="16" height="2"></rect></symbol><symbol id="nfl-purge"><desc id="nfl-purgeDesc">Purge</desc><polygon points="13.41 12 18.21 7.21 16.79 5.79 12 10.59 7.21 5.79 5.79 7.21 10.59 12 5.79 16.79 7.21 18.21 12 13.41 16.79 18.21 18.21 16.79 13.41 12"></polygon></symbol><symbol id="nfl-refresh"><desc id="nfl-refreshDesc">Refresh</desc><path d="M12,2A9.93,9.93,0,0,0,4.94,4.94L3,3V8H8L6.36,6.36a8,8,0,1,1,0,11.3L4.93,19.07A10,10,0,1,0,12,2Z"></path></symbol><symbol id="nfl-remove"><desc id="nfl-removeDesc">Remove</desc><rect x="5" y="11" width="14" height="2"></rect></symbol><symbol id="nfl-replay"><desc id="nfl-replayDesc">Replay</desc><path d="M12,2A9.93,9.93,0,0,0,4.94,4.94L3,3V8H8L6.36,6.36a8,8,0,1,1,0,11.3L4.93,19.07A10,10,0,1,0,12,2Z"></path><polygon points="10 8 10 16 16 12 10 8"></polygon></symbol><symbol id="nfl-search"><desc id="nfl-searchDesc">Search</desc><path d="M19.71,18.29l-4-4a6.52,6.52,0,1,0-1.41,1.41l4,4ZM6,10.5A4.5,4.5,0,1,1,10.5,15,4.51,4.51,0,0,1,6,10.5Z"></path></symbol><symbol id="nfl-settings"><desc id="nfl-settingsDesc">Settings</desc><path d="M18.94,12.89h0a6.33,6.33,0,0,0,0-1.78h0l1.86-1a8.94,8.94,0,0,0-1.23-3l-2,.6-.18-.23h0a7,7,0,0,0-.84-.84h0l-.23-.18h0l.6-2a8.94,8.94,0,0,0-3-1.23l-1,1.86a6.34,6.34,0,0,0-1.78,0l-1-1.86a8.94,8.94,0,0,0-3,1.23l.6,2h0l-.23.18h0a7,7,0,0,0-.84.84h0l-.18.23-2-.6a8.94,8.94,0,0,0-1.23,3l1.86,1h0a6.33,6.33,0,0,0,0,1.78h0l-1.86,1a8.94,8.94,0,0,0,1.23,3l2-.6.18.23h0a7,7,0,0,0,.84.84h0l.23.18h0l-.6,2a8.94,8.94,0,0,0,3,1.23l1-1.86a6.34,6.34,0,0,0,1.78,0l1,1.86a8.94,8.94,0,0,0,3-1.23l-.6-2h0l.23-.18h0a7.06,7.06,0,0,0,.84-.84h0l.18-.23,2,.6a8.94,8.94,0,0,0,1.23-3l-1.86-1ZM12,14a2,2,0,1,1,2-2A2,2,0,0,1,12,14Z"></path></symbol><symbol id="nfl-share-android"><desc id="nfl-share-androidDesc">Share Android</desc><path d="M17,15a3,3,0,0,0-2.08.84l-6-3a2.69,2.69,0,0,0,0-1.64l6-3A3,3,0,1,0,14,6a3,3,0,0,0,0,.36L7.7,9.53a3,3,0,1,0,0,4.94L14,17.64A3,3,0,0,0,14,18a3,3,0,1,0,3-3Z"></path></symbol><symbol id="nfl-share-copy-url"><desc id="nfl-share-copy-urlDesc">Share Copy URL</desc><path d="M8,4V16H20V4ZM18,14H10V6h8Z"></path><polygon points="6 8 4 8 4 18 4 20 6 20 16 20 16 18 6 18 6 8"></polygon></symbol><symbol id="nfl-share-email"><desc id="nfl-share-emailDesc">Share Email</desc><polygon points="19 9 19 7 5 7 5 9 12 13 19 9"></polygon><polygon points="5 17 19 17 19 10 12 14 5 10 5 17"></polygon></symbol><symbol id="nfl-share-facebook"><desc id="nfl-share-facebookDesc">Share Facebook</desc><path class="cls-2" d="M14.86,12l.32-2.33H12.72V8.54c0-.72.2-1.21,1.23-1.21h1.32V5.1A17.59,17.59,0,0,0,13.35,5a3,3,0,0,0-3.2,3.29V9.67H8V12h2.15v7h2.57V12h2.14"></path></symbol><symbol id="nfl-share-instagram"><desc id="nfl-share-instagramDesc">Share Instagram</desc><path d="M12,7.77a4.24,4.24,0,0,1,3.46,1.79H19V7.19A2.19,2.19,0,0,0,16.84,5H7.19A2.19,2.19,0,0,0,5,7.19V9.57H8.55A4.24,4.24,0,0,1,12,7.77ZM16,6.53a.39.39,0,0,1,.39-.39h1.17a.39.39,0,0,1,.39.39V7.71a.39.39,0,0,1-.39.39H16.34A.39.39,0,0,1,16,7.71Z"></path><path d="M16.26,12a4.24,4.24,0,1,1-8.4-.82H5v5.64A2.19,2.19,0,0,0,7.19,19h9.65A2.19,2.19,0,0,0,19,16.84V11.2H16.18A4.35,4.35,0,0,1,16.26,12Z"></path><circle cx="12.02" cy="12.01" r="2.61"></circle></symbol><symbol id="nfl-share-ios"><desc id="nfl-share-iosDesc">Share iOS</desc><polygon points="11 14 13 14 13 5 16 5 12 1 8 5 11 5 11 14"></polygon><polygon points="15 8 15 10 20 10 20 19 4 19 4 10 9 10 9 8 2 8 2 21 22 21 22 8 15 8"></polygon></symbol><symbol id="nfl-share-snapchat"><desc id="nfl-share-snapchatDesc">Share Snapchat</desc><path d="M12.16,4.38h-.32A4.31,4.31,0,0,0,7.83,7a8.11,8.11,0,0,0-.2,3.28l0,.39a.65.65,0,0,1-.32.07,1.81,1.81,0,0,1-.75-.2.6.6,0,0,0-.26-.05.8.8,0,0,0-.79.54c0,.23.06.57.81.87l.24.08c.31.1.78.25.91.55a.72.72,0,0,1-.08.59v0a4.77,4.77,0,0,1-3.24,2.72.34.34,0,0,0-.28.35.45.45,0,0,0,0,.15c.17.39.86.67,2.13.87a1.58,1.58,0,0,1,.11.38c0,.12.05.25.09.38a.38.38,0,0,0,.39.29A2,2,0,0,0,7,18.2a4.28,4.28,0,0,1,.86-.1,3.81,3.81,0,0,1,.62.05,2.94,2.94,0,0,1,1.16.6,3.66,3.66,0,0,0,2.23.87h.2a3.66,3.66,0,0,0,2.23-.87,2.94,2.94,0,0,1,1.16-.6,3.8,3.8,0,0,1,.62-.05,4.31,4.31,0,0,1,.86.09,2,2,0,0,0,.37,0h0a.37.37,0,0,0,.37-.28c0-.13.07-.25.09-.38a1.57,1.57,0,0,1,.11-.38c1.27-.2,2-.48,2.13-.87a.45.45,0,0,0,0-.15.34.34,0,0,0-.28-.35,4.77,4.77,0,0,1-3.24-2.72v0a.72.72,0,0,1-.08-.59c.13-.3.6-.45.91-.55l.24-.08c.55-.22.83-.48.82-.79a.66.66,0,0,0-.49-.57h0a.9.9,0,0,0-.34-.06.75.75,0,0,0-.31.06,1.89,1.89,0,0,1-.7.2.62.62,0,0,1-.27-.07l0-.35v0A8.11,8.11,0,0,0,16.17,7a4.32,4.32,0,0,0-4-2.59Z"></path></symbol><symbol id="nfl-share-twitter"><desc id="nfl-share-twitterDesc">Share Twitter</desc><path class="cls-2" d="M18.92,7.71a5.66,5.66,0,0,1-1.63.45,2.84,2.84,0,0,0,1.25-1.57,5.68,5.68,0,0,1-1.8.69,2.84,2.84,0,0,0-4.91,1.94,2.87,2.87,0,0,0,.07.65A8.06,8.06,0,0,1,6,6.89a2.84,2.84,0,0,0,.88,3.79,2.83,2.83,0,0,1-1.29-.36v0a2.84,2.84,0,0,0,2.28,2.78,2.82,2.82,0,0,1-1.28,0,2.84,2.84,0,0,0,2.65,2,5.7,5.7,0,0,1-3.53,1.22,5.84,5.84,0,0,1-.68,0A8.08,8.08,0,0,0,17.51,9.54c0-.12,0-.25,0-.37a5.78,5.78,0,0,0,1.42-1.47"></path></symbol><symbol id="nfl-skip-next"><desc id="nfl-skip-nextDesc">Skip Next</desc><polygon points="14 4 14 10 5 4 5 20 14 14 14 20 18 20 18 4 14 4"></polygon></symbol><symbol id="nfl-skip-previous"><desc id="nfl-skip-previousDesc">Skip Previous</desc><polygon points="19 4 10 10 10 4 6 4 6 20 10 20 10 14 19 20 19 4"></polygon></symbol><symbol id="nfl-standings"><desc id="nfl-standingsDesc">Standings</desc><rect x="10" y="5" width="10" height="2"></rect><rect x="10" y="11" width="10" height="2"></rect><rect x="10" y="17" width="10" height="2"></rect><rect x="4" y="4" width="4" height="4"></rect><rect x="4" y="10" width="4" height="4"></rect><rect x="4" y="16" width="4" height="4"></rect></symbol><symbol id="nfl-star"><desc id="nfl-starDesc">Star</desc><polygon points="12 3 14.11 9.49 20.94 9.49 15.41 13.51 17.52 20 12 15.99 6.48 20 8.59 13.51 3.06 9.49 9.89 9.49 12 3"></polygon></symbol><symbol id="nfl-stats"><desc id="nfl-statsDesc">Stats</desc><rect x="15.5" y="8" width="3" height="12"></rect><rect x="10.5" y="4" width="3" height="16"></rect><rect x="5.5" y="12" width="3" height="8"></rect></symbol><symbol id="nfl-swap"><desc id="nfl-swapDesc">Swap</desc><polygon points="19 8 7 8 7 6 4 9 7 12 7 10 19 10 19 8"></polygon><polygon points="20 15 17 12 17 14 5 14 5 16 17 16 17 18 20 15"></polygon></symbol><symbol id="nfl-teams"><desc id="nfl-teamsDesc">Teams</desc><path d="M15,10h6c0-2-2-7-9-7S3,9,3,13l2,5c2,0,3,2,5,2s2.85-1.15,3.85-2.15L16,20h5V13H13.88Zm-3.94,7.65a1.21,1.21,0,1,1,0-1.71A1.21,1.21,0,0,1,11.06,17.65ZM19,15v3H17l-3-3Z"></path></symbol><symbol id="nfl-tickets"><desc id="nfl-ticketsDesc">Tickets</desc><path d="M14,3a2,2,0,0,1-4,0H7V21h3a2,2,0,0,1,4,0h3V3Zm-.77,9.72L14,15.06l-2-1.44-2,1.44.76-2.34-2-1.44h2.46L12,8.94l.76,2.34h2.46Z"></path></symbol><symbol id="nfl-video"><desc id="nfl-videoDesc">Video</desc><path class="cls-1" d="M12,1A11,11,0,1,0,23,12,11,11,0,0,0,12,1ZM10,16V8l6,4Z"></path><path class="cls-2" d="M12,2A10,10,0,1,1,2,12,10,10,0,0,1,12,2m0-1A11,11,0,1,0,23,12,11,11,0,0,0,12,1Z"></path><polygon class="cls-2" points="10 16 16 12 10 8 10 16"></polygon><rect class="cls-3" width="24" height="24"></rect></symbol><symbol id="nfl-visibility-off"><desc id="nfl-visibility-offDesc">Visibility Off</desc><path d="M8.5,12A3.5,3.5,0,0,1,12,8.5a3.46,3.46,0,0,1,1.6.4l1.34-1.34A8.17,8.17,0,0,0,12,7a9.53,9.53,0,0,0-8,5,9.7,9.7,0,0,0,3.1,3.4l1.8-1.8A3.46,3.46,0,0,1,8.5,12Z"></path><path d="M12,10a2,2,0,0,0-1.95,2.45l2.39-2.39A2,2,0,0,0,12,10Z"></path><path d="M16.86,8.57,20,5.47l-.73-.73L4.68,19.29h0l.73.73L9,16.42A8.17,8.17,0,0,0,12,17a9.53,9.53,0,0,0,8-5A9.71,9.71,0,0,0,16.86,8.57ZM12,15.5a3.46,3.46,0,0,1-1.64-.43l1.14-1.14a2,2,0,0,0,2.43-2.43l1.14-1.14A3.46,3.46,0,0,1,15.5,12,3.5,3.5,0,0,1,12,15.5Z"></path></symbol><symbol id="nfl-visibility-on"><desc id="nfl-visibility-onDesc">Visibility On</desc><path d="M12,7a9.53,9.53,0,0,0-8,5,9.53,9.53,0,0,0,8,5,9.53,9.53,0,0,0,8-5A9.53,9.53,0,0,0,12,7Zm0,8.5A3.5,3.5,0,1,1,15.5,12,3.5,3.5,0,0,1,12,15.5Z"></path><circle cx="12" cy="12" r="2"></circle></symbol><symbol id="nfl-volume-hi"><desc id="nfl-volume-hiDesc">Volume Hi</desc><path d="M17.55,3.68,16.44,5.35a8,8,0,0,1,0,13.31l1.11,1.67a10,10,0,0,0,0-16.64Z"></path><path d="M15.33,7,14.22,8.67a4,4,0,0,1,0,6.65L15.33,17a6,6,0,0,0,0-10Z"></path><polygon points="3 8 3 16 6 16 12 21 12 3 6 8 3 8"></polygon></symbol><symbol id="nfl-volume-low"><desc id="nfl-volume-lowDesc">Volume Low</desc><polygon points="12 3 6 8 3 8 3 16 6 16 12 21 12 3"></polygon></symbol><symbol id="nfl-volume-medium"><desc id="nfl-volume-mediumDesc">Volume Medium</desc><path d="M18,12a6,6,0,0,0-2.67-5L14.22,8.67a4,4,0,0,1,0,6.65L15.33,17A6,6,0,0,0,18,12Z"></path><polygon points="12 3 6 8 3 8 3 16 6 16 12 21 12 3"></polygon></symbol><symbol id="nfl-volume-mute"><desc id="nfl-volume-muteDesc">Volume Mute</desc><polygon points="12 3 6 8 3 8 3 16 6 16 12 21 12 3"></polygon><polygon points="18.41 12 20.19 10.22 18.78 8.81 17 10.59 15.21 8.79 13.79 10.21 15.59 12 13.79 13.79 15.21 15.21 17 13.41 18.81 15.22 20.22 13.81 18.41 12"></polygon></symbol><symbol id="nfl-warning"><desc id="nfl-warningDesc">Warning</desc><path d="M12,2,2,20H22Zm1,15H11V15h2Zm-2-4V8h2v5Z"></path></symbol><symbol id="nfl-website"><desc id="nfl-websiteDesc">Website</desc><path class="cls-2" d="M8.65,16.3A16.87,16.87,0,0,1,11.6,16v4C10.38,19.7,9.3,18.29,8.65,16.3Zm1,3.34a8.26,8.26,0,0,1-1.76-3.17,8.51,8.51,0,0,0-1.94.73A8,8,0,0,0,9.62,19.64Zm4.76,0a8,8,0,0,0,3.7-2.44,8.51,8.51,0,0,0-1.94-.73A8.26,8.26,0,0,1,14.38,19.64ZM11.6,15.19V12.38H8a14.8,14.8,0,0,0,.42,3.15A18.23,18.23,0,0,1,11.6,15.19Zm0-3.61V8.76a18.27,18.27,0,0,1-3.18-.33A14.71,14.71,0,0,0,8,11.58Zm-2-7.26a8,8,0,0,0-3.7,2.44,9,9,0,0,0,1.94.73A8.26,8.26,0,0,1,9.62,4.32Zm2-.32c-1.22.26-2.3,1.67-2.95,3.66A16.87,16.87,0,0,0,11.6,8Zm.8,16c1.22-.26,2.3-1.67,2.95-3.66A16.87,16.87,0,0,0,12.4,16Zm0-11.2v2.82H16a14.71,14.71,0,0,0-.42-3.15A18.27,18.27,0,0,1,12.4,8.76ZM7.64,15.7a15,15,0,0,1-.44-3.32H4a8,8,0,0,0,1.42,4.18A9.44,9.44,0,0,1,7.64,15.7Zm4.76-3.32v2.81a18.23,18.23,0,0,1,3.18.34A14.8,14.8,0,0,0,16,12.38Zm-5.2-.8a15.06,15.06,0,0,1,.44-3.33A9,9,0,0,1,5.42,7.4,8,8,0,0,0,4,11.58Zm9.6.8a15,15,0,0,1-.44,3.32,9.44,9.44,0,0,1,2.22.86A8,8,0,0,0,20,12.38Zm-.44-4.13a15.06,15.06,0,0,1,.44,3.33H20A8,8,0,0,0,18.58,7.4,9,9,0,0,1,16.36,8.25Zm-2-3.93a8.26,8.26,0,0,1,1.76,3.17,9,9,0,0,0,1.94-.73A8,8,0,0,0,14.38,4.32ZM12.4,4V8a16.87,16.87,0,0,0,2.95-.3C14.7,5.67,13.62,4.26,12.4,4Z"></path><g id="Bell">
  <rect id="Bounds" class="cls-1" width="24" height="24"></rect>
</g><rect id="Bounds" class="cls-1" width="24" height="24"></rect></symbol><symbol id="nfl-caret-down"><desc id="nfl-caret-downDesc">Caret down</desc><polygon points="6 9 18 9 12 15"></polygon></symbol><symbol id="nfl-caret-up"><desc id="nfl-caret-upDesc">Caret up</desc><polygon transform="translate(12.000000, 12.000000) scale(1, -1) translate(-12.000000, -12.000000)" points="6 9 18 9 12 15"></polygon></symbol><symbol id="nfl-at"><desc id="nfl-atDesc">At</desc><path d="M10.188,21h0C5.814,21,3.476,18.808,4.1,15.686L5.572,8.314C6.2,5.192,9.4,3,13.8,3s6.72,2.192,6.1,5.314l-1.547,7.722a1.686,1.686,0,0,1-1.8,1.461H15.139c-1.265,0-1.812-.326-2.035-.918A4.77,4.77,0,0,1,9.96,17.661c-3.048,0-3.477-2.261-3.137-3.922l.7-3.486a4.67,4.67,0,0,1,4.706-3.938A3.372,3.372,0,0,1,15,7.439l.62-.96h2.257l-1.879,9.358c-.058.315.051.461.368.461h.072A.536.536,0,0,0,17,15.844l1.486-7.43C18.971,6,17.246,4.237,13.566,4.237c-3.55,0-6.108,1.657-6.613,4.177L5.516,15.585C5.029,18,6.77,19.763,10.444,19.763a7.152,7.152,0,0,0,4.682-1.516l.7.918A8.656,8.656,0,0,1,10.188,21ZM11.2,15.406a2.9,2.9,0,0,0,1.925-.738L14.2,9.324a1.853,1.853,0,0,0-1.624-.754,1.821,1.821,0,0,0-1.832,1.717l-.682,3.4c-.231,1.192.22,1.718,1.143,1.718Z"></path></symbol></svg>


<script src="https://www.nfl.com/compiledassets/js/vendor/requirejs/require-2.3.5.min.js?_t=bebd45d1f406bbe61424136b03e50895" type="text/javascript" async="" defer="" data-main="https://www.nfl.com/compiledassets/js/main.js?_t=11ff89d037be6b1242b9c71f6a639def"></script><script src="https://www.nfl.com/compiledassets/js/vendor/picturefill/picturefill.min.js?_t=c927ae7f55dda5652225c00240e24232" type="text/javascript" async="" defer=""></script>

</div>

  <div id="fb-root"></div>
  <script type="text/javascript">
    window._taboola = window._taboola || [];
    _taboola.push({ flush: true });
  </script>

  
  

  

<div class="nfl-c-cookie-consent" data-require="modules/cookieConsent" data-cookiepolicyurl="https://www.nfl.com/legal/privacy" data-cookiedomain=".nfl.com"></div>

  <div class="nfl-c-unsupported-browsers nfl-c-unsupported-browsers--hide" data-unsupported-browsers="parent" aria-hidden="true" data-require="modules/unsupportedBrowsers">
  <div role="dialog" aria-described-by="unsupportedBrowsers:desc" class="cc-window d3-o-cookie cc-banner cc-type-info cc-theme-block cc-bottom ">
    <span id="unsupportedBrowsers:desc" class="d3-o-cookie-message">The browser you are using is no longer supported on this site. It is highly recommended that you use the latest versions of a supported browser in order to receive an optimal viewing experience. The following browsers are supported: Chrome, Edge, Firefox and Safari.</span>
    <div class="cc-compliance">
      <button data-unsupported-browsers="button" aria-label="dismiss cookie message" tabindex="0" class="cc-btn cc-dismiss d3-o-cookie-btn d3-o-cookie-btn--dismiss">Got it!</button>
    </div>
  </div>
</div>

<div id="parsely-root" style="display: none;"><span id="parsely-cfg" data-parsely-site="nfl.com"></span></div><script src="//d1z2jf7jlzjs58.cloudfront.net/keys/nfl.com/p.js"></script><iframe src="https://cdns.us1.gigya.com/gs/webSdk/Api.aspx?apiKey=3_Qa8TkWpIB8ESCBT8tY2TukbVKgO5F6BJVc7N1oComdwFzI7H2L9NOWdm11i_BY9f&amp;version=latest&amp;build=10936#origin=https://www.nfl.com/schedules/2020/REG1/&amp;gig_loggerConfig=%7B%22logLevel%22%3A0%2C%22clientMuteLevel%22%3A0%2C%22logTheme%22%3A1%7D" style="position: absolute; height: 0px; width: 0px; display: none;"></iframe><div class="ui-selectmenu-menu d3-js-dropdown__menu ui-front"><ul aria-hidden="true" aria-labelledby="ui-id-1-button" id="ui-id-1-menu" role="listbox" tabindex="0" class="ui-menu ui-corner-bottom ui-widget ui-widget-content"></ul></div><div class="ui-selectmenu-menu d3-js-dropdown__menu ui-front"><ul aria-hidden="true" aria-labelledby="ui-id-2-button" id="ui-id-2-menu" role="listbox" tabindex="0" class="ui-menu ui-corner-bottom ui-widget ui-widget-content"></ul></div><iframe id="google_osd_static_frame_1927782016478" name="google_osd_static_frame" style="display: none; width: 0px; height: 0px;"></iframe></body>
        </html> 
    </div>    
    </div>
</div>
