<?php
use yii\helpers\Html;
use yii\base\Widget;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */

$this->title = 'National Football League Management';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>National Football League Management</h1>        
    </div>
    <br>
    <br>
    <h2 style="padding-bottom: 0px">Noticias</h2>
    <hr></hr>
    <br>
    <div class="row">
        <div class="col-lg-6">
            <?= Html::img('@web/foto.jpg', ['alt'=>Yii::$app->name,'class'=>'news'])?>
            <p class="text_url"><a href="https://www.cbssports.com/nfl/news/why-nyheim-hines-former-philip-rivers-award-winner-is-a-perfect-fit-alongside-the-new-colts-qb/" target="_blank" style="color: grey">¿Por qué Hines encaja perfectamente con Rivers?</a></p>
        </div>
        <div class="col-lg-6">
            <?= Html::img('@web/foto2.jpg', ['alt'=>Yii::$app->name,'class'=>'news'])?>
            <p class="text_url"><a href="https://www.cbssports.com/nfl/news/texans-justin-reid-seeking-fair-opportunities-from-the-nfl-for-colin-kaepernick-eric-reid/" target="_blank" style="color: grey">Reid a la NFL: ayuda a los jugadores que protestan</a></p>
        </div>
     </div>
    <div class="row">
        <div class="col-lg-6">
            <?= Html::img('@web/foto3.jpg', ['alt'=>Yii::$app->name,'class'=>'news'])?>
            <p class="text_url"><a href="https://www.cbssports.com/nfl/news/shane-vereen-on-patriots-benching-malcolm-butler-in-super-bowl-i-have-not-gotten-the-same-answer/" target="_blank" style="color: grey">Vereen: Butler todavía es un misterio</a></p>
        </div>
        <div class="col-lg-6">
            <?= Html::img('@web/foto4.jpg', ['alt'=>Yii::$app->name,'class'=>'news'])?>
            <p class="text_url"><a href="https://www.cbssports.com/nfl/news/five-interesting-joe-montana-nuggets-to-know-on-49ers-legends-64th-birthday/" target="_blank" style="color: grey">Cinco hechos de Montana en su cumpleaños</a></p>
        </div>
     </div>
    <div class="body-content"> 
        
        <div>
            <br>
            <br>
            <h1 class="acceso">Accesos directos</h1>
        </div>
       
        <div>
        <div class="row">
            <div class="col-lg-6">
                <div class="thumbnail">
                    <div class="caption">
                        <h2 style="text-align: center"> MVP </h2>
                        <p style="text-align: center">
                            Jugador con más puntos por partido
                        </p>
                        <p style="text-align: center">
                            <?= Html::a('Resultado', ['site/mvp'], ['class' => 'btn btn-primary'])?>
                        </p>
                    </div>
                </div>
            </div>
          
            <div class="col-lg-6">
                <div class="thumbnail">
                    <div class="caption">
                        <h2 style="text-align: center"> Centro Yale </h2>
                        <p style="text-align: center">
                            Jugadores del centro de Yale
                        </p>
                        <p style="text-align: center">
                            <?= Html::a('Resultado', ['site/consulta2'], ['class' => 'btn btn-primary'])?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        </div> 
        <div class="row">
            <div class="col-lg-6">
                <div class="thumbnail">
                    <div class="caption">
                        <h2 style="text-align: center">  Media Touchdowns </h2>
                        <p style="text-align: center">
                            Media de touchdowns por partido
                        </p>
                        <p style="text-align: center">
                            <?= Html::a('Resultado', ['site/consulta3'], ['class' => 'btn btn-primary'])?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="thumbnail">
                    <div class="caption">
                        <h2 style="text-align: center"> Jugadores más jovenes </h2>
                        <p style="text-align: center">
                            Muestra los jugadores más jovenes de toda la temporada
                        </p>
                        <p style="text-align: center">
                            <?= Html::a('Resultado', ['site/consulta4'], ['class' => 'btn btn-primary'])?>
                        </p>
                    </div>
                </div>
            </div>
           </div>   
        <div class="row">
            <div class="col-lg-6">
                <div class="thumbnail">
                    <div class="caption">
                        <h2 style="text-align: center">  QuaterBacks Jaguar </h2>
                        <p style="text-align: center">
                            Nombre de los quarterbacks de los jaguars
                        </p>
                        <p style="text-align: center">
                            <?= Html::a('Resultado', ['site/consulta5'], ['class' => 'btn btn-primary'])?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="thumbnail">
                    <div class="caption">
                        <h2 style="text-align: center"> Jugadores más jovenes </h2>
                        <p style="text-align: center">
                            Muestra los jugadores más jovenes de toda la temporada
                        </p>
                        <p style="text-align: center">
                            <?= Html::a('Resultado', ['site/consulta4'], ['class' => 'btn btn-primary'])?>
                        </p>
                    </div>
                </div>
            </div>
        </div>   
            
        </div>
</div>