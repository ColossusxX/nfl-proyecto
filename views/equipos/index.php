<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */



$this->title = 'Equipos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="equipos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <br>
    <br>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout'=>"{items}\n{pager}",
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            'nombre',
            'estado',
            'numjugadores',
            'entrenadorprin',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <p style="text-align: right">
        <?= Html::a('Crear equipos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


</div>
